import {StatusCodes} from 'http-status-codes';
import type {ZodEffects, ZodObject, ZodRawShape} from 'zod';
import {errorResponse} from '@cornerstone/response-formatter';
import type {AppMiddleware} from '@/libs/server-router';

export type BasicSchema = ZodObject<ZodRawShape, 'strict' | 'passthrough'>;

export type ValidationDataSource = 'body' | 'query' | 'headers' | 'cookies';

export type CreateValidateSchema = <T extends BasicSchema>(schema: T | ZodEffects<T>, dataSrc: ValidationDataSource) => AppMiddleware;

export const createValidateSchema: CreateValidateSchema = (schema, dataSrc) => async (req, res, next) => {
  const result = await schema.safeParseAsync(req[dataSrc]);

  if (!result.success) {
    res.status(StatusCodes.UNPROCESSABLE_ENTITY)
      .json(
        errorResponse(
          'REQUEST_VALIDATION_ERROR',
          undefined,
          result.error.flatten(),
        ),
      );

    return;
  }

  const nextResult = next?.();

  if (!(nextResult instanceof Promise)) return;

  await nextResult;
};
