import type {AppMiddleware} from '@/libs/server-router';
import {AppError} from '@cornerstone/app-error';
import {StatusCodes} from 'http-status-codes';
import {appHost} from '@/config/app';

export const whitelistUrl = [
  '/api/oauth/google/authorization-url',
  '/api/oauth/google/sign-in',
  '/api/oauth/facebook/authorization-url',
  '/api/oauth/facebook/sign-in',
];

export const withCsrfTokenValidation: AppMiddleware = async (req, _res, next) => {
  const cookieCsrfToken = req.cookies['csrf-token'];
  const headerCsrfToken = req.headers['csrf-token'];

  const {url} = req;

  if (
    url &&
    !whitelistUrl.includes((new URL(url, appHost)).pathname) &&
    cookieCsrfToken !== headerCsrfToken
  ) {
    throw new AppError(
      'Invalid csrf token',
      'REQUEST_VALIDATION_ERROR',
      undefined,
      StatusCodes.FORBIDDEN,
    );
  }

  const nextResult = next?.();

  if (!(nextResult instanceof Promise)) return;

  await nextResult;
};
