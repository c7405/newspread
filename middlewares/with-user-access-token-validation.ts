import {findUserAccessToken} from '@/apis/server/authentication';
import {parseBearerToken} from '@/formatters/token';
import type {AppMiddleware} from '@/libs/server-router';
import {AppError} from '@cornerstone/app-error';
import {errorResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import {accessForbiddenHeader} from '@/config/authentication';

export const withUserAccessTokenValidation: AppMiddleware = async (req, res, next) => {
  try {
    const authorization = req.headers['authorization'];

    if (!authorization) {
      throw new AppError('Empty authorization header', 'INVALID_USER_ACCESS_TOKEN');
    }

    await findUserAccessToken({
      accessToken: parseBearerToken(authorization),
    });

    const nextResult = next?.();

    if (!(nextResult instanceof Promise)) return;

    await nextResult;
  } catch (e) {
    return res.status(StatusCodes.FORBIDDEN)
      .setHeader('access-control-expose-headers', accessForbiddenHeader)
      .setHeader(accessForbiddenHeader, 'true')
      .json(
        errorResponse(
          'INVALID_USER_ACCESS_TOKEN',
        ),
      );
  }
};
