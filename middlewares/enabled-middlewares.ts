import {withCsrfTokenValidation} from '.';

export const apiMiddlewares = [
  withCsrfTokenValidation,
] as const;
