export type GetOrPrepareElementById = (id: string, root: HTMLElement, tagName: keyof HTMLElementTagNameMap) => HTMLElement;

export const getOrPrepareElementById: GetOrPrepareElementById = (id, rootEl, tagName = 'div') => {
  let el = document.getElementById(id);

  if (!el) {
    el = document.createElement(tagName);
    el.id = id;

    rootEl.appendChild(el);
  }

  return el;
};
