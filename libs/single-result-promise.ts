export type CreateProimseTrigger<T> = () => Promise<T>;

export type CreatePromise = <T>(fn: CreateProimseTrigger<T>) => {
  exec: () => Promise<void> | never;
  subscribe: () => Promise<T>;
};

type QueueListItem<T> = {
  resolve: (value: T | PromiseLike<T>) => void;
  reject: (reason?: any) => void;
};

export function createPromise<T>(fn: CreateProimseTrigger<T>) {
  const queueList: QueueListItem<T>[] = [];

  let isProcessing = false;

  return {
    async exec() {
      if (isProcessing) return;

      isProcessing = true;

      try {
        const data = await fn();

        while (queueList.length) {
          queueList.shift()?.resolve(data);
        }
      } catch (e) {
        while (queueList.length) {
          queueList.shift()?.reject(e);
        }
      } finally {
        isProcessing = false;
      }
    },
    subscribe() {
      const promise = new Promise<T>((resolve, reject) => {
        queueList.push({resolve, reject});
      });

      this.exec();

      return promise;
    },
  };
}
