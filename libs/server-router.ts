import {apiMiddlewares} from '@/middlewares/enabled-middlewares';
import type {AllowedHttpMethod} from '@/types/request';
import {isAllowedHttpMethod} from '@/utils/server/request';
import {errorResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler, NextApiRequest, NextApiResponse} from 'next';

export type AppMiddleware = (req: NextApiRequest, res: NextApiResponse, next: AppMiddlewareNext) => void | Promise<void>;

export type AppMiddlewareNext = () => ReturnType<AppMiddleware | NextApiHandler>;

export type CreateAppMiddlewareNext = (req: NextApiRequest, res: NextApiResponse, list: (AppMiddleware | NextApiHandler)[], index: number) => AppMiddlewareNext;

export const createAppMiddlewareNext: CreateAppMiddlewareNext = (req, res, list, index) => () => (
  list[index]?.(req, res, createAppMiddlewareNext(req, res, list, index + 1))
);

export type CreateRouterConfig = {
  [key in AllowedHttpMethod]?: NextApiHandler | (AppMiddleware | NextApiHandler)[];
}

export type CreateRouter = (config: CreateRouterConfig) => NextApiHandler;

export const createRouter: CreateRouter = config => async (req, res) => {
  const {method} = req;

  if (!method || !isAllowedHttpMethod(method) || !(method in config) || !config[method]) {
    res.status(StatusCodes.METHOD_NOT_ALLOWED)
      .json(errorResponse('HTTP_METHOD_FORBIDDEN'));

    return;
  }

  const configMethodItem = config[method];

  if (!configMethodItem) return;

  const appliedMiddlewares: (AppMiddleware | NextApiHandler)[] = [
    ...apiMiddlewares,
  ];

  appliedMiddlewares.push(
    ...Array.isArray(configMethodItem)
      ? configMethodItem
      : [configMethodItem],
  );

  const result = createAppMiddlewareNext(req, res, appliedMiddlewares, 0)();

  if (!(result instanceof Promise)) return;

  await result;
};
