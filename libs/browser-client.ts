import {substituteToken} from '@/apis/client/authentication';
import type {SubstituteTokenResponseData} from '@/apis/client/authentication/subsitute-token';
import {appHost} from '@/config/app';
import {accessForbiddenHeader} from '@/config/authentication';
import {csrfTokenCookieKey, csrfTokenHeaderKey} from '@/config/security';
import {createBearerToken} from '@/formatters/token';
import {createPromise} from '@/libs/single-result-promise';
import {getAccessToken, setAccessToken} from '@/utils/client/access-token';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import cookies from 'js-cookie';
import ky from 'ky';
import * as selfModule from './browser-client';

const updateRefreshTokenPromise = createPromise<SubstituteTokenResponseData>(async () => {
  const {accessToken} = await substituteToken();

  return {accessToken};
});

export const client = ky.extend({
  prefixUrl: appHost,
  headers: {
    'content-type': 'application/json',
  },
  hooks: {
    beforeRequest: [
      async request => {
        const accessToken = getAccessToken();

        request.headers.set('authorization', accessToken ? createBearerToken(accessToken) : '');
        request.headers.set(csrfTokenHeaderKey, cookies.get(csrfTokenCookieKey) ?? '');
      },
    ],
    afterResponse: [
      async (request, _options, response) => {
        const {
          status: repsonseStatus,
          headers: responseHeaders,
        } = response;

        const accessForbiddenHeaderValue = responseHeaders.get(accessForbiddenHeader);

        if (
          repsonseStatus !== StatusCodes.FORBIDDEN ||
          !accessForbiddenHeaderValue ||
          !JSON.parse(accessForbiddenHeaderValue)
        ) return;

        const {accessToken} = await updateRefreshTokenPromise.subscribe();

        setAccessToken(accessToken);

        const {headers} = request;

        headers.set('authorization', accessToken ? createBearerToken(accessToken) : '');
        headers.set('content-type', 'application/json');

        return ky(request);
      },
    ],
  },
});

export async function swrFetcher<T = unknown>(path: string, searchParams?: Record<string, string>) {
  const {data} = await selfModule.client
    .get(path, {searchParams})
    .json<SuccessResponseContent<T>>();

  return data;
}
