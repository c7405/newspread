export type ToKeyMap = <MapKey extends string, MapKeyValue extends string, Item extends Record<MapKey, MapKeyValue> & Record<string, any>>(list: Item[], key: MapKey) => Record<MapKeyValue, Item>;

export const toKeyMap: ToKeyMap = (list, key) => (
  list.reduce((carry, item) => Object.assign(
    carry ?? {},
    {[item[key]]: item},
  ), {} as unknown as Record<string, any>)
);
