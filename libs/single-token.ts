export type Token = string | null;

export type SetToken = (token: Token) => void;

export type GetToken = () => Token;

export type SingleTokenHandler = {
  setToken: SetToken;
  getToken: GetToken;
};

export type CreateSingleToken = () => SingleTokenHandler;

export const createSingleToken: CreateSingleToken = () => {
  let token: Token = null;

  return {
    setToken: newToken => (token = newToken),
    getToken: () => token,
  };
};
