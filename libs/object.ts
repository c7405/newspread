export type OnlyTruthyValue = (data: Record<string, any>) => Record<string, any>;

export const onlyTruthyValue: OnlyTruthyValue = data => Object.entries(data)
  .filter(([, value]) => value)
  .reduce((carry, [key, value]) => Object.assign(carry, {[key]: value}), {});
