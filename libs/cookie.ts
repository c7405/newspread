import {enc} from 'crypto-js';
import AES from 'crypto-js/aes';
import {encryptKey as appEncryptKey} from '@/config/app';

export type EncryptValue = (value: string) => string;

export const encryptValue: EncryptValue = value => `${AES.encrypt(value, appEncryptKey)}`;

export type DecryptValue = (value: string) => string;

export const decryptValue: DecryptValue = value => (
  AES.decrypt(value, appEncryptKey).toString(enc.Utf8)
);
