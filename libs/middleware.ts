import type {NextRequest} from 'next/dist/server/web/spec-extension/request';
import {NextResponse} from 'next/dist/server/web/spec-extension/response';
import type {NextMiddleware, NextMiddlewareResult} from 'next/dist/server/web/types';

export type NextMiddlewareHandler = () => Promise<NextMiddlewareResult>;
export type AppMiddleware = (req: NextRequest, res: NextResponse, next: NextMiddlewareHandler) => Promise<NextMiddlewareResult>;

export type WithMiddlewares = (...middlewares: AppMiddleware[]) => NextMiddleware;

type CreateNext = (req: NextRequest, res: NextResponse, middlewares: AppMiddleware[], index: number) => NextMiddlewareHandler;

const createNext: CreateNext = (req, res, middlewares, index) => async () => {
  const nextId = index + 1;

  return middlewares[nextId]?.(
    req,
    res,
    createNext(req, res, middlewares, nextId),
  );
};

export const withMiddlewares: WithMiddlewares = (...middlewares) => async req => (
  createNext(req, NextResponse.next(), middlewares, -1)()
);
