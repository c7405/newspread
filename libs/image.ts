import type {AppImageFormat} from '@/types/resources';
import * as selfModule from './image';

export type HasWebpSupport = () => boolean;

export const hasWebpSupport: HasWebpSupport = () => (
  document.createElement('canvas')
    .toDataURL('image/webp')
    .indexOf('data:image/webp') === 0
);

export type HasAvifSupport = () => Promise<boolean>;

export const hasAvifSupport: HasAvifSupport = async () => {
  const imageBlobLink = 'data:image/avif;base64,AAAAFGZ0eXBhdmlmAAAAAG1pZjEAAACgbWV0YQAAAAAAAAAOcGl0bQAAAAAAAQAAAB5pbG9jAAAAAEQAAAEAAQAAAAEAAAC8AAAAGwAAACNpaW5mAAAAAAABAAAAFWluZmUCAAAAAAEAAGF2MDEAAAAARWlwcnAAAAAoaXBjbwAAABRpc3BlAAAAAAAAAAQAAAAEAAAADGF2MUOBAAAAAAAAFWlwbWEAAAAAAAAAAQABAgECAAAAI21kYXQSAAoIP8R8hAQ0BUAyDWeeUy0JG+QAACANEkA=';

  try {
    const response = await fetch(imageBlobLink);

    await response.blob();

    return true;
  } catch (e) {
    return false;
  }
};

export type GetAppImageFormat = (src: string) => AppImageFormat;

export const getAppImageFormat: GetAppImageFormat = src => {
  if (selfModule.hasWebpSupport()) {
    return 'WEBP';
  }

  return src.toLowerCase().endsWith('.png') ? 'PNG' : 'JPEG';
};

export type GetImageSizes = (ratio: number) => string;

export const getImageSizes: GetImageSizes = ratio => (
  [576, 768, 992, 1200].map(breakpoint => (
    `(max-width: ${breakpoint}px) ${Math.round(breakpoint / 2 * ratio)}px`
  ))
    .concat(`${ratio * 100}vw`)
    .join(',')
);
