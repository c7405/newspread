import {env, appHost} from '@/config/app';

export type IsEnv = (...testEnvs: string[]) => boolean;

export const isEnv: IsEnv = (...testEnvs) => testEnvs.some(testEnv => env === testEnv);

export type BaseUrl = (path: string) => string;

export const baseUrl: BaseUrl = path => `${new URL(path, appHost)}`;
