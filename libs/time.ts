import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

dayjs.extend(duration);

export type HumanReadDiffMetaUnit = 'day' | 'week' | 'month' | 'year';

export type HumanReadDiffMeta = {
  unit: HumanReadDiffMetaUnit;
  value: number;
};

export type HumanReadDiff = (dateA: dayjs.ConfigType, dateB: dayjs.ConfigType) => HumanReadDiffMeta;

export const humanReadDiff: HumanReadDiff = (dateA, dateB) => {
  const diffValue = dayjs(dateA).diff(dayjs(dateB), 'day');
  const absDiffValue = Math.abs(diffValue);
  const diffDuration = dayjs.duration(diffValue, 'days');

  let durationValue: HumanReadDiffMeta = {
    unit: 'day',
    value: 0,
  };

  if (absDiffValue < 7) {
    durationValue = {
      unit: 'day',
      value: diffDuration.get('days'),
    };
  }

  if (absDiffValue >= 7 && absDiffValue < 30) {
    durationValue = {
      unit: 'week',
      value: diffDuration.get('weeks'),
    };
  }

  if (absDiffValue >= 30 && absDiffValue < 365) {
    durationValue = {
      unit: 'month',
      value: diffDuration.get('months'),
    };
  }

  if (absDiffValue >= 365) {
    durationValue = {
      unit: 'year',
      value: diffDuration.get('years'),
    };
  }

  return durationValue;
};
