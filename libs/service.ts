import {imageServiceHost} from '@/config/service';

export type ImageServiceUrl = (path: string) => string;

export const imageServiceUrl: ImageServiceUrl = path => `${new URL(path, imageServiceHost)}`;

export type IsImageServiceUrl = (path: string) => boolean;

export const isImageServiceUrl: IsImageServiceUrl = path => path.includes(imageServiceHost);
