import {z} from 'zod';

export const schema = z.object({
  keyword: z.union([
    z.string().min(1).max(100),
    z.undefined(),
  ]),
  cursor: z.union([
    z.string().min(1),
    z.undefined(),
  ]),
  mode: z.union([
    z.enum(['ALL', 'USER_BOOKMARKED']),
    z.undefined(),
  ]),
})
  .required()
  .strict();
