import {z} from 'zod';

export const schema = z.object({
  postId: z.number().min(1),
})
  .required()
  .strict();
