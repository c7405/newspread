import {z} from 'zod';

export const schema = z.object({
  postId: z.string().min(1).regex(/\d+/),
})
  .required()
  .strict();
