import {z} from 'zod';

export const querySchema = z.object({
  code: z.string().min(1).max(500),
})
  .passthrough()
  .required();
