import {z} from 'zod';

export const schema = z.object({
  subscriptionPlan: z.enum(['MONTHLY', 'ANNUAL']).nullable(),
})
  .strict();
