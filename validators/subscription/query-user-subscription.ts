import {z} from 'zod';

export const querySchema = z.object({
  userId: z.string().min(1).regex(/\d+/),
})
  .required()
  .strict();
