import {z} from 'zod';

export const cookiesSchema = z.object({
  'refresh-token': z.string().min(1),
})
  .passthrough()
  .required();

export const querySchema = z.object({
  year: z.string().regex(/^\d{4}$/),
})
  .strict()
  .required();
