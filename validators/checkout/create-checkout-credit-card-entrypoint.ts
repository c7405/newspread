import {z} from 'zod';

export const bodySchema = z.object({
  subscriptionType: z.enum(['ANNUAL', 'MONTHLY']),
  remark: z.string().min(1).max(500).optional().nullable(),
})
  .strict()
  .required();
