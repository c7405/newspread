import {z} from 'zod';

export const cookiesSchema = z.object({
  'refresh-token': z.string().min(1),
})
  .passthrough()
  .required();
