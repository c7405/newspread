import {z} from 'zod';

export const cookiesSchema = z.object({
  'refresh-token': z.string().min(1),
})
  .passthrough()
  .required();

export const querySchema = z.object({
  cursor: z.string().min(1).regex(/^\d+$/).optional().nullable(),
  perPage: z.string().min(1).regex(/^\d+$/),
})
  .strict()
  .required();
