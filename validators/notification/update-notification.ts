import {z} from 'zod';

export const querySchema = z.object({
  id: z.string().regex(/\d+/),
})
  .strict()
  .required();

export const bodySchema = z.object({
  hasRead: z.boolean(),
})
  .strict()
  .required();
