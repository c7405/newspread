export {
  cookiesSchema as queryNotificationsCookiesSchema,
  querySchema as queryNotificationsQuerySchema,
} from './query-notifications';
export {
  bodySchema as updateNotificationBodySchema,
  querySchema as updateNotificationQuerySchema,
} from './update-notification';

