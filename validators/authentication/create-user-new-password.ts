import {z} from 'zod';

export const formSchema = z.object({
  newPassword: z.string().min(8),
  confirmNewPassword: z.string(),
})
  .strict()
  .required()
  .refine(
    ({newPassword, confirmNewPassword}) => newPassword === confirmNewPassword,
    {
      message: 'Passwords don\'t match',
      path: ['confirmNewPassword'],
    },
  );

export const bodySchema = z.object({
  newPassword: z.string().min(8),
  confirmNewPassword: z.string(),
  authorizationCode: z.string().length(40),
})
  .strict()
  .required()
  .refine(
    ({newPassword, confirmNewPassword}) => newPassword === confirmNewPassword,
    {
      message: 'Passwords don\'t match',
      path: ['confirmNewPassword'],
    },
  );
