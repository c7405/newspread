import {z} from 'zod';

export const querySchema = z.object({
  id: z.string().min(1).regex(/\d+/),
})
  .required()
  .strict();

export const bodySchema = z.object({
  firstName: z.string().min(1).max(200),
  lastName: z.string().min(1).max(200),
  avatarImagePath: z.string().min(1).max(200),
})
  .required()
  .strict();
