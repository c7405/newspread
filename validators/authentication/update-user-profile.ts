import {z} from 'zod';

export const schema = z.object({
  avatarImagePath: z.string().min(1),
  firstName: z.string().min(1).max(200),
  lastName: z.string().min(1).max(200),
})
  .strict()
  .required();
