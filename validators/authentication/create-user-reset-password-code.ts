import {z} from 'zod';

export const bodySchema = z.object({
  account: z.string().min(1).email(),
})
  .required()
  .strict();
