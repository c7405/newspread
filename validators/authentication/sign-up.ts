import z from 'zod';

export const schema = z.object({
  account: z.string().min(1).max(200).email(),
  password: z.string().min(8),
  repassword: z.string().min(8),
  firstName: z.string().min(1).max(200),
  lastName: z.string().min(1).max(200),
})
  .strict()
  .required()
  .refine(
    ({password, repassword}) => password === repassword,
    {
      message: 'Re-enterred password should match the password',
      path: ['repassword'],
    },
  );
