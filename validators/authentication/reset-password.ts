import {z} from 'zod';

export const schema = z.object({
  account: z.string().min(1).max(200).email(),
})
  .strict()
  .required();
