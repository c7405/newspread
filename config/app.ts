export const appHost = process.env.NEXT_PUBLIC_APP_HOST ?? 'http://localhost:3000';

export const encryptKey = process.env.APP_ENCRYPT_KEY ?? '';

export const env = process.env.NODE_ENV;
