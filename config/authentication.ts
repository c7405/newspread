export const clientId = process.env.AUTHENTICATION_SERVICE_CRENDENTAIL_CLIENT_ID ?? '';

export const clientSecret = process.env.AUTHENTICATION_SERVICE_CRENDENTAIL_CLIENT_SECRET ?? '';

export const accessForbiddenHeader = 'authentication-access-forbidden';
