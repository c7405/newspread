export const csrfTokenCookieKey = 'csrf-token';

export const csrfTokenHeaderKey = 'csrf-token';
