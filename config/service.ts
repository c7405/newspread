export const authenticationServiceHost = process.env.AUTHENTICATION_SERVICE_HOST ?? '';

export const imageServiceHost = process.env.IMAGE_SERVICE_HOST ?? '';
