import {onlyTruthyValue} from '@/libs/object';

describe('Function: onlyTruthyValue', () => {
  it('should keep key with truthy value only', () => {
    const mockData = {
      a: 1,
      b: 0,
      c: undefined,
      d: null,
      e: '',
    };

    expect(onlyTruthyValue(mockData)).toEqual({a: 1});
  });
});
