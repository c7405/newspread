import {isEnv, baseUrl} from '@/libs/app';

jest.mock('@/config/app', () => ({
  env: 'mockEnv',
  appHost: 'https://mock.host',
}));

describe('function: isEnv', () => {
  it('checked if any of the input environments is current environment', () => {
    expect(isEnv('mockEnv')).toBe(true);
    expect(isEnv('mockEnv 1', 'mockEnv')).toBe(true);
  });
});

describe('function: baseUrl', () => {
  it('outputs url prefixed with app host', () => {
    expect(baseUrl('mock/page/path')).toBe('https://mock.host/mock/page/path');
    expect(baseUrl('/mock/page/path')).toBe('https://mock.host/mock/page/path');
  });
});
