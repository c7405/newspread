import {toKeyMap} from '@/libs/array';

describe('function: toKeyMap', () => {
  it('sets array item as the value to output object with item value of input key as key', () => {
    const mockList = [
      {
        mock: 'a',
        another: 'b',
      },
      {
        mock: 'b',
        another: 'a',
      },
    ];

    expect(toKeyMap(mockList, 'mock')).toEqual({
      a: {
        mock: 'a',
        another: 'b',
      },
      b: {
        mock: 'b',
        another: 'a',
      },
    });
  });
} );
