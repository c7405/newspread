import {imageServiceUrl, isImageServiceUrl} from '@/libs/service';

jest.mock('@/config/service', () => ({
  imageServiceHost: 'https://mock.image.service.host',
}));

describe('function: imageServiceUrl', () => {
  it('get image service url according to input path', () => {
    expect(imageServiceUrl('path/name')).toBe('https://mock.image.service.host/path/name');
    expect(imageServiceUrl('/path/name')).toBe('https://mock.image.service.host/path/name');
  });
});

describe('function: isImageServiceUrl', () => {
  it('checks if input url is image service url', () => {
    expect(isImageServiceUrl('https://mock.image.service.host/path')).toBe(true);
    expect(isImageServiceUrl('https://mock.not/path')).toBe(false);
  });
});
