import {getOrPrepareElementById} from '@/libs/dom';

describe('Function: getOrPrepareElementById', () => {
  let rootEl: HTMLElement;

  beforeEach(() => {
    rootEl = document.createElement('div');
    document.body.appendChild(rootEl);
  });

  afterEach(() => {
    document.body.innerHTML = '';
  });

  it('should get existing element when the it could be found', () => {
    const existingEl = document.createElement('div');
    const mockId = 'mock-id';

    existingEl.id = mockId;
    document.body.appendChild(existingEl);

    expect(getOrPrepareElementById(mockId, rootEl, 'div')).toBe(existingEl);
  });

  it('should create non-exist element when the it could not be found', () => {
    const mockId = 'mock-id';
    const el = getOrPrepareElementById(mockId, rootEl, 'div');

    expect(rootEl.firstElementChild).toBe(el);
  });
});
