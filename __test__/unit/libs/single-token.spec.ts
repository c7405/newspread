import {createSingleToken} from '@/libs/single-token';

describe('function: createSingleToken', () => {
  it.only('token getter and setter after token is being created', () => {
    const singleToken = createSingleToken();

    expect(singleToken).toEqual(
      expect.objectContaining({
        setToken: expect.any(Function),
        getToken: expect.any(Function),
      }),
    );

    const {setToken, getToken} = singleToken;

    expect(getToken()).toBeNull();

    const mockToken = 'mockToken';

    setToken(mockToken);

    expect(getToken()).toBe(mockToken);
  });
});
