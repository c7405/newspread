import {encryptValue, decryptValue} from '@/libs/cookie';
import AES from 'crypto-js/aes';
import {enc, lib} from 'crypto-js';

jest.mock('@/config/app', () => ({
  encryptKey: 'mockEncryptKey',
}));

describe('function: encryptValue', () => {
  it('outputs encrypted input string', () => {
    const emptyCipherParams = lib.CipherParams.create({});
    const encryptedString = 'mock';
    const mockValue = 'value';

    const mockEncrypt = jest.spyOn(AES, 'encrypt');
    const mockToString = jest.spyOn(emptyCipherParams, 'toString');

    mockEncrypt.mockReturnValueOnce(emptyCipherParams);
    mockToString.mockReturnValueOnce(encryptedString);

    expect(encryptValue(mockValue)).toBe(encryptedString);

    expect(mockEncrypt).toHaveBeenCalledTimes(1);
    expect(mockEncrypt).toHaveBeenCalledWith(mockValue, 'mockEncryptKey');

    expect(mockToString).toHaveBeenCalledTimes(1);
    expect(mockToString).toHaveBeenCalledWith();
  });
});

describe('function: decrypt', () => {
  it('decrypts input value', () => {
    const emptyWordArray = lib.WordArray.create();
    const mockDecrypt = jest.spyOn(AES, 'decrypt');
    const mockToString = jest.spyOn(emptyWordArray, 'toString');
    const mockEncryptedString = 'mockEncrypted';
    const mockDecryptedString = 'mockDecrypted';

    mockDecrypt.mockReturnValueOnce(emptyWordArray);
    mockToString.mockReturnValueOnce(mockDecryptedString);

    expect(decryptValue(mockEncryptedString)).toBe(mockDecryptedString);

    expect(mockDecrypt).toBeCalledTimes(1);
    expect(mockDecrypt).toHaveBeenCalledWith(mockEncryptedString, 'mockEncryptKey');

    expect(mockToString).toHaveBeenCalledTimes(1);
    expect(mockToString).toHaveBeenCalledWith(enc.Utf8);
  });
});
