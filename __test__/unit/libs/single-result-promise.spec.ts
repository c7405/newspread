import {createPromise} from '@/libs/single-result-promise';

describe('function: createPromise', () => {
  it('returns result to subscribers after input fn being executed', async () => {
    const mockData = {mock: true};
    const promise = createPromise(() => new Promise(resolve => {
      window.setTimeout(() => {
        resolve(mockData);
      }, 100);
    }));

    const results = await Promise.all([
      promise.subscribe(),
      promise.subscribe(),
    ]);

    results.forEach(result => {
      expect(result).toBe(mockData);
    });
  });
});
