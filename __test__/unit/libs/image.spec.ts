import {hasWebpSupport, getAppImageFormat, getImageSizes} from '@/libs/image';
import * as imageModule from '@/libs/image';

describe('function: hasWebpSupport', () => {
  it('detects webp support', () => {
    const mockToDataURL = jest.spyOn(HTMLCanvasElement.prototype, 'toDataURL');

    mockToDataURL.mockReturnValueOnce('notSupport')
      .mockReturnValueOnce('data:image/webp;mock');

    expect(hasWebpSupport()).toBe(false);
    expect(hasWebpSupport()).toBe(true);

    expect(mockToDataURL).toHaveBeenCalledTimes(2);
    expect(mockToDataURL).toHaveBeenCalledWith('image/webp');

    mockToDataURL.mockRestore();
  });
});

describe('function: getAppImageFormat', () => {
  it('get image format for webp', () => {
    const mockHasWebpSupport = jest.spyOn(imageModule, 'hasWebpSupport');

    mockHasWebpSupport.mockReturnValueOnce(true);

    expect(getAppImageFormat('mock')).toBe('WEBP');

    expect(mockHasWebpSupport).toHaveBeenCalledTimes(1);
    expect(mockHasWebpSupport).toHaveBeenCalledWith();

    mockHasWebpSupport.mockRestore();
  });

  it('get image format from image source for other non-webp type', () => {
    const mockHasWebpSupport = jest.spyOn(imageModule, 'hasWebpSupport');

    mockHasWebpSupport.mockReturnValue(false);

    [
      'https://mock.test/1.png',
      'https://mock.test/1.PNG',
    ]
      .forEach(src => {
        expect(getAppImageFormat(src)).toBe('PNG');
      });

    [
      'https://mock.test/1.jpg',
      'https://mock.test/1.jpeg',
      'https://mock.test/1.JPG',
      'https://mock.test/1.JPEG',
    ]
      .forEach(src => {
        expect(getAppImageFormat(src)).toBe('JPEG');
      });

    expect(mockHasWebpSupport).toHaveBeenCalledTimes(6);
    expect(mockHasWebpSupport).toHaveBeenCalledWith();

    mockHasWebpSupport.mockRestore();
  });
});

describe('function: getImageSizes', () => {
  it('get image sized according to input ratio', () => {
    expect(getImageSizes(0.5)).toBe('(max-width: 576px) 144px,(max-width: 768px) 192px,(max-width: 992px) 248px,(max-width: 1200px) 300px,50vw');
  });
});
