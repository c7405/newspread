import {humanReadDiff} from '@/libs/time';

describe('Function: humanReadDiff', () => {
  it('should return unit in day and value if absolute day diff is less then 7', () => {
    const dateA = '2022-04-01 00:00:00';
    const dateB = '2022-03-31 00:00:00';

    expect(humanReadDiff(dateA, dateB)).toEqual({
      unit: 'day',
      value: 1,
    });
  });

  it('should return unit in week and value if absolute day diff is greater then 7 and less then 30', () => {
    const dateA = '2022-04-01 00:00:00';
    const dateB = '2022-03-25 00:00:00';

    expect(humanReadDiff(dateA, dateB)).toEqual({
      unit: 'week',
      value: 1,
    });
  });

  it('should return unit in month and value if absolute day diff is greater then 30 and less then 365', () => {
    const dateA = '2022-04-01 00:00:00';
    const dateB = '2022-02-28 00:00:00';

    expect(humanReadDiff(dateA, dateB)).toEqual({
      unit: 'month',
      value: 1,
    });
  });

  it('should return unit in year and value if absolute day diff is greater then 365', () => {
    const dateA = '2022-04-01 00:00:00';
    const dateB = '2021-04-01 00:00:00';

    expect(humanReadDiff(dateA, dateB)).toEqual({
      unit: 'year',
      value: 1,
    });
  });
});
