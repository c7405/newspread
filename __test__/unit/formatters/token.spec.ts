import {createBearerToken, parseBearerToken} from '@/formatters/token';

describe('function: createBearerToken', () => {
  it('creates token in bearer format', () => {
    expect(createBearerToken('mock')).toBe('Bearer mock');
  });
});

describe('function: parseBearerToken', () => {
  it('returns input authorization if token is not of bearer token format', () => {
    expect(parseBearerToken('')).toBe('');
    expect(parseBearerToken('mock')).toBe('mock');
  });

  it('returns token without Bearer format if token is valid bearer token', () => {
    expect(parseBearerToken('Bearer token')).toBe('token');
  });
});
