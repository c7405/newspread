import type {HumanReadDiffMeta, HumanReadDiffMetaUnit} from '@/libs/time';
import {formatTimeRange} from '@/formatters/time';
import type {AcceptLocale} from '@/formatters/time';

describe('function: formatTimeRange', () => {
  test.each<[HumanReadDiffMetaUnit, number, AcceptLocale, string]>([
    ['day', 0, 'en', 'Today'],
    ['day', 1, 'en', '1 day later'],
    ['day', -1, 'en', '1 day ago'],
    ['day', 2, 'en', '2 days later'],
    ['day', -2, 'en', '2 days ago'],
    ['week', 0, 'en', 'This Week'],
    ['week', 1, 'en', '1 week later'],
    ['week', -1, 'en', '1 week ago'],
    ['week', 2, 'en', '2 weeks later'],
    ['week', -2, 'en', '2 weeks ago'],
    ['month', 0, 'en', 'This Month'],
    ['month', 1, 'en', '1 month later'],
    ['month', -1, 'en', '1 month ago'],
    ['month', 2, 'en', '2 months later'],
    ['month', -2, 'en', '2 months ago'],
    ['year', 0, 'en', 'This Year'],
    ['year', 1, 'en', '1 year later'],
    ['year', -1, 'en', '1 year ago'],
    ['year', 2, 'en', '2 years later'],
    ['year', -2, 'en', '2 years ago'],
  ])(
    'should display message for unit: %s and value: %i',
    (unit, value, locale, expectMessage) => {
      const metaData: HumanReadDiffMeta = {unit, value};

      expect(formatTimeRange(metaData, locale)).toBe(expectMessage);
    },
  );
});
