import {formatDate} from '@/formatters/date';

describe('function: formatDate', () => {
  it('should format date in YYYY.MM.DD format', () => {
    expect(formatDate('2099-12-31')).toBe('2099.12.31');
  });
});
