import {createPoint} from '@/formatters/page/size';

describe('function: createPoint', () => {
  it('should output point with defined format', () => {
    expect(createPoint(1, 2)).toEqual({x: 1, y: 2});
  });
});
