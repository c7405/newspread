import {formatAuthorsName} from '@/formatters/page/post';

describe('function: formatAuthorsName', () => {
  it('should join each author\'s first and last name with comma', () => {
    const authors = [
      {id: 1, firstName: 'A', lastName: 'B'},
      {id: 2, firstName: 'C', lastName: 'D'},
    ];

    expect(formatAuthorsName(authors)).toBe('A B, C D');
  });
});
