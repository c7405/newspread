import {createImageServiceLoaderUrlLoader} from '@/formatters/page/image-url';

describe('function: createImageServiceLoaderUrlLoader', () => {
  it('creates formatter that formats image url', () => {
    const imageFormatList = [
      'AVIF',
      'WEBP',
      'JPEG',
      'PNG',
    ] as const;

    imageFormatList.forEach(format => {
      const formatter = createImageServiceLoaderUrlLoader(format);

      expect(formatter).toBeInstanceOf(Function);

      const urlWithoutQuality = formatter({
        src: 'https://mock.test/image.jpg',
        width: 1080,
      });

      const urlWithQuality = formatter({
        src: 'https://mock.test/image.jpg',
        width: 1080,
        quality: 80,
      });

      expect(urlWithoutQuality).toBe(`https://mock.test/image.jpg?width=1080&quality=75&format=${format}`);
      expect(urlWithQuality).toBe(`https://mock.test/image.jpg?width=1080&quality=80&format=${format}`);
    });
  });
});
