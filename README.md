# Newspread

    行動版型的新聞聚合器，透過訂閱服務的權限調整可瀏覽的文章範圍。

## npm scripts

#### `npm run dev`

  * 以 `development` 模式執行App
  * 執行後可於 [http://localhost:3000](http://localhost:3000) 瀏覽執行內容
  * 程式碼有任何變動時，頁面會自動載入更新
  * 任何 linter 檢測出的錯誤都會紀錄在 console 中

### `npm run test:unit`
  * 使用 `jest` 執行單元測試，可在 `console` 中執行測試進階操作

### `npm run build`
  * 以 `production` 模式編譯App，並將靜態檔案輸出至 `.next` 資料夾
  * 與檔案編譯最佳化會在此階段執行

### `npm start`
  * 以 `production` 模式執行App
  * 執行後可於 [http://localhost:3000](http://localhost:3000) 瀏覽執行內容
  * 執行前請先執行 `npm run build`

### `npm run lint`
  * 檢查程式碼是否符合規則，可在 `console` 中檢視檢查結果

### `npm run storybook`
  * 預覽使用元件樣式與事件
  * 執行後可於 [http://localhost:6006/](http://localhost:6006/) 瀏覽執行內容
