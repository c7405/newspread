import {client} from '@/libs/browser-client';
import {onlyTruthyValue} from '@/libs/object';
import type {Notification} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryNotificationsRequestData = {
  cursor?: number;
  perPage: number;
};

export type QueryNotificationsResponseData = {
  notifications: Notification[];
};

export type QueryNotifications = (data: QueryNotificationsRequestData) => Promise<QueryNotificationsResponseData>;

export const queryNotifications: QueryNotifications = async data => {
  const {cursor, perPage} = data;

  const {
    data: {
      notifications,
    },
  } = await client.get(
    'api/notifications',
    {
      searchParams: onlyTruthyValue({
        cursor,
        perPage,
      }),
    },
  )
    .json<SuccessResponseContent<QueryNotificationsResponseData>>();

  return {notifications};
};


