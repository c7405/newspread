import {client} from '@/libs/browser-client';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type SignInRequestData = {
  account: string;
  password: string;
};

export type SignInResponseData = {
  accessToken: string;
};

export type SignIn = (data: SignInRequestData) => Promise<SignInResponseData>;

export const signIn: SignIn = async ({account, password}) => {
  const {
    data: {
      accessToken,
    },
  } = await client.post(
    'api/auth/sign-in',
    {
      json: {
        account,
        password,
      },
    },
  )
    .json<SuccessResponseContent<SignInResponseData>>();

  return {accessToken};
};
