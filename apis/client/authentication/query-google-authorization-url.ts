import {client} from '@/libs/browser-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryGoogleAuthorizationUrlResponseData = {
  authorizationUrl: string;
};

export type QueryGoogleAuthorizationUrl = () => Promise<QueryGoogleAuthorizationUrlResponseData> | never;

export const queryGoogleAuthorizationUrl: QueryGoogleAuthorizationUrl = async () => {
  const {
    data: {
      authorizationUrl,
    },
  } = await client.get('api/oauth/google/authorization-url')
    .json<SuccessResponseContent<QueryGoogleAuthorizationUrlResponseData>>();

  return {authorizationUrl};
};


