import {client} from '@/libs/browser-client';
import {SuccessResponseContent} from '@cornerstone/response-formatter';
import {User} from '@/types/resources';

export type FindUserWithDetailRequestData = {
  id: User['id'];
};

export type FindUserWithDetailResponseData = User;

export type FindUserWithDetail = (data: FindUserWithDetailRequestData) => Promise<FindUserWithDetailResponseData>;

export const findUserWithDetail: FindUserWithDetail = async ({id: userId}) => {
  const {
    data: {
      id,
      account,
      userDetail,
    },
  } = await client.get(`api/auth/user/${userId}`)
    .json<SuccessResponseContent<FindUserWithDetailResponseData>>();

  return {
    id,
    account,
    userDetail,
  };
};
