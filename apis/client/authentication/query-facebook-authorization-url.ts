import {client} from '@/libs/browser-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryFacebookAuthorizationUrlResponseData = {
  authorizationUrl: string;
};

export type QueryFacebookAuthorizationUrl = () => Promise<QueryFacebookAuthorizationUrlResponseData> | never;

export const queryFacebookAuthorizationUrl: QueryFacebookAuthorizationUrl = async () => {
  const {
    data: {
      authorizationUrl,
    },
  } = await client.get('api/oauth/facebook/authorization-url')
    .json<SuccessResponseContent<QueryFacebookAuthorizationUrlResponseData>>();

  return {authorizationUrl};
};


