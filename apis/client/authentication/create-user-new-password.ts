import {client} from '@/libs/browser-client';

export type CreateUserNewPasswordRequestData = {
  newPassword: string;
  confirmNewPassword: string;
  authorizationCode: string;
};

export type CreateUserNewPassword = (data: CreateUserNewPasswordRequestData) => Promise<void> | never;

export const createUserNewPassword: CreateUserNewPassword = async data => {
  const {newPassword, confirmNewPassword, authorizationCode} = data;

  await client.post(
    'api/auth/reset-password',
    {
      json: {
        newPassword,
        confirmNewPassword,
        authorizationCode,
      },
    },
  );
};
