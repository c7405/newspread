import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import {client} from '@/libs/browser-client';

export type SubstituteTokenResponseData = {
  accessToken: string;
};

export type SubsituteToken = () => Promise<SubstituteTokenResponseData>;

export const substituteToken = async () => {
  const {
    data: {
      accessToken,
    },
  } = await client.patch(
    'api/auth/token',
  )
    .json<SuccessResponseContent<SubstituteTokenResponseData>>();

  return {accessToken};
};
