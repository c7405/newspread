import {client} from '@/libs/browser-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type SignUpRequestData = {
  account: string;
  password: string;
  repassword: string;
  firstName: string;
  lastName: string;
};

export type SignUpResponseData = {
  id: number;
  userDetail: {
    id: number;
  };
};

export type SignUp = (data: SignUpRequestData) => Promise<SignUpResponseData>;

export const signUp: SignUp = async data => {
  const {
    account,
    password,
    repassword,
    firstName,
    lastName,
  } = data;

  const {
    data: {
      id,
      userDetail: {
        id: userDetailId,
      },
    },
  } = await client.post(
    'api/auth/sign-up',
    {
      json: {
        account,
        password,
        repassword,
        firstName,
        lastName,
      },
    },
  )
    .json<SuccessResponseContent<SignUpResponseData>>();

  return {
    id,
    userDetail: {
      id: userDetailId,
    },
  };
};
