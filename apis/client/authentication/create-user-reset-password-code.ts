import {client} from '@/libs/browser-client';

export type CreateUserResetPasswordCodeRequestData = {
  account: string;
};

export type CreatUserResetPasswordCode = (data: CreateUserResetPasswordCodeRequestData) => Promise<void> | never;

export const createUserResetPasswordCode: CreatUserResetPasswordCode = async ({account}) => {
  await client.post(
    'api/auth/reset-password-code',
    {
      json: {
        account,
      },
    },
  );
};
