import {client} from '@/libs/browser-client';

export type DeleteToken = () => Promise<void>;

export const deleteToken: DeleteToken = async () => {
  await client.delete('api/auth/token');
};
