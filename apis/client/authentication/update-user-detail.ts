import {client} from '@/libs/browser-client';
import type {UserDetail} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type UpdateUserDetailRequestData = Pick<UserDetail, 'firstName' | 'lastName' | 'avatarImagePath'>;

export type UpdateUserDetailResponseData = {
  userDetail: UserDetail;
};

export type UserUserDetail = (id: UserDetail['id'], data: UpdateUserDetailRequestData) => Promise<UpdateUserDetailResponseData>;

export const updateUserDetail: UserUserDetail = async (id, data) => {
  const {
    data: {
      userDetail,
    },
  } = await client.patch(
    `api/auth/user-detail/${id}`,
    {
      json: data,
    },
  )
    .json<SuccessResponseContent<UpdateUserDetailResponseData>>();

  return {userDetail};
};
