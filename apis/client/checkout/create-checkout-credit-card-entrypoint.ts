import {client} from '@/libs/browser-client';
import {CheckoutPeriodType, SubscriptionPlanType} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type CreateCheckoutCreditCardParamsRequestData = {
  subscriptionType: SubscriptionPlanType;
  remark?: string;
};

export type CreateCheckoutCreditCardParamsResponseData = {
  checkoutEntrypoint: string;
  checkoutCreditCardParams: {
    MerchantTradeNo: string;
    MerchantTradeDate: string;
    TotalAmount: number;
    TradeDesc: number;
    ItemName: string;
    ReturnURL: string;
    ChoosePayment: 'Credit';
    OrderResultURL: string;
    PeriodAmount: number;
    PeriodType: CheckoutPeriodType;
    Frequency: number;
    ExecTimes: number;
    PeriodReturnURL: string;
    MerchantID: number;
    PaymentType: 'aio',
    EncryptType: 1,
    DeviceSource: string,
    CheckMacValue: string;
  };
};

export type CreateCheckoutCreditCardParams = (data: CreateCheckoutCreditCardParamsRequestData) => Promise<CreateCheckoutCreditCardParamsResponseData> | never;

export const createCheckoutCreditCardEntrypoint: CreateCheckoutCreditCardParams = async data => {
  const {
    subscriptionType,
    remark,
  } = data;

  const {
    data: {
      checkoutEntrypoint,
      checkoutCreditCardParams,
    },
  } = await client.post(
    'api/checkout/entrypoint/credit-card',
    {
      json: {
        subscriptionType,
        remark,
      },
    },
  )
    .json<SuccessResponseContent<CreateCheckoutCreditCardParamsResponseData>>();

  return {
    checkoutEntrypoint,
    checkoutCreditCardParams,
  };
};
