import {client} from '@/libs/browser-client';
import type {User, UserSubscription} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryUserSubscriptionRequestData = {
  userId: User['id'];
};

export type QueryUserSubscriptionResponseData = {
  userSubscription: UserSubscription;
};

export type QueryUserSubscription = (data: QueryUserSubscriptionRequestData) => Promise<QueryUserSubscriptionResponseData>;

export const queryUserSubscription: QueryUserSubscription = async ({userId}) => {
  const {
    data: {
      userSubscription,
    },
  } = await client.get(
    'api/subscription/user-subscription',
    {
      searchParams: {
        userId,
      },
    },
  )
    .json<SuccessResponseContent<QueryUserSubscriptionResponseData>>();

  return {userSubscription};
};
