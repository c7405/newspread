import {client} from '@/libs/browser-client';
import {UserSubscription} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type DeleteUserSubscriptionRequestData = {
  id: UserSubscription['id'];
};

export type DeleteUserSubscriptionResponseData = {
  userSubscription: UserSubscription;
};

export type DeleteUserSubscription = (data: DeleteUserSubscriptionRequestData) => Promise<DeleteUserSubscriptionResponseData> | never;

export const deleteUserSubscription: DeleteUserSubscription = async ({id}) => {
  const {
    data: {
      userSubscription,
    },
  } = await client.delete(`api/subscription/user-subscription/${id}`)
    .json<SuccessResponseContent<DeleteUserSubscriptionResponseData>>();

  return {userSubscription};
};
