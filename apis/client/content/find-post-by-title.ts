import {client} from '@/libs/browser-client';
import type {Post} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type FindPostByTitleRequestData = {
  title: Post['title'];
};

export type FindPostByTitleResponseData = {
  post: Post;
};

export type FindPostByTitle = (data: FindPostByTitleRequestData) => Promise<FindPostByTitleResponseData>;

export const findPostByTitle: FindPostByTitle = async ({title}) => {
  const {
    data: {
      post,
    },
  } = await client.get(`api/content/post/title/${title}`)
    .json<SuccessResponseContent<FindPostByTitleResponseData>>();

  return {post};
};
