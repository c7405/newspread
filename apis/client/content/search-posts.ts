import {client} from '@/libs/browser-client';
import {SuccessResponseContent} from '@cornerstone/response-formatter';
import type {Post, Bookmark} from '@/types/resources';

export type SearchPostsRequestData = {
  keyword?: string;
  bookmarkUserId?: number;
  cursor?: number;
};

export type SearchPostsResponseData = {
  posts: Post[];
  bookmarks: Bookmark[];
  cursor?: number;
};

export type SearchPosts = (data: SearchPostsRequestData) => Promise<SearchPostsResponseData>;

export const searchPosts: SearchPosts = async ({keyword, bookmarkUserId, cursor}) => {
  const {
    data: {
      cursor: nextCursor,
      bookmarks,
      posts,
    },
  } = await client.get(
    'api/content/posts',
    {
      searchParams: {
        ...keyword && {keyword},
        ...bookmarkUserId && {bookmarkUserId},
        ...cursor && {cursor},
      },
    },
  )
    .json<SuccessResponseContent<SearchPostsResponseData>>();

  return {
    cursor: nextCursor,
    bookmarks,
    posts,
  };
};
