import {client} from '@/libs/browser-client';
import {Bookmark} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type DeleteBookmarkRequestData = {
  id: Bookmark['id'];
};

export type DeleteBookmarkResponseData = {
  bookmark: Bookmark;
};

export type DeleteBookmark = (data: DeleteBookmarkRequestData) => Promise<DeleteBookmarkResponseData> | never;

export const deleteBookmark: DeleteBookmark = async ({id}) => {
  const {
    data: {
      bookmark,
    },
  } = await client.delete(`api/content/bookmark/${id}`)
    .json<SuccessResponseContent<DeleteBookmarkResponseData>>();

  return {bookmark};
};
