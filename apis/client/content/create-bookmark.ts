import {client} from '@/libs/browser-client';
import {Bookmark, Post} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type CreateBookmarkRequestData = {
  postId: Post['id'];
};

export type CreateBookmarkResponseData = {
  bookmark: Bookmark;
};

export type CreateBookmark = (data: CreateBookmarkRequestData) => Promise<CreateBookmarkResponseData> | never;

export const createBookmark: CreateBookmark = async ({postId}) => {
  const {
    data: {
      bookmark,
    },
  } = await client.post(
    'api/content/bookmark',
    {
      json: {
        postId,
      },
    },
  )
    .json<SuccessResponseContent<CreateBookmarkResponseData>>();

  return {bookmark};
};
