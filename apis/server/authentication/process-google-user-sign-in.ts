import {client} from '@/libs/service-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type ProcessGoogleUserSignInRequestData = {
  code: string;
};

export type ProcessGoogleUserSignInResponseData = {
  refreshToken: string;
  accessToken: string;
};

export type ProcessGoogleUserSignIn = (data: ProcessGoogleUserSignInRequestData) => Promise<ProcessGoogleUserSignInResponseData> | never;

export const processGoogleUserSignIn: ProcessGoogleUserSignIn = async ({code}) => {
  const {
    data: {
      refreshToken,
      accessToken,
    },
  } = await client('authentication').post(
    'api/oauth/google/sign-in',
    {
      json: {
        code,
      },
    },
  )
    .json<SuccessResponseContent<ProcessGoogleUserSignInResponseData>>();

  return {
    refreshToken,
    accessToken,
  };
};
