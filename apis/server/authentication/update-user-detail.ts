import {client} from '@cornerstone/service-client';
import type {User, UserDetail} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type UpdateUserDetailRequestData =
  Pick<UserDetail, 'firstName' | 'lastName' | 'avatarImagePath'> & {
    updateUserId: User['id'];
  };

export type UpdateUserDetailResponseData = {
  userDetail: UserDetail;
};

export type UserUserDetail = (id: UserDetail['id'], data: UpdateUserDetailRequestData) => Promise<UpdateUserDetailResponseData>;

export const updateUserDetail: UserUserDetail = async (id, data) => {
  const {
    data: {
      userDetail,
    },
  } = await client('authentication').patch(
    `api/user-detail/${id}`,
    {
      json: data,
    },
  )
    .json<SuccessResponseContent<UpdateUserDetailResponseData>>();

  return {userDetail};
};
