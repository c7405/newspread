import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import {client} from '@cornerstone/service-client';

export type QueryGoogleAuthorizationUrlResponseData = {
  authorizationUrl: string;
};

export type QueryGoogleAuthorizationUrl = () => Promise<QueryGoogleAuthorizationUrlResponseData> | never;

export const queryGoogleAuthorizationUrl: QueryGoogleAuthorizationUrl = async () => {
  const {
    data: {
      authorizationUrl,
    },
  } = await client('authentication').get('api/oauth/google/authorization-url')
    .json<SuccessResponseContent<QueryGoogleAuthorizationUrlResponseData>>();

  return {authorizationUrl};
};


