import {client} from '@/libs/service-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type ProcessFacebookUserSignInRequestData = {
  code: string;
};

export type ProcessFacebookUserSignInResponseData = {
  refreshToken: string;
  accessToken: string;
};

export type ProcessFacebookUserSignIn = (data: ProcessFacebookUserSignInRequestData) => Promise<ProcessFacebookUserSignInResponseData> | never;

export const processFacebookUserSignIn: ProcessFacebookUserSignIn = async ({code}) => {
  const {
    data: {
      refreshToken,
      accessToken,
    },
  } = await client('authentication').post(
    'api/oauth/facebook/sign-in',
    {
      json: {
        code,
      },
    },
  )
    .json<SuccessResponseContent<ProcessFacebookUserSignInResponseData>>();

  return {
    refreshToken,
    accessToken,
  };
};
