import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import {client} from '@cornerstone/service-client';

export type QueryFacebookAuthorizationUrlResponseData = {
  authorizationUrl: string;
};

export type QueryFacebookAuthorizationUrl = () => Promise<QueryFacebookAuthorizationUrlResponseData> | never;

export const queryFacebookAuthorizationUrl: QueryFacebookAuthorizationUrl = async () => {
  const {
    data: {
      authorizationUrl,
    },
  } = await client('authentication').get('api/oauth/facebook/authorization-url')
    .json<SuccessResponseContent<QueryFacebookAuthorizationUrlResponseData>>();

  return {authorizationUrl};
};


