import {client} from '@/libs/service-client';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';

export type FindUserAccessTokenRequestData = {
  accessToken: string;
};

export type FindUserAccessTokenResponseData = {
  iat: number;
  exp: number;
  sub: string;
};

export type FindUserAccessToken = (data: FindUserAccessTokenRequestData) => Promise<FindUserAccessTokenResponseData>;

export const findUserAccessToken: FindUserAccessToken = async ({accessToken}) => {
  const {
    data: {
      iat,
      exp,
      sub,
    },
  } = await client('authentication').post(
    'api/token/user/access-token',
    {
      json: {
        accessToken,
      },
    },
  )
    .json<SuccessResponseContent<FindUserAccessTokenResponseData>>();

  return {
    iat,
    exp,
    sub,
  };
};
