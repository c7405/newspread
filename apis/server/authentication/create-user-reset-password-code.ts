import {client} from '@/libs/service-client';

export type CreateUserResetPasswordCodeRequestData = {
  account: string;
};

export type CreateUserResetPasswordCode = (data: CreateUserResetPasswordCodeRequestData) => Promise<void> | never;

export const createUserResetPasswordCode: CreateUserResetPasswordCode = async ({account}) => {
  await client('authentication').post(
    'api/user/reset-password-code',
    {
      json: {
        account,
      },
    },
  );
};
