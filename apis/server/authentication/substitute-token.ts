import {client} from '@/libs/service-client';

export type SubstituteTokenRequestData = {
  refreshToken: string;
};

export type SubstituteTokenResponseData = {
  refreshToken: string;
  accessToken: string;
};

export type SubstituteToken = (data: SubstituteTokenRequestData) => Promise<SubstituteTokenResponseData>;

export const substituteToken: SubstituteToken = async data => {
  const {refreshToken: currentRefreshToken} = data;

  const {
    data: {
      refreshToken,
      accessToken,
    },
  } = await client('authentication').patch(
    'api/token/user',
    {
      json: {
        refreshToken: currentRefreshToken,
      },
    },
  )
    .json<{data: SubstituteTokenResponseData}>();

  return {
    refreshToken,
    accessToken,
  };
};
