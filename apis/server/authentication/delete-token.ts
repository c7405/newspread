import {client} from '@cornerstone/service-client';

export type DeleteTokenRequestData = {
  refreshToken: string;
};

export type DeleteToken = (data: DeleteTokenRequestData) => Promise<void>;

export const deleteToken: DeleteToken = async ({refreshToken}) => {
  await client('authentication').delete(`api/token/user/${refreshToken}`);
};
