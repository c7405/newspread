import {client} from '@/libs/service-client';

export type ExchangeTokenRequestData = {
  authorizationCode: string;
};

export type ExchangeTokenResponseData = {
  refreshToken: string;
  accessToken: string;
};

export type ExchangeToken = (data: ExchangeTokenRequestData) => Promise<ExchangeTokenResponseData> | never;

export const exchangeToken: ExchangeToken = async ({authorizationCode}) => {
  const {
    data: {
      refreshToken,
      accessToken,
    },
  } = await client('authentication').post(
    'api/token/user',
    {
      json: {
        authorizationCode,
      },
    },
  )
    .json<{data: ExchangeTokenResponseData}>();

  return {
    refreshToken,
    accessToken,
  };
};
