import {client} from '@/libs/service-client';

export type CreateUserNewPasswordRequestData = {
  newPassword: string;
  confirmNewPassword: string;
  authorizationCode: string;
};

export type CreateUserNewPassword = (data: CreateUserNewPasswordRequestData) => Promise<void> | never;

export const createUserNewPassword: CreateUserNewPassword = async data => {
  const {newPassword, confirmNewPassword, authorizationCode} = data;

  await client('authentication').post(
    'api/user/password',
    {
      json: {
        newPassword,
        confirmNewPassword,
        authorizationCode,
      },
    },
  );
};
