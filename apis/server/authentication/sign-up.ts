import {client} from '@/libs/service-client';

export type SignUpRequestData = {
  account: string;
  password: string;
  repassword: string;
  firstName: string;
  lastName: string;
  avatarImagePath: string;
  createUserId: number;
};

export type SignUpResponseData = {
  id: number;
  userDetail: {
    id: number;
  };
};

export type SignUp = (data: SignUpRequestData) => Promise<SignUpResponseData>;

export const signUp: SignUp = async data => {
  const {
    account,
    password,
    repassword,
    firstName,
    lastName,
    avatarImagePath,
    createUserId,
  } = data;

  const {
    data: {
      id,
      userDetail,
    },
  } = await client('authentication').post(
    'api/user',
    {
      json: {
        account,
        password,
        repassword,
        firstName,
        lastName,
        avatarImagePath,
        createUserId,
      },
    },
  )
    .json<{data: SignUpResponseData}>();

  return {
    id,
    userDetail: {
      id: userDetail.id,
    },
  };
};
