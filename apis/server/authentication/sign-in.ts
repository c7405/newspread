import {client} from '@/libs/service-client';

export type SignInRequestData = {
  account: string;
  password: string;
};

export type SignInResponseData = {
  authorizationCode: string
};

export type SignIn = (data: SignInRequestData) => Promise<SignInResponseData> | never;

export const signIn: SignIn = async ({account, password}) => {
  const {data: {authorizationCode}} = await client('authentication').post(
    'api/auth/sign-in',
    {
      json: {
        account,
        password,
      },
    },
  )
    .json<{data: SignInResponseData}>();

  return {authorizationCode};
};
