import {client} from '@/libs/service-client';
import {BillingHistory, User} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryUserBillingHistoryRequestData = {
  userId: User['id'];
  year: number;
};

export type QueryUserBillingHistoryResponseData = {
  billingHistory: BillingHistory[];
};

export type QueryUserBillingHistory = (data: QueryUserBillingHistoryRequestData) => Promise<QueryUserBillingHistoryResponseData> | never;

export const queryUserBillingHistory: QueryUserBillingHistory = async ({userId, year}) => {
  const {
    data: {
      billingHistory,
    },
  } = await client('checkout').get(
    'api/billing/history',
    {
      searchParams: {
        userId,
        year,
      },
    },
  )
    .json<SuccessResponseContent<QueryUserBillingHistoryResponseData>>();

  return {
    billingHistory,
  };
};
