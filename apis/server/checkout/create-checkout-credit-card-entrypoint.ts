import {client} from '@/libs/service-client';
import {SubscriptionPlanType, CheckoutPeriodType} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type CreateCheckoutCreditCardParamsRequestData = {
  subscriptionType: SubscriptionPlanType;
  customerId: number;
  createUserId: number;
  remark?: string;
};

export type CreateCheckoutCreditCardParamsResponseData = {
  checkoutEntrypoint: string;
  checkoutCreditCardParams: {
    MerchantTradeNo: string;
    MerchantTradeDate: string;
    TotalAmount: number;
    TradeDesc: number;
    ItemName: string;
    ReturnURL: string;
    ChoosePayment: 'Credit';
    OrderResultURL: string;
    PeriodAmount: number;
    PeriodType: CheckoutPeriodType;
    Frequency: number;
    ExecTimes: number;
    PeriodReturnURL: string;
    MerchantID: number;
    PaymentType: 'aio',
    EncryptType: 1,
    DeviceSource: string,
    CheckMacValue: string;
  };
};

export type CreateCheckoutCreditCardParams = (data: CreateCheckoutCreditCardParamsRequestData) => Promise<CreateCheckoutCreditCardParamsResponseData> | never;

export const createCheckoutCreditCardEntrypoint: CreateCheckoutCreditCardParams = async data => {
  const {
    subscriptionType,
    customerId,
    createUserId,
    remark,
  } = data;

  const {
    data: {
      checkoutEntrypoint,
      checkoutCreditCardParams,
    },
  } = await client('checkout').post(
    'api/checkout/entrypoint',
    {
      json: {
        subscriptionType,
        customerId,
        createUserId,
        remark,
      },
    },
  )
    .json<SuccessResponseContent<CreateCheckoutCreditCardParamsResponseData>>();

  return {
    checkoutEntrypoint,
    checkoutCreditCardParams,
  };
};
