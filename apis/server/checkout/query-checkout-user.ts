import {client} from '@/libs/service-client';
import {User, EcpayTransaction, UserSubscription} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryCheckoutUserRequestData = {
  userId: User['id'];
};

export type QueryCheckoutUserResponseData = {
  userSubscription: UserSubscription | null;
  ecpayTransaction: EcpayTransaction | null;
};

export type QueryCheckoutUser = (data: QueryCheckoutUserRequestData) => Promise<QueryCheckoutUserResponseData> | never;

export const queryCheckoutUser: QueryCheckoutUser = async ({userId}) => {
  const {
    data: {
      userSubscription,
      ecpayTransaction,
    },
  } = await client('checkout').get(`api/checkout/user/${userId}`)
    .json<SuccessResponseContent<QueryCheckoutUserResponseData>>();

  return {
    userSubscription,
    ecpayTransaction,
  };
};
