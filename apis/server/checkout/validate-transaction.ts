import {client} from '@cornerstone/service-client';
import {pick} from 'lodash';

export type ValidateTransactionRequestData = {
  MerchantID: string;
  MerchantTradeNo: string;
  StoreID: string;
  RtnCode: string;
  RtnMsg: string;
  TradeNo: string;
  TradeAmt: string;
  PaymentDate: string;
  PaymentType: string;
  PaymentTypeChargeFee: string;
  TradeDate: string;
  SimulatePaid: string;
  CustomField1: string;
  CustomField2: string;
  CustomField3: string;
  CustomField4: string;
  CheckMacValue: string;
};

export type ValidateTransaction = (data: ValidateTransactionRequestData) => Promise<void> | never;

export const validateTransaction: ValidateTransaction = async data => {
  await client('checkout').post(
    'api/transaction/ecpay/validity',
    {
      json: pick(
        data,
        [
          'MerchantID',
          'MerchantTradeNo',
          'StoreID',
          'RtnCode',
          'RtnMsg',
          'TradeNo',
          'TradeAmt',
          'PaymentDate',
          'PaymentType',
          'PaymentTypeChargeFee',
          'TradeDate',
          'SimulatePaid',
          'CustomField1',
          'CustomField2',
          'CustomField3',
          'CustomField4',
          'CheckMacValue',
        ],
      ),
    },
  );
};
