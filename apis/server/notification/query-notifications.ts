import {client} from '@/libs/service-client';
import type {Notification} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryNotificationsRequestData = {
  userId?: Notification['userId'];
  cursor?: number;
  perPage: number;
};

export type QueryNotificationsResponseData = {
  notifications: Notification[];
  cursor?: number;
};

export type QueryNotifications = (data: QueryNotificationsRequestData) => Promise<QueryNotificationsResponseData>;

export const queryNotifications: QueryNotifications = async data => {
  const {userId, cursor, perPage} = data;

  const {
    data: {
      notifications,
      cursor: nextCursor,
    },
  } = await client('notification').get(
    'api/notifications',
    {
      searchParams: {
        userId,
        cursor,
        perPage,
      },
    },
  )
    .json<SuccessResponseContent<QueryNotificationsResponseData>>();

  return {
    notifications,
    cursor: nextCursor,
  };
};


