import {client} from '@/libs/service-client';
import {Notification} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type UpdateNotificationRequestData = {
  id: number;
  hasRead: boolean;
};

export type UpdateNotificationResponseData = {
  notification: Notification;
};

export type UpdateNotification = (data: UpdateNotificationRequestData) => Promise<UpdateNotificationResponseData>;

export const updateNotification: UpdateNotification = async ({id, hasRead}) => {
  const {
    data: {
      notification,
    },
  } = await client('notification').patch(
    `api/notification/${id}`,
    {
      json: {
        hasRead,
      },
    },
  )
    .json<SuccessResponseContent<UpdateNotificationResponseData>>();

  return {notification};
};
