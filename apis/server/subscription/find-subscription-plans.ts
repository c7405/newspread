import {client} from '@/libs/service-client';
import {SubscriptionPlan} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type FindSubscriptionPlansResponseData = {
  subscriptionPlans: SubscriptionPlan[];
};

export type FindSubscriptionPlans = () => Promise<FindSubscriptionPlansResponseData>;

export const findSubscriptionPlans: FindSubscriptionPlans = async () => {
  const {
    data: {
      subscriptionPlans,
    },
  } = await client('subscription').get('api/subscription-plan')
    .json<SuccessResponseContent<FindSubscriptionPlansResponseData>>();

  return {subscriptionPlans};
};
