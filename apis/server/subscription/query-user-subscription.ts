import {client} from '@/libs/service-client';
import type {User, UserSubscription} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type QueryUserSubscriptionRequestData = {
  userId: User['id'];
};

export type QueryUserSubscriptionResponseData = {
  userSubscription: UserSubscription;
};

export type QueryUserSubscription = (data: QueryUserSubscriptionRequestData) => Promise<QueryUserSubscriptionResponseData> | never;

export const queryUserSubscription: QueryUserSubscription = async ({userId}) => {
  const {
    data: {
      userSubscription,
    },
  } = await client('subscription').get(
    'api/user-subscription',
    {
      searchParams: {
        userId,
      },
    },
  )
    .json<SuccessResponseContent<QueryUserSubscriptionResponseData>>();

  return {userSubscription};
};
