import {client} from '@/libs/service-client';
import {Bookmark} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type DeleteBookmarkRequestData = {
  id: Bookmark['id'];
};
export type DeleteBookmarkResponseData = {
  bookmark: Bookmark;
};

export type DeleteBookmark = (data: DeleteBookmarkRequestData) => Promise<DeleteBookmarkResponseData>;

export const deleteBookmark: DeleteBookmark = async ({id}) => {
  const {
    data: {
      bookmark,
    },
  } = await client('content').delete(
    `api/bookmark/${id}`,
  )
    .json<SuccessResponseContent<DeleteBookmarkResponseData>>();

  return {bookmark};
};
