import type {Bookmark, Post, User} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';
import {client} from 'libs/service-client';

export type FindBookmarksByUserIdsRequestData = {
  userId?: User['id'];
  postIds: Post['id'][];
};

export type FindBookmarksByUserIdsResponseData = {
  bookmarks: Bookmark[];
};

export type FindBookmarksByUserIds = (data: FindBookmarksByUserIdsRequestData) => Promise<FindBookmarksByUserIdsResponseData>;

export const findBookmarksByUserIds: FindBookmarksByUserIds = async ({userId, postIds}) => {
  const {
    data: {
      bookmarks,
    },
  } = await client('content').get(
    'api/bookmarks',
    {
      searchParams: new URLSearchParams([
        ...userId ? [['userId', `${userId}`]] : [],
        ...postIds.map(postId => ['postIds', `${postId}`]),
      ]),
    },
  )
    .json<SuccessResponseContent<FindBookmarksByUserIdsResponseData>>();

  return {bookmarks};
};
