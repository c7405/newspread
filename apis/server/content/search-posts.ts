import {client} from '@/libs/service-client';
import type {Bookmark, Post, PostSearchMode} from '@/types/resources';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import {onlyTruthyValue} from '@/libs/object';

export type SearchPostsRequestData<T extends PostSearchMode> = {
  keyword?:string;
  bookmarkUserId: T extends 'ALL'
    ? number | undefined
    : T extends 'USER_BOOKMARKED'
      ? number
      : never;
  cursor?: number;
  mode: T;
};

export type SearchPostsResponseData = {
  bookmarks: Bookmark[];
  posts: Post[];
  cursor?:number;
};

export async function searchPosts<T extends PostSearchMode>(data: SearchPostsRequestData<T>) {
  const {
    keyword,
    bookmarkUserId,
    cursor,
    mode = 'ALL',
  } = data;
  const {
    data: {
      bookmarks,
      posts,
      cursor: nextCursor,
    },
  } = await client('content').get(
    'api/posts',
    {
      searchParams: {
        ...onlyTruthyValue({
          keyword,
          bookmarkUserId: mode === 'ALL' ? undefined : bookmarkUserId,
          cursor,
        }),
        perPage: 20,
      },
    },
  )
    .json<SuccessResponseContent<SearchPostsResponseData>>();

  return {
    bookmarks,
    posts,
    cursor: nextCursor,
  };
}

