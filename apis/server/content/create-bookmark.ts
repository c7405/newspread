import {client} from '@/libs/service-client';
import {Bookmark, Post, User} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';

export type CreateBookmarkRequestData = {
  userId: User['id'];
  postId: Post['id'];
};

export type CreateBookmarkResponseData = {
  bookmark: Bookmark;
};

export type CreateBookmark = (data: CreateBookmarkRequestData) => Promise<CreateBookmarkResponseData>;

export const createBookmark: CreateBookmark = async ({userId, postId}) => {
  const {
    data: {
      bookmark,
    },
  } = await client('content').post(
    'api/bookmark',
    {
      json: {
        userId,
        postId,
      },
    },
  )
    .json<SuccessResponseContent<CreateBookmarkResponseData>>();

  return {bookmark};
};
