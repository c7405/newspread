import type {Post, PostWithContentAuthor} from '@/types/resources';
import {SuccessResponseContent} from '@cornerstone/response-formatter';
import {client} from '@cornerstone/service-client';

export type FindPostByTitleRequestData = {
  title: Post['title'];
};

export type FindPostByTitleResponseData = {
  post: PostWithContentAuthor;
};

export type FindPostByTitle = (data: FindPostByTitleRequestData) => Promise<FindPostByTitleResponseData>;

export const findPostByTitle: FindPostByTitle = async ({title}) => {
  const {
    data: {
      post,
    },
  } = await client('content').get(`api/post/title/${title}`)
    .json<SuccessResponseContent<FindPostByTitleResponseData>>();

  return {post};
};
