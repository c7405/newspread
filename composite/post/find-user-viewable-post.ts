import {findPostByTitle} from '@/apis/server/content';
import {FindPostByTitleRequestData} from '@/apis/server/content/find-post-by-title';
import {queryUserSubscription} from '@/apis/server/subscription';
import type {PostWithContentAuthor, User, UserSubscription} from '@/types/resources';
import {surppressNotFoundError} from '@/utils/server/api';
import {fetchUser} from '@/utils/server/refresh-token';

export type FindUserViewablePostReqeustData = FindPostByTitleRequestData & {
  refreshToken: string;
};

export type FindUserViewablePostResponseData = {
  post: PostWithContentAuthor;
  user: User | null;
  userSubscription: UserSubscription | null;
};

export type FindUserViewablePost = (data: FindUserViewablePostReqeustData) => Promise<FindUserViewablePostResponseData>;

export const findUserViewablePost: FindUserViewablePost = async ({title, refreshToken}) => {
  const [{post}, user] = await Promise.all([
    findPostByTitle({title}),
    fetchUser(refreshToken),
  ]);

  let userSubscription: UserSubscription | null = null;

  if (post.exclusive) {
    if (user) {
      ({userSubscription} = await surppressNotFoundError(
        () => queryUserSubscription({userId: user.id}),
        {userSubscription: null},
      ));
    }

    if (!userSubscription) {
      post.content = post.content.slice(0, 3);
    }
  }

  return {post, user, userSubscription};
};
