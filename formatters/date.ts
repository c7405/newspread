import dayjs from 'dayjs';
import type {ConfigType} from 'dayjs';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);

export type FormatDate = (date: ConfigType) => string;

export const formatDate: FormatDate = date => dayjs.utc(date).format('YYYY.MM.DD');
