import {HumanReadDiffMeta, HumanReadDiffMetaUnit} from '@/libs/time';

export type AcceptLocale = 'en';

export type LocaleTemplateMap = {
  [locale in AcceptLocale]: {
    [unit in HumanReadDiffMetaUnit]: {
      zero: string;
      positive: string;
      negative: string;
    };
  };
};

export type LocalePostProcessMap = {
  [locale in AcceptLocale]?: (message: string, metaData: HumanReadDiffMeta) => string;
};

const localeTemplateMap: LocaleTemplateMap = {
  en: {
    day: {
      zero: 'Today',
      positive: '%d day%s later',
      negative: '%d day%s ago',
    },
    week: {
      zero: 'This Week',
      positive: '%d week%s later',
      negative: '%d week%s ago',
    },
    month: {
      zero: 'This Month',
      positive: '%d month%s later',
      negative: '%d month%s ago',
    },
    year: {
      zero: 'This Year',
      positive: '%d year%s later',
      negative: '%d year%s ago',
    },
  },
};

export const localePostProcessMap: LocalePostProcessMap = {
  en: (message, {value}) => message.replace('%s', Math.abs(value) === 1 ? '' : 's'),
};

export type FormatTimeRange = (metaData: HumanReadDiffMeta, locale: AcceptLocale) => string;

export const formatTimeRange: FormatTimeRange = (metaData, locale) => {
  const {unit, value} = metaData;

  let message = '';

  if (value === 0) {
    message = localeTemplateMap[locale][unit].zero;
  }

  if (value > 0) {
    message = localeTemplateMap[locale][unit].positive;
  }

  if (value < 0) {
    message = localeTemplateMap[locale][unit].negative;
  }

  message = message.replace('%d', `${value > 0 ? value : Math.abs(value)}`);

  const postProcessor = localePostProcessMap[locale];

  return postProcessor ? postProcessor(message, metaData) : message;
};
