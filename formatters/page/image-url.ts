import type {AppImageFormat} from '@/types/resources';

export type CreateImageServiceLoaderUrlPayload = {
  src: string;
  width: number;
  quality?: number;
};

export type CreateImageServiceLoaderUrlLoader = (format: AppImageFormat) => (payload: CreateImageServiceLoaderUrlPayload) => string;

export const createImageServiceLoaderUrlLoader: CreateImageServiceLoaderUrlLoader = format => payload => {
  const {src, width, quality = 75} = payload;
  const url = new URL(src);
  const {searchParams} = url;

  searchParams.append('width', `${width}`);
  searchParams.append('quality', `${quality}`);
  searchParams.append('format', format);

  return `${url}`;
};
