import type {Author} from '@/types/resources';

export type FormatAuthorsName = (authors: Author[]) => string;

export const formatAuthorsName: FormatAuthorsName = authors => (
  authors.map(({firstName, lastName}) => `${firstName} ${lastName}`).join(', ')
);
