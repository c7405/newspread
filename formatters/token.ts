export type CreateBearerToken = (token: string) => string;

export const createBearerToken: CreateBearerToken = token => `Bearer ${token}`;

export type ParseBearerToken = (authorization: string) => string;

export const parseBearerToken: ParseBearerToken = authorization => {
  const prefix = 'Bearer ';

  return !authorization || !authorization.startsWith(prefix)
    ? authorization
    : authorization.slice(prefix.length);
};
