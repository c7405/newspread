import {AppLayout} from '@/components/layout';
import {ViewUserProfile} from '@/components/pages/home/view-user-profile';
import type {UserProps} from '@/utils/server/with-user-props';
import {createGetServerSideProps} from '@/utils/server/with-user-props';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {SWRConfig} from 'swr';

export type UserProfileProps = UserProps;

export const getServerSideProps: GetServerSideProps<UserProfileProps> = createGetServerSideProps();

const UserProfile: NextPage<UserProfileProps> = ({user}) => (
  <SWRConfig value={{
    fallback: {
      'api/auth/user': user,
    },
  }}>
    <AppLayout activeType="user-profile">
      <Head>
        <title>User Profile | Newspread</title>
      </Head>
      <ViewUserProfile />
    </AppLayout>
  </SWRConfig>
);

export default UserProfile;
