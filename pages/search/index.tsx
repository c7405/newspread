import {AppLayout} from '@/components/layout';
import {SearchPost} from '@/components/pages/home/search-post';
import {baseUrl} from '@/libs/app';
import type {UserProps} from '@/utils/server/with-user-props';
import {createGetServerSideProps} from '@/utils/server/with-user-props';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {SWRConfig} from 'swr';

export const getServerSideProps: GetServerSideProps<UserProps> = createGetServerSideProps();

const Search: NextPage<UserProps> = ({user}) => (
  <SWRConfig value={{
    fallback: {
      'api/auth/user': user,
    },
  }}>
    <AppLayout activeType="search">
      <Head>
        <title>Search | Newspread</title>
        <meta
          name="description"
          content="Search the whole news database with the keyword related to the title and content body." />
        <meta
          property="og:type"
          content="website" />
        <meta
          property="og:site_name"
          content="Newspread" />
        <meta
          property="og:url"
          content={`${baseUrl('search')}`} />
        <meta
          property="og:title"
          content="Search | Newspread" />
        <meta
          property="og:description"
          content="Search the whole news database with the keyword related to the title and content body." />
        <meta
          property="og:image"
          content={baseUrl('icon-512.png')} />
        <meta
          property="og:image:width"
          content="512" />
        <meta
          property="og:image:height"
          content="512" />
      </Head>
      <SearchPost />
    </AppLayout>
  </SWRConfig>
);

export default Search;
