import {findBookmarksByUserIds} from '@/apis/server/content';
import {FullContentLoadingIndicator} from '@/components/base';
import {AppLayout} from '@/components/layout';
import {PostViewer, SubscriptionCover} from '@/components/pages/post/base';
import type {PostViewerProps} from '@/components/pages/post/base/PostViewer';
import type {SubscriptionCoverProps} from '@/components/pages/post/base/SubscriptionCover';
import {findUserViewablePost} from '@/composite/post';
import {
  usePostShare,
  usePostView,
  useSingleBookmarkProcess,
  useUserSubscription,
} from '@/hooks/page';
import {baseUrl} from '@/libs/app';
import type {
  Bookmark,
  PostWithContentAuthor,
  User,
  UserSubscription,
} from '@/types/resources';
import {AppError} from '@cornerstone/app-error';
import {StatusCodes} from 'http-status-codes';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {useRouter} from 'next/router';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type PostTitleProps = {
  post: PostWithContentAuthor;
  user: User | null;
  bookmark: Bookmark | null;
  userSubscription: UserSubscription | null;
};

export const getServerSideProps: GetServerSideProps<PostTitleProps> = async ({req, query}) => {
  const {title} = query;

  if (!title || typeof title !== 'string') {
    throw new AppError(
      undefined,
      'POST_NOT_FOUND',
      undefined,
      StatusCodes.NOT_FOUND,
    );
  }

  const {post, user, userSubscription} = await findUserViewablePost({
    title: `${query.title}`,
    refreshToken: req.cookies['refresh-token'],
  });

  let bookmark: PostTitleProps['bookmark'] | null = null;

  if (user) {
    ({bookmarks: [bookmark = null]} = await findBookmarksByUserIds({
      userId: user.id,
      postIds: [post.id],
    }));
  }

  return {
    props: {
      user,
      post,
      bookmark,
      userSubscription,
    },
  };
};

const PostTitle: NextPage<PostTitleProps> = props => {
  const {
    user,
    post: initPost,
    bookmark: initBookmark,
    userSubscription: initUserSubscription,
  } = props;

  const router = useRouter();

  const {
    userSubscription,
    isLoading: isUserSubscriptionLoading,
  } = useUserSubscription({
    userId: user?.id ?? undefined,
    fallbackData: {
      userSubscription: initUserSubscription,
    },
  });

  const {
    post,
    isLoading: isPostLoading,
  } = usePostView({
    title: initPost.title,
    fallbackData: {
      post: initPost,
    },
  });

  const {
    bookmark,
    isLoading: isBookmarkLoading,
    onBookmarkProcess,
  } = useSingleBookmarkProcess({
    user,
    postId: post?.id,
    fallbackData: {
      bookmark: initBookmark,
    },
  });

  const {onPostShare} = usePostShare();

  const isDateLoading = (
    isPostLoading ||
    isBookmarkLoading ||
    isUserSubscriptionLoading
  );

  const {
    id: postId,
    coverImageWidth,
    coverImageHeight,
    coverImageSrc,
    title,
    exclusive,
    readTime,
    updateDate,
    content,
    authors,
  } = post ?? initPost;

  const onShareClick: PostViewerProps['onShareClick'] = () => {
    onPostShare(title);
  };
  const onBookmarkClick: PostViewerProps['onBookmarkClick'] = () => {
    onBookmarkProcess(postId, bookmark?.id);
  };

  const onSubscribePageRedirect: SubscriptionCoverProps['onSubscribe'] = () => {
    router.push('/subscribe');
  };

  const PostFooter: VFC = () => (
    exclusive && !userSubscription
      ? (
        <footer className={styles.footer}>
          <SubscriptionCover onSubscribe={onSubscribePageRedirect} />
        </footer>
      )
      : null
  );

  const ogImagePathUrl = new URL(coverImageSrc);
  const {searchParams} = ogImagePathUrl;
  const ogImageWidth = 768;
  const opImageHeight = Math.round(768 / coverImageWidth * coverImageHeight);

  searchParams.append('format', 'WEBP');
  searchParams.append('width', `${ogImageWidth}`);
  searchParams.append('quality', '75');

  return (
    <AppLayout>
      <Head>
        <title>{title} | Newspread</title>
        <meta
          name="description"
          content={title} />
        <meta
          property="og:type"
          content="article" />
        <meta
          property="og:url"
          content={baseUrl(`/post/${title}`)} />
        <meta
          property="og:title"
          content={title} />
        <meta
          property="og:description"
          content={title} />
        <meta
          property="og:image"
          content={`${ogImagePathUrl}`} />
        <meta
          property="og:image:width"
          content={`${ogImageWidth}`} />
        <meta
          property="og:image:height"
          content={`${opImageHeight}`} />
      </Head>
      {
        isDateLoading
          ? (
            <FullContentLoadingIndicator />
          )
          : (
            <div className={styles.main}>
              <PostViewer
                coverImageWidth={coverImageWidth}
                coverImageHeight={coverImageHeight}
                coverImageSrc={coverImageSrc}
                title={title}
                exclusive={exclusive}
                authors={authors}
                readTime={readTime}
                updateDate={updateDate}
                content={content}
                allowBookmark={!!user}
                bookmarked={!!bookmark}
                onShareClick={onShareClick}
                onBookmarkClick={onBookmarkClick} />
              <PostFooter />
            </div>
          )
      }
    </AppLayout>
  );
};

export default PostTitle;
