import {signUp} from '@/apis/client/authentication';
import {AuthenticationLayout} from '@/components/layout';
import {SignUpForm} from '@/components/pages/auth/sign-up';
import type {SignUpFormProps} from '@/components/pages/auth/sign-up/SignUpForm';
import type {GetServerSideProps, InferGetServerSidePropsType} from 'next';
import Head from 'next/head';
import router from 'next/router';

export const getServerSideProps: GetServerSideProps = async ({req}) => {
  if (req.cookies['refresh-token']) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const AuthSignUpPage: InferGetServerSidePropsType<typeof getServerSideProps> = () => {
  const onSignUpFormSubmit: SignUpFormProps['onSubmit'] = async data => {
    const {
      account,
      password,
      repassword,
      firstName,
      lastName,
    } = data;

    await signUp({
      account,
      password,
      repassword,
      firstName,
      lastName,
    });

    router.push('/auth/sign-in');
  };

  return (
    <AuthenticationLayout
      title="Sign Up"
      subTitle="Create a new account">
      <Head>
        <title>Sign Up | Newspread</title>
        <meta
          name="description"
          content="Come and join us! Become a member of newspread." />
        <meta
          property="og:title"
          content="Sign Up" />
        <meta
          property="og:description"
          content="Come and join us! Become a member of newspread." />
        <meta
          property="og:image"
          content="/image/logo.svg" />
      </Head>
      <SignUpForm onSubmit={onSignUpFormSubmit} />
    </AuthenticationLayout>
  );
};

export default AuthSignUpPage;
