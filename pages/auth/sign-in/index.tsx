import {
  queryFacebookAuthorizationUrl,
  queryGoogleAuthorizationUrl,
  signIn,
} from '@/apis/client/authentication';
import {Anchor} from '@/components/base';
import {AuthenticationLayout} from '@/components/layout';
import {SignInForm, SocialNetworkButtonSet} from '@/components/pages/auth/sign-in';
import type {OnSignInCredentialSubmit} from '@/components/pages/auth/sign-in/SignInForm';
import type {SocialNetworkButtonSetProps} from '@/components/pages/auth/sign-in/SocialNetworkButtonSet';
import {useAppErrorModal} from '@/hooks/app';
import {setAccessToken} from '@/utils/client/access-token';
import type {GetServerSideProps, InferGetServerSidePropsType} from 'next';
import Head from 'next/head';
import Link from 'next/link';
import router from 'next/router';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export const getServerSideProps: GetServerSideProps = async ({req}) => {
  if (req.cookies['refresh-token']) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const AuthSignInPage: InferGetServerSidePropsType<typeof getServerSideProps> = () => {
  const {appendToMessageMap} = useAppErrorModal();

  const UserSignInFailedMessage: VFC = () => (
    <div>
      Please check your credential data and retry, if you forgot your password, you can try {' '}
      <Link
        href="/auth/forgot-password"
        passHref>
        <Anchor>recover the password</Anchor>
      </Link>
      .
    </div>
  );

  appendToMessageMap(
    'USER_SIGN_IN_FAILED',
    UserSignInFailedMessage,
  );

  const onSignInFormSubmit: OnSignInCredentialSubmit = async ({account, password}) => {
    const {accessToken} = await signIn({account, password});

    setAccessToken(accessToken);

    const redirectToUrl = (new URLSearchParams(window.location.search)).get('redirectTo');

    if (redirectToUrl && redirectToUrl.startsWith('/')) {
      router.push(redirectToUrl);

      return;
    }

    router.push('/');
  };

  const onGoogleSignIn: SocialNetworkButtonSetProps['onGoogleSignIn'] = async () => {
    const {authorizationUrl} = await queryGoogleAuthorizationUrl();

    window.location.assign(authorizationUrl);
  };

  const onFacebookSignIn: SocialNetworkButtonSetProps['onFacebookSignIn'] = async () => {
    const {authorizationUrl} = await queryFacebookAuthorizationUrl();

    window.location.assign(authorizationUrl);
  };

  return (
    <AuthenticationLayout
      title="Welcome Back!"
      subTitle="Please enter your credential to access your account">
      <Head>
        <title>Sign In | Newspread</title>
        <meta
          name="description"
          content="Sign in to newspread, and enjoy all the reading experience from our service." />
        <meta
          property="og:title"
          content="Sign In" />
        <meta
          property="og:description"
          content="Sign in to newspread, and enjoy all the reading experience from our service." />
        <meta
          property="og:image"
          content="/image/logo.svg" />
      </Head>
      <div className={styles.containerSignInForm}>
        <div className={styles.containerSocialNetworkButtonSet}>
          <SocialNetworkButtonSet
            onGoogleSignIn={onGoogleSignIn}
            onFacebookSignIn={onFacebookSignIn} />
        </div>
        <div>
          <span className={styles.signInTypeSeparator}>or</span>
        </div>
        <div>
          <SignInForm onSubmit={onSignInFormSubmit} />
        </div>
        <div className={styles.containerSignUpNotice}>
          {'Don\'t have an account? '}
          <Link
            href="/auth/sign-up"
            passHref>
            <Anchor>Sign Up</Anchor>
          </Link>
          {' '}now!
        </div>
        <div className={styles.containerForgotPasswordNotice}>
          <Link
            href="/auth/forgot-password"
            passHref>
            <Anchor>Forgot Password?</Anchor>
          </Link>
        </div>
      </div>
    </AuthenticationLayout>
  );
};

export default AuthSignInPage;
