import {createUserNewPassword} from '@/apis/client/authentication';
import {Anchor} from '@/components/base';
import {AuthenticationLayout} from '@/components/layout';
import {CreateNewPasswordForm} from '@/components/pages/auth/forgot-password';
import type {CreateNewPasswordFormProps} from '@/components/pages/auth/forgot-password/CreateNewPasswordForm';
import classnames from 'classnames';
import {Checkmark} from 'grommet-icons';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {useEffect, useState} from 'react';
import styles from './styles.module.scss';

export const getServerSideProps: GetServerSideProps<Record<string, never>> = async ctx => {
  if (!ctx.query.authorizationCode) {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const AuthCreateNewPassword: NextPage = () => {
  const [isUserNewPasswordCreated, setIsUserNewPasswordCreated] = useState<boolean>(false);
  const router = useRouter();

  const onNewPasswordFormSubmit: CreateNewPasswordFormProps['onSubmit'] = async data => {
    const {newPassword, confirmNewPassword} = data;
    const authorizationCode = (new URLSearchParams(window.location.search)).get('authorizationCode');

    await createUserNewPassword({
      newPassword,
      confirmNewPassword,
      authorizationCode: authorizationCode as unknown as string,
    });

    setIsUserNewPasswordCreated(true);
  };

  useEffect(() => {
    if (!isUserNewPasswordCreated) return;

    const timerId = window.setTimeout(() => {
      router.replace('/auth/sign-in');
    }, 3000);

    return () => {
      window.clearTimeout(timerId);
    };
  }, [router, isUserNewPasswordCreated]);

  const layoutTitle = isUserNewPasswordCreated
    ? 'User password is renewed'
    : 'Create new password';
  const layoutSubTitle = isUserNewPasswordCreated
    ? 'Your user password has been updated successfully, please sign in with the new password. This page will redirect to sign in page automatically in 3 seconds'
    : 'For security\'s sake, please enter a new password which is different from the previous one.';

  return (
    <AuthenticationLayout
      title={layoutTitle}
      subTitle={layoutSubTitle}>
      <Head>
        <title>Create New Password | Newspread</title>
      </Head>
      <section className={styles.main}>
        <div className={
          classnames(
            styles.containerBodyContent,
            {[styles.active]: isUserNewPasswordCreated},
          )
        }>
          <div className={styles.containerCheckmarkIcon}>
            <Checkmark color="plain" />
          </div>
          <div className={styles.containerBackToSignIn}>
            <Link
              href="/auth/sign-in"
              passHref>
              <Anchor>Back to Sign In</Anchor>
            </Link>
          </div>
        </div>
        <div className={
          classnames(
            styles.containerBodyContent,
            styles.containerCreateNewPasswordForm,
            {[styles.active]: !isUserNewPasswordCreated},
          )
        }>
          <CreateNewPasswordForm onSubmit={onNewPasswordFormSubmit} />
        </div>
      </section>
    </AuthenticationLayout>
  );
};

export default AuthCreateNewPassword;
