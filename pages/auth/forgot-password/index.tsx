import {createUserResetPasswordCode} from '@/apis/client/authentication';
import {Anchor} from '@/components/base';
import {AuthenticationLayout} from '@/components/layout';
import {ResetPasswordForm} from '@/components/pages/auth/reset-password';
import type {ResetPasswordFormProps} from '@/components/pages/auth/reset-password/ResetPasswordForm';
import classnames from 'classnames';
import {Send} from 'grommet-icons';
import {NextPage} from 'next';
import Head from 'next/head';
import Link from 'next/link';
import {useState} from 'react';
import styles from './styles.module.scss';

const AuthResetPasswordPage: NextPage = () => {
  const [isResetPasswordCodeCreated, setIsResetPasswordCodeCreated] = useState<boolean>(false);
  const layoutTitle = isResetPasswordCodeCreated
    ? 'Follow the instructions in email'
    : 'Reset Password';
  const layoutSubTitle = isResetPasswordCodeCreated
    ? 'Please check your email inbox and follow the steps in the mail to reset your password. If you could not receive the mail, please check your spam box or resend the mail again.'
    : 'Please enter the account which you want to reset the password for, and you will receive an email of instruction to reset your password.';

  const onResetPasswordFormSubmit: ResetPasswordFormProps['onSubmit'] = async ({account}) => {
    await createUserResetPasswordCode({account});

    setIsResetPasswordCodeCreated(true);
  };

  return (
    <AuthenticationLayout
      title={layoutTitle}
      subTitle={layoutSubTitle}>
      <Head>
        <title>Reset Password | Newspread</title>
        <meta
          name="description"
          content="Follow the instruction to reset member password." />
        <meta
          property="og:title"
          content="Sign Up" />
        <meta
          property="og:description"
          content="Follow the instruction to reset member password." />
        <meta
          property="og:image"
          content="/image/logo.svg" />
      </Head>
      <section className={styles.main}>
        <div className={styles.body}>
          <div className={
            classnames(
              styles.containerIconSend,
              styles.containerBodyContent,
              {[styles.active]: isResetPasswordCodeCreated},
            )}>
            <Send color="plain" />
          </div>
          <div className={
            classnames(
              styles.containerBodyContent,
              styles.containerResetPasswordForm,
              {[styles.active]: !isResetPasswordCodeCreated},
            )
          }>
            <ResetPasswordForm onSubmit={onResetPasswordFormSubmit} />
          </div>
        </div>
        <div className={styles.containerBackToSignIn}>
          <Link
            href="/auth/sign-in"
            passHref>
            <Anchor>Back to Sign In</Anchor>
          </Link>
        </div>
      </section>
    </AuthenticationLayout>
  );
};

export default AuthResetPasswordPage;
