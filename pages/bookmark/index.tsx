import {AppLayout} from '@/components/layout';
import {SearchBookmark} from '@/components/pages/home/search-bookmark';
import type {UserProps} from '@/utils/server/with-user-props';
import {createGetServerSideProps} from '@/utils/server/with-user-props';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {SWRConfig} from 'swr';

export const getServerSideProps: GetServerSideProps<UserProps> = createGetServerSideProps();

const Bookmark: NextPage<UserProps> = ({user}) => (
  <SWRConfig value={{
    fallback: {
      'api/auth/user': user,
    },
  }}>
    <AppLayout activeType="bookmark">
      <Head>
        <title>Bookmark | Newspread</title>
      </Head>
      <SearchBookmark />
    </AppLayout>
  </SWRConfig>
);

export default Bookmark;
