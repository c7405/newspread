import {validateTransaction} from '@/apis/server/checkout';
import type {ValidateTransactionRequestData} from '@/apis/server/checkout/validate-transaction';
import {Anchor, IconTextHeader} from '@/components/base';
import {AppLayout} from '@/components/layout';
import {Checkmark} from 'grommet-icons';
import pick from 'lodash.pick';
import type {GetServerSideProps, NextPage} from 'next';
import {handle, json} from 'next-runtime';
import Head from 'next/head';
import Link from 'next/link';
import type {ParsedUrlQuery} from 'querystring';
import styles from './styles.module.scss';

export type PaymentResultProps = {
  paymentResult: boolean;
};

export const getServerSideProps: GetServerSideProps<PaymentResultProps> = handle<PaymentResultProps, ParsedUrlQuery, ValidateTransactionRequestData>({
  async post({req: {body}}) {
    let paymentResult = false;

    try {
      await validateTransaction(
        pick(
          body,
          [
            'MerchantID',
            'MerchantTradeNo',
            'StoreID',
            'RtnCode',
            'RtnMsg',
            'TradeNo',
            'TradeAmt',
            'PaymentDate',
            'PaymentType',
            'PaymentTypeChargeFee',
            'TradeDate',
            'SimulatePaid',
            'CustomField1',
            'CustomField2',
            'CustomField3',
            'CustomField4',
            'CheckMacValue',
          ],
        ),
      );

      paymentResult = +body.RtnCode === 1;
    } catch (e) {
      console.error(e);
    }

    return json({
      paymentResult,
    });
  },
});

const PaymentResult: NextPage<PaymentResultProps> = ({paymentResult}) => {
  const textHeaderContent = paymentResult ?
    {
      title: 'Payment Success',
      subTitle: 'Congradulation! Your new plan was confirmed. You can check the order info for more detail in the user tab.',
    } :
    {
      title: 'Payment Failed',
      subTitle: 'We\'re sorry. An error occured while processing your order. Please try again.',
    };

  return (
    <AppLayout>
      <Head>
        <title>Payment Result | Newspread</title>
      </Head>
      <div className={styles.main}>
        <IconTextHeader
          icon={Checkmark}
          title={textHeaderContent.title}
          subTitle={textHeaderContent.subTitle} />
        <Link
          href="/"
          passHref>
          <Anchor>Back to Home</Anchor>
        </Link>
      </div>
    </AppLayout>
  );
};

export default PaymentResult;


