import {
  findBookmarksByUserIds as serverSideFindBookmarksByUserIds,
  searchPosts as serverSideSearchPosts,
} from '@/apis/server/content';
import {AppLayout} from '@/components/layout';
import {ViewPost} from '@/components/pages/home/view-post';
import {baseUrl} from '@/libs/app';
import type {Bookmark, Post} from '@/types/resources';
import {fetchUser} from '@/utils/server/refresh-token';
import {UserProps} from '@/utils/server/with-user-props';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {SWRConfig} from 'swr';

export type HomeIndexProps = UserProps & {
  bookmarks: Bookmark[];
  posts: Post[];
  cursor?: number;
};

export const getServerSideProps: GetServerSideProps<HomeIndexProps> = async ({req}) => {
  const user = await fetchUser(req.cookies['refresh-token']);
  const bookmarkUserId = user?.id;

  const {
    posts: posts = [],
    cursor: cursor,
  } = await serverSideSearchPosts({
    bookmarkUserId,
    mode: 'ALL',
  });

  const {bookmarks} = await serverSideFindBookmarksByUserIds({
    userId: bookmarkUserId,
    postIds: posts.map(({id}) => id),
  });

  return {
    props: {
      user,
      bookmarks,
      posts,
      cursor: cursor ? +cursor : undefined,
    },
  };
};

const Home: NextPage<HomeIndexProps> = props => {
  const {
    user,
    posts,
    cursor,
    bookmarks,
  } = props;

  return (
    <SWRConfig value={{
      fallback: {
        'api/auth/user': user,
      },
    }}>
      <AppLayout activeType="home">
        <Head>
          <title>Home | Newspread</title>
          <meta
            name="description"
            content="News aggregator for various categories which provides every kinds of fresh new information and solid reviews." />
          <meta
            property="og:type"
            content="website" />
          <meta
            property="og:site_name"
            content="Newspread" />
          <meta
            property="og:url"
            content={`${baseUrl('home')}`} />
          <meta
            property="og:title"
            content="Home | Newspread" />
          <meta
            property="og:description"
            content="News aggregator for various categories which provides every kinds of fresh new information and solid reviews." />
          <meta
            property="og:image"
            content={baseUrl('icon-512.png')} />
          <meta
            property="og:image:width"
            content="512" />
          <meta
            property="og:image:height"
            content="512" />
        </Head>
        <ViewPost
          bookmarks={bookmarks}
          posts={posts}
          cursor={cursor} />
      </AppLayout>
    </SWRConfig>
  );
};

export default Home;
