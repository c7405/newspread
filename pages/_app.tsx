import {AppErrorContainer} from '@/components/base';
import '@/styles/_base.scss';
import type {AppProps} from 'next/app';
import Head from 'next/head';

const App: (props: AppProps) => JSX.Element = ({Component, pageProps}) => (
  <>
    <Head>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1" />
      <link
        rel="icon"
        href="/favicon.ico"
        sizes="any" />
      <link
        rel="icon"
        href="/favicon.svg"
        type="image/svg+xml" />
      <link
        rel="mask-icon"
        href="/favicon.svg"
        color="#000000" />
      <link
        rel="apple-touch-icon"
        href="/apple-touch-icon.png" />
    </Head>
    <AppErrorContainer>
      <Component {...pageProps} />
    </AppErrorContainer>
  </>
);

export default App;
