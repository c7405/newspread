import {createCheckoutCreditCardEntrypoint} from '@/apis/client/checkout';
import {FullContentLoadingIndicator} from '@/components/base';
import {AppLayout} from '@/components/layout';
import {SubscriptionPlanEditor} from '@/components/pages/subscribe/base';
import type {SubscriptionPlanEditorProps} from '@/components/pages/subscribe/base/SubscriptionPlanEditor';
import {useUser} from '@/hooks/app';
import {useCheckoutUser, useSubscriptionPlans} from '@/hooks/page';
import {submitCheckoutForm} from '@/utils/client/checkout-form';
import type {GetServerSideProps, NextPage} from 'next';
import Head from 'next/head';
import {SWRConfig} from 'swr';
import styles from './styles.module.scss';

export const getServerSideProps: GetServerSideProps = async ({req}) => {
  const refreshToken = req.cookies['refresh-token'];

  if (!refreshToken) {
    const redirectSearchParams = new URLSearchParams({
      redirectTo: '/subscribe',
    });

    return {
      redirect: {
        destination: `/auth/sign-in?${redirectSearchParams}`,
        permanent: false,
      },
    };
  }

  return {props: {}};
};

const Subscribe: NextPage = () => {
  const {
    user,
    isLoading: isUserLoading,
  } = useUser();

  const {
    userSubscription,
    isLoading: isCheckoutUserLoading,
  } = useCheckoutUser({user});

  const {
    subscriptionPlanMap,
    isLoading: isSubscriptionPlansLoading,
  } = useSubscriptionPlans();

  const isPageDataLoading = isUserLoading ||
    isCheckoutUserLoading ||
    isSubscriptionPlansLoading;

  const onSubscriptionPlanCheckout: SubscriptionPlanEditorProps['onCheckout'] = async ({subscriptionPlan}) => {
    const {checkoutEntrypoint, checkoutCreditCardParams} = await createCheckoutCreditCardEntrypoint({
      subscriptionType: subscriptionPlan,
    });

    submitCheckoutForm(checkoutEntrypoint, checkoutCreditCardParams);
  };

  return (
    <SWRConfig value={{
      fallback: {
        'api/auth/user': user,
      },
    }}>
      <AppLayout>
        <Head>
          <title>Subscribe | Newspread</title>
        </Head>
        <div className={styles.main}>
          {
            isPageDataLoading
              ? (
                <FullContentLoadingIndicator />
              )
              : (
                subscriptionPlanMap && (
                  <div className={styles.containerSubscriptionPlanEditor}>
                    <SubscriptionPlanEditor
                      headerTitle="Subscribe"
                      subscriptionPlan={userSubscription?.subscriptionPlanType ?? null}
                      subscriptionPlanMap={subscriptionPlanMap}
                      onCheckout={onSubscriptionPlanCheckout} />
                  </div>
                )
              )
          }
        </div>
      </AppLayout>
    </SWRConfig>
  );
};

export default Subscribe;
