import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {createBookmarkSchema, findBookmarkSchema} from '@/validators/content';
import {onBookmarkFind} from './get';
import {onBookmarkCreate} from './post';

export default createRouter({
  POST: [
    withUserAccessTokenValidation,
    createValidateSchema(createBookmarkSchema, 'body'),
    onBookmarkCreate,
  ],
  GET: [
    withUserAccessTokenValidation,
    createValidateSchema(findBookmarkSchema, 'query'),
    onBookmarkFind,
  ],
});
