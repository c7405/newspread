import {createBookmark} from '@/apis/server/content';
import {decryptValue} from '@/libs/cookie';
import {errorBoundary} from '@/utils/server/error-boundary';
import {parseUserId} from '@/utils/server/refresh-token';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onBookmarkCreate: NextApiHandler = errorBoundary(async (req, res) => {
  const {bookmark} = await createBookmark({
    userId: parseUserId(
      decryptValue(req.cookies['refresh-token']),
    ),
    postId: +req.body.postId,
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({bookmark}),
    );
});
