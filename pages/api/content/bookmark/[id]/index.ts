import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {deleteBookmarkSchema} from '@/validators/content';
import {onBookmarkDelete} from './delete';

export default createRouter({
  DELETE: [
    withUserAccessTokenValidation,
    createValidateSchema(deleteBookmarkSchema, 'query'),
    onBookmarkDelete,
  ],
});
