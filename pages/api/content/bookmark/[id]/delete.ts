import {deleteBookmark} from '@/apis/server/content';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onBookmarkDelete: NextApiHandler = errorBoundary(async (req, res) => {
  const {id} = req.query;

  const {bookmark} = await deleteBookmark({
    id: +`${id}`,
  });

  res.status(StatusCodes.OK)
    .json(successResponse({bookmark}));
});
