import {findBookmarksByUserIds} from '@/apis/server/content';
import {successResponse} from '@cornerstone/response-formatter';
import type {SuccessResponseContent} from '@cornerstone/response-formatter';
import type {NextApiHandler} from 'next';
import type {Bookmark} from '@/types/resources';
import {StatusCodes} from 'http-status-codes';
import {parseUserId} from '@/utils/server/refresh-token';
import {decryptValue} from '@/libs/cookie';
import {errorBoundary} from '@/utils/server/error-boundary';

export type OnBookmarkFindResponseData = {
  bookmark: Bookmark | null;
};

export const onBookmarkFind: NextApiHandler<SuccessResponseContent<OnBookmarkFindResponseData>> = errorBoundary(async (req, res) => {
  const refreshToken = req.cookies['refresh-token'];

  let bookmark: OnBookmarkFindResponseData['bookmark'] = null;

  if (refreshToken) {
    ({bookmarks: [bookmark]} = await findBookmarksByUserIds({
      userId: parseUserId(
        decryptValue(refreshToken),
      ),
      postIds: [+`${req.query.postId}`],
    }));
  }

  res.status(StatusCodes.OK)
    .json(
      successResponse({bookmark}),
    );
});
