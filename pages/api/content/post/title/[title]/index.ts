import {createRouter} from '@/libs/server-router';
import {findPostByTitleSchema} from '@/validators/content';
import {createValidateSchema} from '@/middlewares';
import {onPostFindByTitle} from './get';

export default createRouter({
  GET: [
    createValidateSchema(findPostByTitleSchema, 'query'),
    onPostFindByTitle,
  ],
});
