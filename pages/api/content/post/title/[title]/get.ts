import {findUserViewablePost} from '@/composite/post';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onPostFindByTitle: NextApiHandler = errorBoundary(async (req, res) => {
  const {post} = await findUserViewablePost({
    title: `${req.query.title}`,
    refreshToken: req.cookies['refresh-token'],
  });

  res.status(StatusCodes.OK)
    .json(successResponse({post}));
});
