import {findBookmarksByUserIds} from '@/apis/server/content/find-bookmarks-by-user-post-ids';
import {searchPosts} from '@/apis/server/content/search-posts';
import {PostSearchMode} from '@/types/resources';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';
import {parseUserId} from '@/utils/server/refresh-token';
import {decryptValue} from '@/libs/cookie';

export const onPostsSearch: NextApiHandler = errorBoundary(async (req, res) => {
  const {
    keyword,
    cursor,
    mode,
  } = req.query;
  const refreshToken = req.cookies['refresh-token'];
  const bookmarkUserId = refreshToken
    ? parseUserId(
      decryptValue(refreshToken),
    )
    : undefined;

  const {
    posts,
    cursor: nextCursor,
  } = await searchPosts({
    keyword: keyword && typeof keyword === 'string' ? keyword: undefined,
    bookmarkUserId,
    cursor: cursor && typeof cursor === 'string' ? +cursor : undefined,
    mode: mode && typeof mode === 'string' ? mode as unknown as PostSearchMode : 'ALL',
  });

  const {bookmarks} = await findBookmarksByUserIds({
    userId: bookmarkUserId,
    postIds: posts.map(({id}) => id),
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        posts,
        bookmarks,
        cursor: nextCursor,
      }),
    );
});
