import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {searchPostsQuerySchema} from '@/validators/content';
import {onPostsSearch} from './get';

export default createRouter({
  GET: [
    createValidateSchema(searchPostsQuerySchema, 'query'),
    onPostsSearch,
  ],
});
