import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {queryCheckoutUserCookiesSchema} from '@/validators/checkout';
import {onCheckoutUserQuery} from './get';

export default createRouter({
  GET: [
    withUserAccessTokenValidation,
    createValidateSchema(queryCheckoutUserCookiesSchema, 'cookies'),
    onCheckoutUserQuery,
  ],
});
