import {queryCheckoutUser} from '@/apis/server/checkout';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {NextApiHandler} from 'next';
import {StatusCodes} from 'http-status-codes';
import {parseUserId} from '@/utils/server/refresh-token';
import {decryptValue} from '@/libs/cookie';

export const onCheckoutUserQuery: NextApiHandler = errorBoundary(async (req, res) => {
  const {userSubscription, ecpayTransaction} = await queryCheckoutUser({
    userId: parseUserId(
      decryptValue(
        req.cookies['refresh-token'],
      ),
    ),
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        userSubscription,
        ecpayTransaction,
      }),
    );
});
