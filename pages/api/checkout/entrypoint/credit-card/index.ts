import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {createCheckoutCreditCardEntrypointBodySchema} from '@/validators/checkout';
import {onCheckoutCreditCardEntrypointCreate} from './post';

export default createRouter({
  POST: [
    withUserAccessTokenValidation,
    createValidateSchema(createCheckoutCreditCardEntrypointBodySchema, 'body'),
    onCheckoutCreditCardEntrypointCreate,
  ],
});
