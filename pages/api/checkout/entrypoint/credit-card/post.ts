import {NextApiHandler} from 'next';
import {createCheckoutCreditCardEntrypoint} from '@/apis/server/checkout';
import {parseUserId} from '@/utils/server/refresh-token';
import {decryptValue} from '@/libs/cookie';
import {StatusCodes} from 'http-status-codes';
import {successResponse} from '@cornerstone/response-formatter';
import {errorBoundary} from '@/utils/server/error-boundary';
import {AppError} from '@cornerstone/app-error';

export const onCheckoutCreditCardEntrypointCreate: NextApiHandler = errorBoundary(async (req, res) => {
  const refreshToken = req.cookies['refresh-token'];

  if (!refreshToken) throw new AppError(undefined, 'INVALID_USER_REFRESH_TOKEN');

  const userId = parseUserId(
    decryptValue(refreshToken),
  );

  const {
    checkoutEntrypoint,
    checkoutCreditCardParams,
  } = await createCheckoutCreditCardEntrypoint({
    subscriptionType: req.body.subscriptionType,
    customerId: userId,
    createUserId: userId,
    remark: 'test',
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        checkoutEntrypoint,
        checkoutCreditCardParams,
      }),
    );
});
