import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {
  queryUserBillingHistoryCookiesSchema,
  queryUserBillingHistoryQuerySchema,
} from '@/validators/checkout';
import {onUserBillingHistoryQuery} from './get';

export default createRouter({
  GET: [
    withUserAccessTokenValidation,
    createValidateSchema(queryUserBillingHistoryCookiesSchema, 'cookies'),
    createValidateSchema(queryUserBillingHistoryQuerySchema, 'query'),
    onUserBillingHistoryQuery,
  ],
});
