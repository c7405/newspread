import {queryUserBillingHistory} from '@/apis/server/checkout';
import {decryptValue} from '@/libs/cookie';
import {errorBoundary} from '@/utils/server/error-boundary';
import {parseUserId} from '@/utils/server/refresh-token';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onUserBillingHistoryQuery: NextApiHandler = errorBoundary(async (req, res) => {
  const {billingHistory} = await queryUserBillingHistory({
    userId: parseUserId(
      decryptValue(
        req.cookies['refresh-token'],
      ),
    ),
    year: +`${req.query.year}`,
  });

  res.status(StatusCodes.OK);
  res.json(
    successResponse({
      billingHistory,
    }),
  );
});
