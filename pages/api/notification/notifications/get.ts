import {queryNotifications} from '@/apis/server/notification';
import {decryptValue} from '@/libs/cookie';
import {parseUserId} from '@/utils/client/access-token';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onNotificationsQuery: NextApiHandler = errorBoundary(async (req, res) => {
  const {cursor, perPage} = req.query;

  const {
    notifications,
    cursor: nextCursor,
  } = await queryNotifications({
    userId: parseUserId(
      decryptValue(
        req.cookies['refresh-token'],
      ),
    ),
    cursor: cursor ? +`${cursor}` : undefined,
    perPage: +`${perPage}`,
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        notifications,
        cursor: nextCursor,
      }),
    );
});
