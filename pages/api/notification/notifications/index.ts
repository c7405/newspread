import {createRouter} from '@/libs/server-router';
import {onNotificationsQuery} from './get';
import {createValidateSchema} from '@/middlewares';
import {
  queryNotificationsCookiesSchema,
  queryNotificationsQuerySchema,
} from '@/validators/notification';

export default createRouter({
  GET: [
    createValidateSchema(queryNotificationsCookiesSchema, 'cookies'),
    createValidateSchema(queryNotificationsQuerySchema, 'query'),
    onNotificationsQuery,
  ],
});
