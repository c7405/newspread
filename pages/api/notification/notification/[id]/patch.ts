import {updateNotification} from '@/apis/server/notification';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';
import {errorBoundary} from '@/utils/server/error-boundary';

export const onNotificationUpdate: NextApiHandler = errorBoundary(async (req, res) => {
  const {notification} = await updateNotification({
    id: +`${req.query.id}`,
    hasRead: req.body.hasRead,
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        notification,
      }),
    );
});
