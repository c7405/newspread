import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {
  updateNotificationBodySchema,
  updateNotificationQuerySchema,
} from '@/validators/notification';
import {onNotificationUpdate} from './patch';

export default createRouter({
  PATCH: [
    createValidateSchema(updateNotificationQuerySchema, 'query'),
    createValidateSchema(updateNotificationBodySchema, 'body'),
    onNotificationUpdate,
  ],
});
