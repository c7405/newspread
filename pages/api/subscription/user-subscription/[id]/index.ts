import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {deleteUserSubscriptionQuerySchema} from '@/validators/subscription';
import {onUserSubscriptionDelete} from './delete';

export default createRouter({
  DELETE: [
    withUserAccessTokenValidation,
    createValidateSchema(deleteUserSubscriptionQuerySchema, 'query'),
    onUserSubscriptionDelete,
  ],
});
