import type {NextApiHandler} from 'next';
import {deleteUserSubscription} from '@/apis/server/subscription';
import {errorBoundary} from '@/utils/server/error-boundary';
import {StatusCodes} from 'http-status-codes';
import {successResponse} from '@cornerstone/response-formatter';

export const onUserSubscriptionDelete: NextApiHandler = errorBoundary(async (req, res) => {
  const {userSubscription} = await deleteUserSubscription({
    id: +`${req.query.id}`,
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        userSubscription,
      }),
    );
});
