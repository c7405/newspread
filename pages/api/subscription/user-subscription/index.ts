import {createRouter} from '@/libs/server-router';
import {createValidateSchema, withUserAccessTokenValidation} from '@/middlewares';
import {queryUserSubscriptionQuerySchema} from '@/validators/subscription';
import {onUserSubscriptionFind} from './get';

export default createRouter({
  GET: [
    withUserAccessTokenValidation,
    createValidateSchema(queryUserSubscriptionQuerySchema, 'query'),
    onUserSubscriptionFind,
  ],
});
