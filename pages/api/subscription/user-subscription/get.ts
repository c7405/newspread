import {queryUserSubscription} from '@/apis/server/subscription';
import {errorBoundary} from '@/utils/server/error-boundary';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onUserSubscriptionFind: NextApiHandler = errorBoundary(async (req, res) => {
  const {userSubscription} = await queryUserSubscription({
    userId: +`${req.query.userId}`,
  });

  res.status(StatusCodes.OK)
    .json({userSubscription});
});
