import {findSubscriptionPlans} from '@/apis/server/subscription';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onSubscriptionPlansFind: NextApiHandler = errorBoundary(async (_req, res) => {
  const {subscriptionPlans} = await findSubscriptionPlans();

  res.status(StatusCodes.OK);
  res.json(
    successResponse({subscriptionPlans}),
  );
});
