import {createRouter} from '@/libs/server-router';
import {onSubscriptionPlansFind} from './get';

export default createRouter({
  GET: onSubscriptionPlansFind,
});
