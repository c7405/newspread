import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {signInSchema} from '@/validators/authentication';
import {onSignInPost} from './post';

export default createRouter({
  POST: [
    createValidateSchema(signInSchema, 'body'),
    onSignInPost,
  ],
});
