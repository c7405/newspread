import type {NextApiHandler} from 'next';
import {StatusCodes} from 'http-status-codes';
import {signIn, exchangeToken} from '@/apis/server/authentication';
import {successResponse} from '@cornerstone/response-formatter';
import {errorBoundary} from '@/utils/server/error-boundary';
import {setRefreshTokenCookie} from '@/utils/server/response';

export const onSignInPost: NextApiHandler = errorBoundary(async (req, res) => {
  const {account, password} = req.body;
  const {authorizationCode} = await signIn({account, password});
  const {refreshToken, accessToken} = await exchangeToken({authorizationCode});

  setRefreshTokenCookie(req, res)(refreshToken);

  res.status(StatusCodes.OK)
    .json(
      successResponse({accessToken}),
    );
});
