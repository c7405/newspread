import type {NextApiHandler} from 'next';
import {StatusCodes} from 'http-status-codes';
import {successResponse, errorResponse} from '@cornerstone/response-formatter';
import {substituteToken} from '@/apis/server/authentication';
import {decryptValue} from '@/libs/cookie';
import {accessForbiddenHeader} from '@/config/authentication';
import {setRefreshTokenCookie} from '@/utils/server/response';

export const onTokenPatch: NextApiHandler = async (req, res) => {
  const cookieRefreshToken = req.cookies['refresh-token'];

  if (!cookieRefreshToken) {
    res.status(StatusCodes.UNPROCESSABLE_ENTITY)  
      .setHeader('access-control-expose-headers', accessForbiddenHeader)
      .setHeader(accessForbiddenHeader, 'true')
      .json(
        errorResponse('USER_AUTHENTICATION_EXPIRED'),
      );
  
    return;
  }
  
  const {
    refreshToken,
    accessToken,
  } = await substituteToken({
    refreshToken: decryptValue(cookieRefreshToken),
  });
  
  setRefreshTokenCookie(req, res)(refreshToken);
  
  res.status(StatusCodes.OK)
    .json(
      successResponse({accessToken}),
    );
};
