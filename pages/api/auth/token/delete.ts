import {deleteToken} from '@/apis/server/authentication';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import Cookies from 'cookies';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onTokenDelete: NextApiHandler = errorBoundary(async (req, res) => {
  const refreshToken = req.cookies['refresh-token'];

  if (!refreshToken) {
    res.status(StatusCodes.OK).json(successResponse({}));

    return;
  }

  try {
    await deleteToken({refreshToken});
  } catch (e) {
    console.error(e);
  } finally {
    new Cookies(req, res).set('refresh-token');
  }

  res.status(StatusCodes.OK).json(successResponse({}));
});
