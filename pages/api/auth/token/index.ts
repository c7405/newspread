import {createRouter} from '@/libs/server-router';
import {onTokenDelete} from './delete';
import {onTokenPatch} from './patch';

export default createRouter({
  DELETE: onTokenDelete,
  PATCH: onTokenPatch,
});
