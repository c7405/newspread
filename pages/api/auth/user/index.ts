import {createRouter} from '@/libs/server-router';
import {onUserWithDetailFind} from './get';

export default createRouter({
  GET: onUserWithDetailFind,
});
