import {findUserWithDetail} from '@/apis/server/authentication';
import {successResponse} from '@cornerstone/response-formatter';
import {errorBoundary} from '@/utils/server/error-boundary';
import {StatusCodes} from 'http-status-codes';
import {NextApiHandler} from 'next';
import {AppError} from '@cornerstone/app-error';
import {decryptValue} from '@/libs/cookie';
import {decode} from 'jsonwebtoken';

export const onUserWithDetailFind: NextApiHandler = errorBoundary(async (req, res) => {
  const refreshToken = req.cookies['refresh-token'];
  const inValidRefreshTokenError = new AppError(
    undefined,
    'INVALID_USER_REFRESH_TOKEN',
    undefined,
    StatusCodes.UNAUTHORIZED,
  );

  if (typeof refreshToken !== 'string') throw inValidRefreshTokenError;

  let userId: string | undefined;

  try {
    userId = decode(decryptValue(refreshToken), {json: true})?.sub;

    if (!userId) throw inValidRefreshTokenError;
  } catch (e) {
    throw inValidRefreshTokenError;
  }

  const {id, account, userDetail} = await findUserWithDetail({id: +userId});

  res.status(StatusCodes.OK)
    .json(successResponse({id, account, userDetail}));
});
