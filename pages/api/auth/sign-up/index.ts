import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {signUpSchema} from '@/validators/authentication';
import {onSignUpPost} from './post';

export default createRouter({
  POST: [
    createValidateSchema(signUpSchema, 'body'),
    onSignUpPost,
  ],
});
