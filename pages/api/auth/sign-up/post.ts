import {NextApiHandler} from 'next';
import {StatusCodes} from 'http-status-codes';
import {successResponse} from '@cornerstone/response-formatter';
import {signUp} from '@/apis/server/authentication';
import {imageServiceUrl} from '@/libs/service';
import {errorBoundary} from '@/utils/server/error-boundary';

export const onSignUpPost: NextApiHandler = errorBoundary(async (req, res) => {
  const {
    account,
    password,
    repassword,
    firstName,
    lastName,
  } = req.body;

  const {id, userDetail} = await signUp({
    account,
    password,
    repassword,
    firstName,
    lastName,
    avatarImagePath: imageServiceUrl('api/image/ddd5599709818d1924bc'),
    createUserId: 1,
  });

  res.status(StatusCodes.OK)
    .json(
      successResponse({
        id,
        userDetail,
      }),
    );
});
