import type {NextApiHandler} from 'next';
import {parseUserId} from '@/utils/server/refresh-token';
import {decryptValue} from '@/libs/cookie';
import {errorBoundary} from '@/utils/server/error-boundary';
import {updateUserDetail} from '@/apis/server/authentication';
import {StatusCodes} from 'http-status-codes';
import {successResponse} from '@cornerstone/response-formatter';

export const onUserDetailUpdate: NextApiHandler = errorBoundary(async (req, res) => {
  const {id} = req.query;
  const {firstName, lastName, avatarImagePath} = req.body;

  const {userDetail} = await updateUserDetail(
    +`${id}`,
    {
      firstName,
      lastName,
      avatarImagePath,
      updateUserId: parseUserId(
        decryptValue(
          req.cookies['refresh-token'],
        ),
      ),
    },
  );

  res.status(StatusCodes.OK)
    .json(
      successResponse({userDetail}),
    );
});
