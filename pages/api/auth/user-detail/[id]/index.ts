import {onUserDetailUpdate} from './patch';
import {updateUserDetailBodySchema, updateUserDetailQuerySchema} from '@/validators/authentication';
import {createValidateSchema} from '@/middlewares';
import {createRouter} from '@/libs/server-router';

export default createRouter({
  PATCH: [
    createValidateSchema(updateUserDetailQuerySchema, 'query'),
    createValidateSchema(updateUserDetailBodySchema, 'body'),
    onUserDetailUpdate,
  ],
});
