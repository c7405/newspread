import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {createUserResetPasswordCodeBodySchema} from '@/validators/authentication';
import {onResetPasswordCodeCreate} from './post';

export default createRouter({
  POST: [
    createValidateSchema(createUserResetPasswordCodeBodySchema, 'body'),
    onResetPasswordCodeCreate,
  ],
});
