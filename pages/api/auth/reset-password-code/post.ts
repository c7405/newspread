import {createUserResetPasswordCode} from '@/apis/server/authentication';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import {NextApiHandler} from 'next';

export const onResetPasswordCodeCreate: NextApiHandler = errorBoundary(async (req, res) => {
  const {account} = req.body;

  await createUserResetPasswordCode({account});

  res.status(StatusCodes.OK)
    .json(successResponse({}));
});
