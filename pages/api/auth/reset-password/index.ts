import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {createUserNewPasswordBodySchema} from '@/validators/authentication';
import {onUserNewPasswordCreate} from './post';

export default createRouter({
  POST: [
    createValidateSchema(createUserNewPasswordBodySchema, 'body'),
    onUserNewPasswordCreate,
  ],
});
