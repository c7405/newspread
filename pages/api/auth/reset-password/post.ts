import {createUserNewPassword} from '@/apis/server/authentication';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';
import {errorBoundary} from '@/utils/server/error-boundary';

export const onUserNewPasswordCreate: NextApiHandler = errorBoundary(async (req, res) => {
  const {newPassword, confirmNewPassword, authorizationCode} = req.body;

  await createUserNewPassword({
    newPassword,
    confirmNewPassword,
    authorizationCode,
  });

  res.status(StatusCodes.OK)
    .json(successResponse({}));
});

