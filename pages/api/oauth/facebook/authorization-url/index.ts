import {createRouter} from '@/libs/server-router';
import {onAuthorizationUrlQuery} from './get';

export default createRouter({
  GET: onAuthorizationUrlQuery,
});
