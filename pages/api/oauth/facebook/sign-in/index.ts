import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {processFacebookUserSignInQuerySchema} from '@/validators/oauth';
import {onUserSignIn} from './get';

export default createRouter({
  GET: [
    createValidateSchema(processFacebookUserSignInQuerySchema, 'query'),
    onUserSignIn,
  ],
});
