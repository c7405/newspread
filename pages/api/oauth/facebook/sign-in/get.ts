import {processFacebookUserSignIn} from '@/apis/server/authentication';
import {errorBoundary} from '@/utils/server/error-boundary';
import {setRefreshTokenCookie} from '@/utils/server/response';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onUserSignIn: NextApiHandler = errorBoundary(async (req, res) => {
  const {refreshToken} = await processFacebookUserSignIn({
    code: `${req.query.code}`,
  });

  setRefreshTokenCookie(req, res)(refreshToken);

  res.status(StatusCodes.TEMPORARY_REDIRECT)
    .redirect('/');
});
