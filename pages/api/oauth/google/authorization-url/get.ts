import {queryGoogleAuthorizationUrl} from '@/apis/server/authentication';
import {errorBoundary} from '@/utils/server/error-boundary';
import {successResponse} from '@cornerstone/response-formatter';
import {StatusCodes} from 'http-status-codes';
import type {NextApiHandler} from 'next';

export const onAuthorizationUrlQuery: NextApiHandler = errorBoundary(async (_req, res) => {
  const {authorizationUrl} = await queryGoogleAuthorizationUrl();

  res.status(StatusCodes.OK)
    .json(
      successResponse({authorizationUrl}),
    );
});
