import {createRouter} from '@/libs/server-router';
import {createValidateSchema} from '@/middlewares';
import {processGoogleUserSignInQuerySchema} from '@/validators/oauth';
import {onUserSignIn} from './get';

export default createRouter({
  GET: [
    createValidateSchema(processGoogleUserSignInQuerySchema, 'query'),
    onUserSignIn,
  ],
});
