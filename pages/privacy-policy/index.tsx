import {AuthenticationLayout} from '@/components/layout';
import {PrivacyPolicyArticle} from '@/components/pages/privacy-policy/base';
import type {NextPage} from 'next';

const PrivacyPolicy: NextPage = () => (
  <AuthenticationLayout>
    <PrivacyPolicyArticle />
  </AuthenticationLayout>
);

export default PrivacyPolicy;
