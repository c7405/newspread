import {TermsOfServiceArticle} from '@/components/pages/terms-of-service/base';
import {AuthenticationLayout} from '@/components/layout';
import type {NextPage} from 'next';

const TermsOfService: NextPage = () => (
  <AuthenticationLayout>
    <TermsOfServiceArticle />
  </AuthenticationLayout>
);

export default TermsOfService;
