export type Post = {
  id: number;
  title: string;
  coverImageSrc: string;
  coverImageWidth: number;
  coverImageHeight: number;
  readTime: number;
  exclusive: boolean;
  updateDate: string;
};

export type Bookmark = {
  id: number;
  postId: Post['id'];
  userId: User['id'];
  createDate: string;
}

export type UserProfile = {
  id: number;
  account: string;
  firstName: string;
  lastName: string;
  avatarImagePath: string;
}

export type PostContentType = 'TEXT' | 'IMAGE';
export type PostContentTextItem = {
  postId: number;
  type: 'TEXT';
  value: string;
  position: number;
  props: null | {
    style: Record<string, string | number>;
  };
};
export type PostContentImageItem = {
  postId: number;
  type: 'IMAGE';
  value: string;
  position: number;
  props: {
    width: number;
    height: number;
    caption: string;
  };
};
export type PostContent =
  | PostContentTextItem
  | PostContentImageItem;

export type PostSearchMode = 'ALL' | 'USER_BOOKMARKED';

export type UserDetail = {
  id: number;
  firstName: string;
  lastName: string;
  avatarImagePath: string;
};

export type User = {
  id: number;
  account: string;
  userDetail: UserDetail;
};

export type Author = {
  id: number;
  firstName: string;
  lastName: string;
};

export type PostWithContentAuthor = Post & {
  content: PostContent[];
  authors: Author[];
};

export type SubscriptionPlanType = 'MONTHLY' | 'ANNUAL';

export type UserSubscription = {
  id: number;
  userId: User['id'];
  subscriptionPlanType: SubscriptionPlanType;
  createDate: string;
};

export type SubscriptionPlan = {
  plan: SubscriptionPlanType;
  description: string;
  price: number;
  unit: string;
};

export type SubscriptionPlanMap = {
  [key in SubscriptionPlanType]: SubscriptionPlan;
};

export type CheckoutPeriodType = 'Y' | 'M';

export type OrderStatus =
  | 'ORDER_CREATED'
  | 'TRANSACTION_PROCESSING'
  | 'TRANSACTION_DONE'
  | 'TRANSACTION_FAILED';

export type Order = {
  id: number;
  tradeId: string;
  subscriptionType: SubscriptionPlanType;
  status: OrderStatus;
  customerId: number;
  createUserId: number;
  createDate: string;
};

export type EcpayTransaction = {
  id: number;
  merchantTradeNo: string;
  rtnCode: number;
  rtnMsg: string;
  tradeAmt: number;
  paymentDate: string;
  paymentType: string;
  paymentTypeChargeFee: number;
  tradeDate: string;
  createTime: string;
};

export type BillingHistory = {
  subscriptionPlan: SubscriptionPlanType;
  paymentStatus: boolean;
  renewalDate: string;
  invoiceDate: string;
};

export type NotificationTextContent = {
  type: 'TEXT',
  text: string;
  props: {
    style?: Record<string, string>;
    newline?: boolean;
  },
};

export type NotificationLinkContent = {
  type: 'LINK',
  text: string;
  props: {
    style?: Record<string, string>;
    href: string;
  };
};

export type NotificationBlockContent =
  | NotificationTextContent
  | NotificationLinkContent;

export type NotificationBlock = NotificationBlockContent[];

export type NotificationContent = NotificationBlock[];

export type Notification = {
  id: number;
  coverImagePath: string;
  coverImageWidth: string;
  coverImageHeight: string;
  title: string;
  content: NotificationContent;
  userId: User['id'];
  hasRead: boolean;
  createDate: string;
  updateDate: string;
};

export type AppImageFormat = 'AVIF' | 'WEBP' | 'JPEG' | 'PNG';
