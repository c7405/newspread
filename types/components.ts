export type Variant = 'default' | 'primary' | 'warning' | 'danger';

export type VoidEventHandler = () => void;

export type AsyncVoidEventHandler = () => Promise<void>;
