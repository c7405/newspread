import {useCallback, useEffect, useState, useMemo} from 'react';
import type {
  ZodObject,
  ZodRawShape,
  ZodError,
  ZodEffects,
} from 'zod';

type GetDataWithChangeState<T> = (newData: T) => void;
type SetDateChangeMap<K extends string | number | symbol> = (dateChangeMap: Partial<Record<K, boolean>>) => void;
type OnReset = () => void;

export type UseValidation = <T extends object, S extends ZodObject<ZodRawShape, 'strict' | 'strip'> | ZodEffects<ZodObject<ZodRawShape, 'strict' | 'strip'>>>(initData: T, schema: S) => [
  boolean,
  T,
  GetDataWithChangeState<Partial<T>>,
  ZodError | null,
  Record<string, string>,
  SetDateChangeMap<keyof T>,
  OnReset,
];

export const useValidation: UseValidation = (initData, schema) => {
  const [dataChangeMap, setDataChangeMap] = useState<Partial<Record<keyof typeof initData, boolean>>>({});
  const [data, setData] = useState<typeof initData>(initData);
  const [valildationError, setValidationError] = useState<ZodError | null>(null);

  const setDataWithChangeState = useCallback<GetDataWithChangeState<Partial<typeof initData>>>(newData => {
    setData({
      ...data,
      ...newData,
    });

    setDataChangeMap({
      ...dataChangeMap,
      ...Object.keys(newData).reduce((carry, key) => Object.assign(
        carry,
        {
          [key]: true,
        },
      ), {}),
    });
  }, [data, setData, dataChangeMap, setDataChangeMap]);

  const isDataChanged = useMemo(() => Object.values(dataChangeMap).some(value => value), [dataChangeMap]);

  const validateData = useCallback(async () => {
    if (!isDataChanged) return;

    const result = await schema.safeParseAsync(data);

    if (result.success) {
      setValidationError(null);
    } else {
      setValidationError(result.error);
    }
  }, [isDataChanged, data, schema]);

  useEffect(() => {
    validateData();
  }, [validateData]);

  const errorDetailMap = valildationError
    ? Object.entries(valildationError.flatten().fieldErrors)
      .reduce((carry, [field, errors]) => {
        if (!(field in dataChangeMap) || !dataChangeMap[field as unknown as keyof typeof initData]) {
          return carry;
        }

        return Object.assign(
          carry,
          {
            [field]: errors?.[0],
          },
        );
      }, {})
    : {};

  const onReset: OnReset = () => {
    setData(initData);
    setDataChangeMap({});
  };

  return [
    isDataChanged,
    data,
    setDataWithChangeState,
    valildationError,
    errorDetailMap,
    setDataChangeMap,
    onReset,
  ];
};
