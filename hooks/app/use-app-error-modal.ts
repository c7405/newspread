import type {VoidEventHandler} from '@/types/components';
import type {AppStatusCode} from '@cornerstone/app-status-code';
import {appStatusCodeMessageMap} from '@cornerstone/app-status-code';
import type {ReactNode} from 'react';
import {useState} from 'react';

const customMessageComponentMap: Map<AppStatusCode, ReactNode> = new Map();

export type TitleVariant = 'default' | 'danger';

export type UseAppErrorModal = () => {
  title: string;
  titleVariant: TitleVariant;
  appendToMessageMap: (appErrorCode: AppStatusCode, component: ReactNode) => void;
  getAppErrorMessage: (appErrorCode: AppStatusCode) => string | ReactNode;
  resetModal: VoidEventHandler;
};

export const useAppErrorModal: UseAppErrorModal = () => {
  const defaultTitle = 'Error';
  const defaultTitleVariant = 'danger';
  const [title, setTitle] = useState<string>(defaultTitle);
  const [titleVariant, setTitleVariant] = useState<TitleVariant>(defaultTitleVariant);

  return {
    title,
    titleVariant,
    setTitle,
    setTitleVariant,
    appendToMessageMap: (appErrorCode: AppStatusCode, component: ReactNode) => {
      customMessageComponentMap.set(appErrorCode, component);
    },
    getAppErrorMessage(appErrorCode: AppStatusCode) {
      return customMessageComponentMap.get(appErrorCode) ??
        appStatusCodeMessageMap[appErrorCode] ??
        appStatusCodeMessageMap['UNKNOWN_ERROR'];
    },
    resetModal() {
      setTitle(defaultTitle);
      setTitleVariant(defaultTitleVariant);
    },
  };
};
