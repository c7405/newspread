import {swrFetcher} from '@/libs/browser-client';
import type {VoidEventHandler} from '@/types/components';
import type {User, UserDetail} from '@/types/resources';
import useSWRImmutable from 'swr/immutable';

export type useUserConfig = {
  fallbackData?: User;
};

export type UseUser = (config?: useUserConfig) => {
  user?: User;
  isLoading: boolean;
  isValidating: boolean;
  onUserDetailUpdate: (data: UserDetail) => void;
  onReset: VoidEventHandler;
};

export const useUser: UseUser = ({fallbackData} = {}) => {
  const {
    data,
    error,
    isValidating,
    mutate,
  } = useSWRImmutable<User>(
    'api/auth/user',
    path => swrFetcher<User>(path),
    {
      fallbackData,
      shouldRetryOnError: false,
    },
  );

  return {
    user: data,
    isLoading: data === undefined && !error,
    isValidating,
    onUserDetailUpdate: updateData => {
      if (!data) return;

      const newUser = {
        ...data,
        userDetail: updateData,
      };

      mutate(newUser);
    },
    onReset: () => {
      mutate(undefined);
    },
  };
};

