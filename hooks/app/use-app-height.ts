import {VoidEventHandler} from '@/types/components';
import {useEffect} from 'react';

export type UseAppHeight = () => {
  onAppHeightStyleUpdate: VoidEventHandler;
};

export const useAppHeight: UseAppHeight = () => {
  const onAppHeightStyleUpdate = () => {
    document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`);
  };

  useEffect(() => {
    window.addEventListener('resize', onAppHeightStyleUpdate);

    onAppHeightStyleUpdate();

    return () => {
      window.removeEventListener('resize', onAppHeightStyleUpdate);
    };
  });

  return {
    onAppHeightStyleUpdate,
  };
};
