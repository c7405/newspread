import {useRef, useCallback, MutableRefObject} from 'react';

export type InputElementRef = HTMLInputElement | null;

export type UseFocus = () => [MutableRefObject<InputElementRef>, () => void];

export const useFocus: UseFocus = () => {
  const inputElRef = useRef<InputElementRef>(null);
  const onInputFocus = useCallback(() => {
    const {current: currentInputEl} = inputElRef;

    if (!currentInputEl) return;

    const {length: rangeEndCursor} = currentInputEl.value;

    currentInputEl.setSelectionRange(rangeEndCursor, rangeEndCursor);
    currentInputEl.focus();
  }, []);

  return [inputElRef, onInputFocus];
};
