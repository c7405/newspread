import type {CreateImageServiceLoaderUrlLoader} from '@/formatters/page/image-url';
import {createImageServiceLoaderUrlLoader} from '@/formatters/page/image-url';
import {isImageServiceUrl} from '@/libs/service';
import {AppError} from '@cornerstone/app-error';
import type {ImageProps} from 'next/image';

export type UseImageFormatPayload = {
  src: ImageProps['src'];
};

export type UseImageFormat = (payload: UseImageFormatPayload) => {
  isImageSerivceImage?: boolean;
  imageFormat?: string;
  imageLoader?: ReturnType<CreateImageServiceLoaderUrlLoader>;
};

export const useImageFormat: UseImageFormat = ({src}) => {
  let imageSrc = '';

  if (typeof src !== 'string') {
    if ('src' in src) {
      imageSrc = src.src;
    } else if ('default' in src) {
      imageSrc = src.default.src;
    } else {
      throw new AppError('Invalid app image src');
    }
  } else {
    imageSrc = src;
  }

  return {
    imageLoader: (
      isImageServiceUrl(imageSrc)
        ? createImageServiceLoaderUrlLoader('WEBP')
        : undefined
    ),
  };
};
