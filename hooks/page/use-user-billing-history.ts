import {swrFetcher} from '@/libs/browser-client';
import {BillingHistory} from '@/types/resources';
import {useState} from 'react';
import useSWR from 'swr';

export type UseUserBillingHistorySWRData = {
  billingHistory: BillingHistory[];
};

export type UseUserBillingHistoryConfig = {
  year?: number;
};

export type UseUserBillingHistoryResult = {
  billingHistory?: BillingHistory[];
  isLoading: boolean;
  billingHistoryYear?: number;
  setBillingHistoryYear: (year: number) => void;
};

export type UseUserBillingHistory = (config: UseUserBillingHistoryConfig) => UseUserBillingHistoryResult;

export const useUserBillingHistory: UseUserBillingHistory = config => {
  const [year, setYear] = useState<UseUserBillingHistoryResult['billingHistoryYear'] | undefined>(config.year);

  const {data, error} = useSWR<UseUserBillingHistorySWRData>(
    year ? ['api/checkout/billing/history', {year}] : null,
    swrFetcher,
  );

  return {
    billingHistory: data?.billingHistory,
    isLoading: !data && !error,
    billingHistoryYear: year,
    setBillingHistoryYear: setYear,
  };
};
