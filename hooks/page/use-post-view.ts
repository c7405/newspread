import {swrFetcher} from '@/libs/browser-client';
import {PostWithContentAuthor} from '@/types/resources';
import useSWR from 'swr';

type PostViewResponseData = {
  post: PostWithContentAuthor;
};

export type UsePostViewConfig = {
  title: PostWithContentAuthor['title'];
  fallbackData: PostViewResponseData;
};

export type UsePostView = (config: UsePostViewConfig) => {
  post: PostWithContentAuthor;
  isLoading: boolean;
  isValidating: boolean;
};

export const usePostView: UsePostView = ({title, fallbackData}) =>  {
  const {data, error, isValidating} = useSWR<PostViewResponseData>(
    () => title ? `api/content/post/title/${title}` : null,
    swrFetcher,
    {
      fallbackData,
    },
  );

  return {
    post: data?.post ?? fallbackData.post,
    isLoading: !data && !error,
    isValidating,
  };
};
