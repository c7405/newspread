import {swrFetcher} from '@/libs/browser-client';
import type {AsyncVoidEventHandler} from '@/types/components';
import type {EcpayTransaction, User, UserSubscription} from '@/types/resources';
import useSWR from 'swr';

export type UseCheckoutUserDataConfig = {
  user?: User;
};

export type UseCheckoutUserData = {
  userSubscription: UserSubscription | null;
  ecpayTransaction: EcpayTransaction | null;
};

export type UseCheckoutUser = (config: UseCheckoutUserDataConfig) => {
  userSubscription: UserSubscription | null;
  ecpayTransaction: EcpayTransaction | null;
  onCheckoutUserRefresh: AsyncVoidEventHandler;
  isLoading: boolean;
};

export const useCheckoutUser: UseCheckoutUser = ({user}) => {
  const {data, error, mutate} = useSWR<UseCheckoutUserData>(
    () => user ? 'api/checkout/user' : null,
    swrFetcher,
    {
      errorRetryCount: 0,
    },
  );

  return {
    userSubscription: data?.userSubscription ?? null,
    ecpayTransaction: data?.ecpayTransaction ?? null,
    isLoading: !data && !error,
    onCheckoutUserRefresh: async () => {
      await mutate();
    },
  };
};
