import {VoidEventHandler} from '@/types/components';
import type {Bookmark, Post} from '@/types/resources';
import {useState} from 'react';
import {useBookmarkProcessToggle} from '.';

type OnProcess = (
  postId: Post['id'],
  postBookmarkId: Bookmark['id'] | undefined,
  onProcessEnd: VoidEventHandler,
) => Promise<void>;

export type UsePostsBookmarkProcess = () => {
  isUndoToastOpen: boolean;
  undoMessage: string;
  undoText: string;
  lastPostId?: Post['id'];
  lastPostBookmarkId?: Bookmark['id'];
  setLastPostId: (postId: Post['id']) => void;
  setLastPostBookmarkId: (postBookmarkId: Bookmark['id']) => void;
  setIsUndoToastOpen: (isOpen: boolean) => void;
  onProcess: OnProcess;
};

export const usePostsBookmarkProcess: UsePostsBookmarkProcess = () => {
  const [isUndoToastOpen, setIsUndoToastOpen] = useState<boolean>(false);
  const [lastPostId, setLastPostId] = useState<Post['id'] | undefined>(undefined);
  const [lastPostBookmarkId, setLastPostBookmarkId] = useState<Bookmark['id'] | undefined>(undefined);
  const undoMessage = isUndoToastOpen ? 'A post bookmark has been removed.' : '';
  const undoText = isUndoToastOpen ? 'Restore' : '';

  const {onBookmarkProcessToggle} = useBookmarkProcessToggle();

  const onProcess: OnProcess = async (postId, postBookmarkId, onProcessEnd) => {
    setIsUndoToastOpen(!!postBookmarkId);
    setLastPostId(postId);
    setLastPostBookmarkId(postBookmarkId);

    await onBookmarkProcessToggle(postId, postBookmarkId);

    onProcessEnd();
  };

  return {
    isUndoToastOpen,
    undoMessage,
    undoText,
    lastPostId,
    lastPostBookmarkId,
    setLastPostId,
    setLastPostBookmarkId,
    setIsUndoToastOpen,
    onProcess,
  };
};
