import {swrFetcher} from '@/libs/browser-client';
import {VoidEventHandler} from '@/types/components';
import type {Bookmark, Post, PostSearchMode} from '@/types/resources';
import {useState} from 'react';
import useSWRInfinite from 'swr/infinite';
import {onlyTruthyValue} from '@/libs/object';

export type PostSearchConfig = {
  headFallbackData?: {
    bookmarks: Bookmark[];
    posts: Post[];
    cursor?: number;
  };
  mode?: PostSearchMode;
  isKeywordRequired?: boolean;
};

export type UsePostInfiniteScroll = (config?: PostSearchConfig) => {
  isLoading: boolean;
  isValidating: boolean;
  posts?: Post[];
  postBookmarkIdMap: Record<string, number | undefined>;
  keyword: string;
  size: number;
  isPageEndReached: () => boolean;
  setKeyword: (keyword: string) => void;
  onReload: VoidEventHandler;
  onNextPageLoad: VoidEventHandler;
};

export const usePostInfiniteScroll: UsePostInfiniteScroll = (config = {}) => {
  const {
    headFallbackData,
    isKeywordRequired = false,
    mode = 'ALL',
  } = config;
  const [keyword, setKeyword] = useState<string>('');
  const {
    data,
    error,
    isValidating,
    size,
    setSize,
    mutate,
  } = useSWRInfinite<{cursor?: number; posts: Post[]; bookmarks: Bookmark[]}>(
    (pageIndex, previousPageData) => {
      const path = 'api/content/posts';

      if (previousPageData && !previousPageData.posts?.length) return null;

      if (!pageIndex) {
        if (!isKeywordRequired || keyword) {
          return [
            path,
            onlyTruthyValue({keyword, mode}),
          ];
        }

        return null;
      }

      const cursor = previousPageData?.cursor;

      return [
        path,
        onlyTruthyValue({keyword, mode, cursor}),
      ];
    },
    (path, searchParams) => swrFetcher(path, searchParams),
    {
      fallbackData: headFallbackData && [headFallbackData],
    },
  );

  return {
    isLoading: !data && !error,
    isValidating,
    isPageEndReached: () => {
      const lastDataItem = data?.at(-1);

      if (!lastDataItem) return false;

      const {
        cursor,
        posts: {
          length: postLen,
        } = [],
      } = lastDataItem;

      return !cursor && !postLen;
    },
    posts: data?.reduce(
      (carry, {posts}) => carry.concat(posts),
      [] as unknown as Post[],
    ),
    postBookmarkIdMap: data
      ? data.reduce((carry, {bookmarks}) => (
        bookmarks.reduce((subCarry, {id, postId}) => Object.assign(
          subCarry,
          {[postId]: id},
        ), carry)
      ), {})
      : {},
    keyword,
    size,
    setKeyword,
    onReload: () => {
      mutate();
    },
    onNextPageLoad: () => setSize(size + 1),
  };
};
