import {FindSubscriptionPlansResponseData} from '@/apis/server/subscription/find-subscription-plans';
import {swrFetcher} from '@/libs/browser-client';
import type {SubscriptionPlanMap} from '@/types/resources';
import useSWR from 'swr';
import {toKeyMap} from '@/libs/array';

export type UseSubscriptionPlansConfig = {
  fallbackData?: FindSubscriptionPlansResponseData;
};

export type UseSubscriptionPlans = (config?: UseSubscriptionPlansConfig) => {
  subscriptionPlans?: FindSubscriptionPlansResponseData['subscriptionPlans'];
  subscriptionPlanMap?: SubscriptionPlanMap;
  isLoading: boolean;
  isValidating: boolean;
};

export const useSubscriptionPlans: UseSubscriptionPlans = config => {
  const {data, error, isValidating} = useSWR<FindSubscriptionPlansResponseData>(
    'api/subscription/subscription-plans',
    swrFetcher,
    {
      fallbackData: config?.fallbackData,
      errorRetryCount: 0,
    },
  );
  const subscriptionPlans = data?.subscriptionPlans;

  return {
    subscriptionPlans,
    subscriptionPlanMap: subscriptionPlans && toKeyMap(subscriptionPlans, 'plan'),
    isLoading: !data && !error,
    isValidating,
  };
};
