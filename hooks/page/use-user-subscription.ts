import {swrFetcher} from '@/libs/browser-client';
import {UserSubscription, User} from '@/types/resources';
import useSWR from 'swr';

export type UserUserSubscriptionConfig = {
  userId?: User['id'];
  fallbackData?: {
    userSubscription: UserSubscription | null;
  };
};

export type UserUserSubscription = (config: UserUserSubscriptionConfig) => {
  userSubscription: UserSubscription | null;
  isLoading: boolean;
};

export const useUserSubscription: UserUserSubscription = ({userId, fallbackData}) => {
  const {data, error} = useSWR<UserUserSubscriptionConfig['fallbackData']>(
    userId ? ['api/subscription/user-subscription', {userId}] : null,
    swrFetcher,
    {
      fallbackData,
      errorRetryCount: 0,
    },
  );

  return {
    userSubscription: data?.userSubscription ?? null,
    isLoading: !data && !error,
  };
};
