import {swrFetcher} from '@/libs/browser-client';
import {onlyTruthyValue} from '@/libs/object';
import type {VoidEventHandler} from '@/types/components';
import type {Notification, User} from '@/types/resources';
import useSWRInfinite from 'swr/infinite';

export type UseUserNotificationInfiniteScrollConfig = {
  userId?: User['id'];
  perPage?: number;
};

export type UseUserNotificationInfiniteScrollResponseData = {
  notifications: Notification[];
  cursor?: number;
};

export type OnPageItemUpdateUpateData = {
  hasRead: Notification['hasRead'];
};

export type UseUserNotificationInfiniteScroll = (config: UseUserNotificationInfiniteScrollConfig) => {
  notifications?: Notification[];
  isLoading: boolean;
  isValidating: boolean;
  isPageEndReached: () => boolean;
  onNextPageLoad: VoidEventHandler;
  onPageItemUpdate: (config: {listIdx: number, updateData: OnPageItemUpdateUpateData}) => void;
};

export const useUserNotificationInfiniteScroll: UseUserNotificationInfiniteScroll = ({userId, perPage = 20}) => {
  const {data, error, size, setSize, isValidating, mutate} = useSWRInfinite<UseUserNotificationInfiniteScrollResponseData>(
    (pageIndex, previousPageData) => {
      if (!userId || (previousPageData && !previousPageData.notifications?.length)) return null;

      return [
        'api/notification/notifications',
        onlyTruthyValue({
          perPage,
          cursor: pageIndex ? previousPageData.cursor : undefined,
        }),
      ];
    },
    swrFetcher,
    {
      errorRetryCount: 0,
    },
  );

  return {
    notifications: data?.reduce(
      (carry, {notifications}) => carry.concat(notifications),
      [] as unknown as Notification[],
    ),
    isLoading: !data && !error,
    isValidating,
    currentPage: size,
    isPageEndReached: () => {
      if (!data) return false;

      const {
        notifications: {length: notificationLen} = [],
        cursor,
      } = data.at(-1) ?? {};

      return !cursor && !notificationLen;
    },
    onNextPageLoad: () => setSize(size + 1),
    onPageItemUpdate: ({listIdx, updateData}) => {
      if (!data) return;

      const newData = [...data];
      const pageIdx = Math.floor(listIdx / perPage);
      const pageNotificationIdx = listIdx % perPage;

      newData[pageIdx].notifications = [
        ...newData[pageIdx].notifications,
      ];

      newData[pageIdx].notifications[pageNotificationIdx] = {
        ...newData[pageIdx].notifications[pageNotificationIdx],
        ...updateData,
      };

      mutate(newData);
    },
  };
};
