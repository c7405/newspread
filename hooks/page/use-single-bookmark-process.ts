import {swrFetcher} from '@/libs/browser-client';
import type {Bookmark, Post, User} from '@/types/resources';
import useSWR from 'swr';
import {useBookmarkProcessToggle} from '.';

type OnBookmarkProcess = (
  postId: Post['id'],
  postBookmarkId: Bookmark['id'] | undefined,
) => Promise<void>;

type FindBookmarkResponseData = {
  bookmark: Bookmark | null;
};

export type UsePostsBookmarkProcessConfig = {
  user?: User | null,
  postId?: Post['id'];
  fallbackData: FindBookmarkResponseData;
};

export type UsePostsBookmarkProcess = (config: UsePostsBookmarkProcessConfig) => {
  bookmark?: FindBookmarkResponseData['bookmark'];
  isLoading: boolean;
  isValidating: boolean;
  onBookmarkProcess: OnBookmarkProcess;
};

export const useSingleBookmarkProcess: UsePostsBookmarkProcess = ({user, postId, fallbackData}) => {
  const {
    data,
    error,
    isValidating,
    mutate,
  } = useSWR<FindBookmarkResponseData>(
    () => user && postId ? ['api/content/bookmark', {postId}] : null,
    swrFetcher,
    {
      fallbackData,
    },
  );

  const {onBookmarkProcessToggle} = useBookmarkProcessToggle();

  const onBookmarkProcess: OnBookmarkProcess = async (activePostId, postBookmarkId) => {
    await onBookmarkProcessToggle(activePostId, postBookmarkId);
    await mutate();
  };

  return {
    bookmark: data?.bookmark,
    isLoading: !data && !error,
    isValidating,
    onBookmarkProcess,
  };
};
