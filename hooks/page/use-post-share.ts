import {baseUrl} from '@/libs/app';
import type {Post} from '@/types/resources';

type OnPostShare = (postTitle: Post['title']) => Promise<void>;

export type UsePostShare = () => {
  onPostShare: OnPostShare;
};

export const usePostShare: UsePostShare = () => {
  const onPostShare: OnPostShare = async postTitle => {
    try {
      await navigator.share?.({
        url: baseUrl(`post/${postTitle}`),
        title: postTitle,
      });
    } catch (e) {
      console.error(e);
    }
  };

  return {onPostShare};
};

