import {createBookmark, deleteBookmark} from '@/apis/client/content';
import type {Bookmark, Post} from '@/types/resources';

type OnBookmarkProcess = (
  postId: Post['id'],
  postBookmarkId: Bookmark['id'] | undefined,
) => Promise<void>;

export type UseBookmarkProcessToggle = () => {
  onBookmarkProcessToggle: OnBookmarkProcess;
};

export const useBookmarkProcessToggle: UseBookmarkProcessToggle = () => {
  const onBookmarkProcessToggle: OnBookmarkProcess = async (postId, postBookmarkId) => {
    if (postBookmarkId) {
      await deleteBookmark({id: postBookmarkId});
    } else if (postId) {
      await createBookmark({postId});
    }
  };

  return {
    onBookmarkProcessToggle,
  };
};
