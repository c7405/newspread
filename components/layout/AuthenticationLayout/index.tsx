import {Footer, Header} from '@/components/pages/auth/base';
import {useAppHeight} from '@/hooks/app';
import type {NextPage} from 'next';
import styles from './styles.module.scss';
import type {ReactNode} from 'react';

export type AuthenticationLayoutBaseProps = {children?: ReactNode};

export type AuthenticationLayoutTitleRequiredProps = AuthenticationLayoutBaseProps & {
  title: string;
  subTitle: string;
};

export type AuthenticationLayoutTitleEmptyProps = AuthenticationLayoutBaseProps & {
  title?: null;
  subTitle?: never;
};

export type AuthenticationLayoutProps =
  | AuthenticationLayoutTitleRequiredProps
  | AuthenticationLayoutTitleEmptyProps;

export const AuthenticationLayout: NextPage<AuthenticationLayoutProps> = ({title, subTitle, children}) => {
  useAppHeight();

  return (
    <div className={styles.main}>
      <Header className={styles.header} />
      <section className={styles.body}>
        {
          title && subTitle && (
            <header className={styles.containerTitle}>
              <h3 className={styles.titleMain}>{title}</h3>
              <h5 className={styles.titleSub}>{subTitle}</h5>
            </header>
          )
        }
        {children}
      </section>
      <Footer className={styles.footer} />
    </div>
  );
};
