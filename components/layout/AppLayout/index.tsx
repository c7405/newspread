import {updateNotification} from '@/apis/client/notification';
import {
  EmptyContentIndicator,
  LoadingIcon,
  PopupPageModal,
  Portal,
  ScrollEndDetector,
} from '@/components/base';
import {PopupPageModalProps} from '@/components/base/PopupPageModal';
import type {ScrollEndDetectorProps} from '@/components/base/ScrollEndDetector';
import {
  Footer,
  Header,
  UserNotification,
  UserNotificationList,
} from '@/components/pages/home/base';
import type {FooterProps} from '@/components/pages/home/base/Footer';
import type {UserNotificationListProps} from '@/components/pages/home/base/UserNotificationList';
import {formatDate} from '@/formatters/date';
import {useAppHeight, useUser} from '@/hooks/app';
import {useUserNotificationInfiniteScroll} from '@/hooks/page';
import type {Notification} from '@/types/resources';
import classnames from 'classnames';
import type {FC} from 'react';
import {useState, useMemo, useCallback} from 'react';
import styles from './styles.module.scss';

export type AppLayoutProps = FooterProps & {
  bodyClassName?: string;
};

export const AppLayout: FC<AppLayoutProps> = ({children, activeType}) => {
  const {
    user,
    isLoading: isUserLoading,
  } = useUser();

  const [isUserNotificationListOpen, setIsUserNotificationListOpen] = useState<boolean>(false);
  const [isUserNotificationContentOpen, setIsUserNotificationContentOpen] = useState<boolean>(false);
  const [activeNotificationListIdx, setActiveNotificationListIdx] = useState<Notification['id'] | undefined>(undefined);

  const {
    notifications,
    isLoading: isNotificationsLoading,
    isValidating: isNotificationsValidating,
    isPageEndReached: isUserNotificationPageEndReached,
    onNextPageLoad: onUserNotificationNextPageLoad,
    onPageItemUpdate: onNotificationPageItemUpdate,
  } = useUserNotificationInfiniteScroll({userId: user?.id});

  const hasUnreadNotification = (
    !isUserLoading &&
    !isNotificationsLoading &&
    !!notifications?.filter(({hasRead}) => !hasRead).length
  );

  const activeNotification = activeNotificationListIdx !== undefined
    ? notifications?.at(activeNotificationListIdx)
    : undefined;

  const onNotificationItemClick: UserNotificationListProps['onNotificationItemClick'] = useCallback(async ({listIdx}) => {
    if (!notifications) return;

    const {id, hasRead} = notifications[listIdx];

    setIsUserNotificationContentOpen(true);
    setActiveNotificationListIdx(listIdx);

    if (!hasRead) {
      await updateNotification({
        id,
        hasRead: true,
      });

      window.setTimeout(() => {
        onNotificationPageItemUpdate({
          listIdx,
          updateData: {
            hasRead: true,
          },
        });
      }, 1000);
    }
  }, [notifications, onNotificationPageItemUpdate]);

  const onNotificationListModalOpenChange: PopupPageModalProps['onOpenChange'] = open => {
    setIsUserNotificationListOpen(open);
  };

  const onNotificationContentModalOpenChange: PopupPageModalProps['onOpenChange'] = open => {
    setIsUserNotificationContentOpen(open);
  };

  const onNotificationContentModalCloseEnd: PopupPageModalProps['onCloseEnd'] = () => {
    setIsUserNotificationContentOpen(false);
    setActiveNotificationListIdx(undefined);
  };

  const onUserNotificationListAboutScrollEnd: ScrollEndDetectorProps['onAboutScrollEnd'] = () => {
    if (isUserNotificationPageEndReached()) return;

    onUserNotificationNextPageLoad();
  };

  const NotificationListModalBody = useMemo(() => {
    const isDataLoading = isNotificationsLoading || isNotificationsValidating;

    if (!isDataLoading && !notifications?.length) {
      return (
        <EmptyContentIndicator message="No Notification" />
      );
    }

    return (
      <>
        <UserNotificationList
          notifications={notifications ?? []}
          onNotificationItemClick={onNotificationItemClick} />
        {
          isDataLoading && (
            <div className={styles.containerLoadingIcon}>
              <LoadingIcon />
            </div>
          )
        }
      </>
    );
  }, [isNotificationsLoading, isNotificationsValidating, notifications, onNotificationItemClick]);

  useAppHeight();

  return (
    <>
      <Portal portalId="user-notification-list-modal">
        <PopupPageModal
          title="Notifications"
          open={isUserNotificationListOpen}
          onOpenChange={onNotificationListModalOpenChange}>
          <ScrollEndDetector onAboutScrollEnd={onUserNotificationListAboutScrollEnd}>
            {NotificationListModalBody}
          </ScrollEndDetector>
        </PopupPageModal>
      </Portal>
      {

        <Portal portalId="user-notification-content-modal">
          <PopupPageModal
            open={isUserNotificationContentOpen}
            onOpenChange={onNotificationContentModalOpenChange}
            onCloseEnd={onNotificationContentModalCloseEnd}>
            {
              activeNotification
                ? (
                  <UserNotification
                    coverImagePath={activeNotification.coverImagePath}
                    coverImageWidth={activeNotification.coverImageWidth}
                    coverImageHeight={activeNotification.coverImageHeight}
                    title={activeNotification.title}
                    content={activeNotification.content}
                    createDate={formatDate(activeNotification.createDate)} />
                )
                : null
            }
          </PopupPageModal>
        </Portal>
      }
      <div className={styles.main}>
        <Header
          userLoading={isUserLoading}
          user={user}
          hasUnreadNotification={hasUnreadNotification}
          notificationActive={isUserNotificationListOpen}
          onNotificationActiveChange={onNotificationListModalOpenChange}
          className={styles.header} />
        <section className={classnames(styles.body, styles.bodyClassName)}>
          {children}
        </section>
        <Footer
          activeType={activeType}
          className={styles.footer} />
      </div>
    </>
  );
};
