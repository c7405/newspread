import {getOrPrepareElementById} from '@/libs/dom';
import type {FC} from 'react';
import {useLayoutEffect, useState} from 'react';
import {createPortal} from 'react-dom';

export type PortalProps = {
  portalId: string;
  portalClassName?: HTMLElement['className'];
};

export const Portal: FC<PortalProps> = ({
  children,
  portalId,
  portalClassName,
}) => {
  const [portalContainerEl, setPortalContainerEl] = useState<HTMLElement | null>(null);

  useLayoutEffect(() => {
    if (!portalId) return;

    const el = getOrPrepareElementById(portalId, document.body, 'div');

    if (portalClassName) {
      el.classList.add(portalClassName);
    }

    setPortalContainerEl(el);

    return () => {
      portalContainerEl?.parentNode?.removeChild(portalContainerEl);
    };
  }, [portalId, portalClassName, portalContainerEl]);

  return portalContainerEl
    ? createPortal(children, portalContainerEl)
    : null;
};
