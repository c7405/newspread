import type {VFC, ReactNode} from 'react';
import classnames from 'classnames';
import capitalize from 'lodash.capitalize';
import {Button, ModalContainer} from '@/components/base';
import type {Variant} from '@/types/components';
import type {ModalContainerProps} from '@/components/base/ModalContainer';
import {VoidEventHandler} from '@/types/components';
import styles from './styles.module.scss';

export type AlertModalProps = {
  open: ModalContainerProps['open'],
  title: string;
  titleVariant: 'default' | 'danger';
  message: string | ReactNode;
  buttonText?: string;
  buttonVariant?: Variant;
  onConfirm: VoidEventHandler;
  onCloseEnd: ModalContainerProps['onCloseEnd'];
};

export const AlertModal: VFC<AlertModalProps> = props => {
  const {
    open,
    title,
    titleVariant = 'default',
    message,
    buttonText = 'OK',
    buttonVariant = 'default',
    onConfirm,
    onCloseEnd,
  } = props;

  const header = (
    <div className={
      classnames(
        styles.containerModalTitle,
        styles[`textTitle${capitalize(titleVariant)}`],
      )}>
      {title}
    </div>
  );

  return (
    <ModalContainer
      open={open}
      header={header}
      body={<div className={styles.containerModalBody}>{message}</div>}
      footer={
        <div className={styles.containerModalFooter}>
          <Button
            variant={buttonVariant}
            onClick={onConfirm}>
            {buttonText}
          </Button>
        </div>
      }
      onCloseEnd={onCloseEnd} />
  );
};

