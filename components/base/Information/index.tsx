import type {VFC} from 'react';
import type {Icon} from 'grommet-icons';
import classnames from 'classnames';
import {Variant} from '@/types/components';
import styles from './styles.module.scss';

export type InformationProps = {
  icon: Icon;
  message: string;
  variant: Variant;
};

export const Information: VFC<InformationProps> = ({
  icon: InformationIcon,
  message,
  variant,
}) => (
  <div className={
    classnames(
      styles.main,
      styles[variant],
    )
  }>
    <span className={styles.containerIcon}>
      <InformationIcon color="plain" />
    </span>
    <span className={styles.textMessage}>{message}</span>
  </div>
);
