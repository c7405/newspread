import type {VFC} from 'react';
import type {Icon} from 'grommet-icons';
import styles from './styles.module.scss';

export type IconTextHeaderProps = {
  icon: Icon;
  title: string;
  subTitle: string;
};

export const IconTextHeader: VFC<IconTextHeaderProps> = ({
  icon: IconComponent,
  title,
  subTitle,
}) => (
  <header className={styles.main}>
    <div className={styles.containerIcon}>
      <IconComponent color="plain" />
    </div>
    <h2 className={styles.textTitle}>{title}</h2>
    <span className={styles.textSubTitle}>{subTitle}</span>
  </header>
);
