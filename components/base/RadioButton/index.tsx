import {forwardRef} from 'react';
import type {InputHTMLAttributes, ChangeEventHandler} from 'react';
import {Radial, RadialSelected} from 'grommet-icons';
import classnames from 'classnames';
import styles from './styles.module.scss';

export type RadioButtonProps = InputHTMLAttributes<HTMLInputElement> & {
  type?: never;
  checked: boolean;
  onChange: ChangeEventHandler<HTMLInputElement>;
};

export const RadioButton = forwardRef<HTMLInputElement, RadioButtonProps>((
  {
    checked,
    className,
    ...otherProps
  },
  ref,
) => (
  <span className={styles.main}>
    <input
      {...otherProps}
      ref={ref}
      checked={checked}
      className={classnames(className, styles.input)}
      type="radio" />
    <span className={styles.containerIconInput}>
      {
        checked
          ? <RadialSelected color="plain" />
          : <Radial color="plain" />
      }
    </span>
  </span>
));

RadioButton.displayName = 'RadioButton';
