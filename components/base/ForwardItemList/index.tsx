import type {VFC, ReactElement} from 'react';
import type {ForwardItemProps} from '@/components/base/ForwardItem';
import styles from './styles.module.scss';
import {ClickActiveHighlight} from '../ClickActiveHighlight';

export type ForwardItemListProps = {
  children: ReactElement<ForwardItemProps> | ReactElement<ForwardItemProps>[];
};

export const ForwardItemList: VFC<ForwardItemListProps> = ({children = []}) => {
  const normalizedChildren = Array.isArray(children) ? children : [children];

  return (
    <ul className={styles.main}>
      {
        normalizedChildren.map((childNode, idx) => (
          <li
            key={idx}
            className={styles.containerForwardItem}>
            <ClickActiveHighlight>
              {childNode}
            </ClickActiveHighlight>
          </li>
        ))
      }
    </ul>
  );
};
