import {VFC, useState, useEffect} from 'react';
import classnames from 'classnames';
import {FormView, FormViewHide} from 'grommet-icons';
import {Input, InputProps} from '@/components/base/Input';
import {useFocus} from '@/hooks/useFocus';
import styles from './styles.module.scss';

export type PasswordInputProps = InputProps;

export const PasswordInput: VFC<PasswordInputProps> = props => {
  const [inputElRef, setInputElFocus] = useFocus();
  const [shouldFocus, setShouldFocus] = useState<boolean>(false);
  const [isPasswordMasked, setIsPasswordMasked] = useState<boolean>(true);
  const onPasswordMaskStateChange = (passwordMaskState: boolean): void => {
    setIsPasswordMasked(passwordMaskState);
    setShouldFocus(true);
  };

  useEffect(
    () => {
      if (!shouldFocus) return;

      setInputElFocus();
      setShouldFocus(false);
    },
    [setInputElFocus, shouldFocus, setShouldFocus],
  );

  return (
    <span className={styles.main}>
      <Input
        {...props}
        ref={inputElRef}
        className={styles.input}
        type={isPasswordMasked ? 'password' : 'text'} />
      <span
        className={classnames(
          styles.passwordMaskSwitch,
          {[styles.active]: !isPasswordMasked},
        )}>
        {
          isPasswordMasked
            ? <FormViewHide onClick={() => onPasswordMaskStateChange(false)} />
            : <FormView onClick={() => onPasswordMaskStateChange(true)}/>
        }
      </span>
    </span>
  );
};
