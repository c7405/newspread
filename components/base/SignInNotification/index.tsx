import {Button} from '@/components/base';
import {User} from 'grommet-icons';
import Link from 'next/link';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';

export type SignInNotificationProps = HTMLAttributes<HTMLDivElement> & {
  title?: string;
  message: string;
}

export const SignInNotification: VFC<SignInNotificationProps> = ({title = 'Welcome!', message}) => (
  <div className={styles.main}>
    <div className={styles.containerBgIconText}>
      <User color="plain" />
      <header className={styles.headerBgIconText}>{title}</header>
      <span className={styles.bodyBgIconText}>{message}</span>
      <div className={styles.containerBgIconTextBtn}>
        <Button variant="primary">
          <Link
            href="/auth/sign-in"
            passHref>
            <div className={styles.textSignIn}>Sign In</div>
          </Link>
        </Button>
      </div>
    </div>
  </div>
);
