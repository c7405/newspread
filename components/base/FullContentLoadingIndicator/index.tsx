import type {VFC} from 'react';
import {LoadingIcon} from '@/components/base';
import styles from './styles.module.scss';

export const FullContentLoadingIndicator: VFC = () => (
  <div className={styles.main}>
    <div className={styles.containerLoadingIcon}>
      <LoadingIcon
        width={60}
        height={60}
        radius={16} />
    </div>
  </div>
);
