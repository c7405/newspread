import {forwardRef} from 'react';
import type {InputHTMLAttributes, ChangeEventHandler} from 'react';
import {
  Checkbox as CheckboxIcon,
  CheckboxSelected as CheckboxSelectedIcon,
} from 'grommet-icons';
import classnames from 'classnames';
import styles from './styles.module.scss';

export type CheckboxProps = InputHTMLAttributes<HTMLInputElement> & {
  type?: never;
  checked: boolean;
  onChange: ChangeEventHandler<HTMLInputElement>;
};

export const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>((
  {
    checked,
    className,
    ...otherProps
  },
  ref,
) => (
  <span className={styles.main}>
    <input
      {...otherProps}
      ref={ref}
      checked={checked}
      className={classnames(className, styles.input)}
      type="checkbox" />
    <span className={styles.containerIconInput}>
      {
        checked
          ? <CheckboxSelectedIcon color="plain" />
          : <CheckboxIcon color="plain" />
      }
    </span>
  </span>
));

Checkbox.displayName = 'Checkbox';
