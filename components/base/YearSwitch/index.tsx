import type {VFC} from 'react';
import classnames from 'classnames';
import {CaretLeftFill, CaretRightFill} from 'grommet-icons';
import styles from './styles.module.scss';

export type YearSwitchProps = {
  minYear: number;
  year: number;
  onSwitch: (year: number) => void;
};

export const YearSwitch: VFC<YearSwitchProps> = ({minYear, year, onSwitch}) => {
  const isLeftArrowDisabled = minYear === year;

  return (
    <div className={styles.main}>
      <div
        className={
          classnames(
            styles.containerSwitchArrow,
            {
              [styles.disabled]: isLeftArrowDisabled,
            },
          )
        }
        onClick={() => {
          if (isLeftArrowDisabled) return;

          const newYear = year - 1;
  
          onSwitch(newYear < 0 ? 0 : newYear);
        }}>
        <CaretLeftFill color="plain" />
      </div>
      <time className={styles.textYear}>{year}</time>
      <div
        className={styles.containerSwitchArrow}
        onClick={() => onSwitch(year + 1)}>
        <CaretRightFill color="plain" />
      </div>
    </div>
  );
};
