import {forwardRef} from 'react';
import type {AnchorHTMLAttributes} from 'react';
import classnames from 'classnames';
import styles from './styles.module.scss';

export type AnchorProps = AnchorHTMLAttributes<HTMLAnchorElement>;

export const Anchor = forwardRef<HTMLAnchorElement, AnchorProps>(
  ({className, children, ...otherProps}, ref) => (
    <a
      {...otherProps}
      ref={ref}
      className={classnames(className, styles.main)}
      rel="noopener noreferrer">
      {children}
    </a>
  ),
);

Anchor.displayName = 'Anchor';
