import type {FC, MouseEventHandler} from 'react';
import {FormNext} from 'grommet-icons';
import styles from './styles.module.scss';

export type ForwardItemProps = {
  onClick?: MouseEventHandler<HTMLDivElement>;
};

export const ForwardItem: FC<ForwardItemProps> = ({children, onClick}) => (
  <div
    className={styles.main}
    onClick={onClick}>
    <span className={styles.textItemText}>{children}</span>
    <FormNext color="plain"/>
  </div>
);
