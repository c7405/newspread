import {CircleInformation} from 'grommet-icons';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type EmptyContentIndicatorProps = {
  message?: string;
};

export const EmptyContentIndicator: VFC<EmptyContentIndicatorProps> = ({message}) => (
  <div className={styles.main}>
    <div className={styles.containerBgIconText}>
      <CircleInformation color="plain" />
      <span>{message}</span>
    </div>
  </div>
);
