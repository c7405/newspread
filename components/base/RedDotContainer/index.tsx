import type {FC, HTMLAttributes} from 'react';
import classnames from 'classnames';
import styles from './styles.module.scss';

export type RedDotContainerProps = HTMLAttributes<HTMLDivElement> & {
  isActive: boolean;
};

export const RedDotContainer: FC<RedDotContainerProps> = ({className, isActive, children}) => (
  <div className={classnames(className, styles.main, {[styles.active]: isActive})}>
    {children}
  </div>
);
