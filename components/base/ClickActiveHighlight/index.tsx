import {createPoint, Point} from '@/formatters/page/size';
import classnames from 'classnames';
import type {FC, HTMLAttributes, ReactNode} from 'react';
import {useEffect, useRef, useState} from 'react';
import styles from './styles.module.scss';

export type ClickActiveHighlightProps = HTMLAttributes<HTMLDivElement> & {
  children: ReactNode;
  disabled?: boolean;
};

export const ClickActiveHighlight: FC<ClickActiveHighlightProps> = props => {
  const {
    disabled,
    children,
    className,
    onClick,
  } = props;
  const [isSvgVisible, setIsSvgVisible] = useState<boolean>(false);
  const [clickPoint, setClickPoint] = useState<Point | undefined>();
  const [clickPointPer, setClickPointPer] = useState<Point | undefined>();
  const mainElRef = useRef<HTMLDivElement | null>(null);
  const svgAnimateElRef = useRef<SVGAnimateElement | null>(null);

  const onClickHighlight: ClickActiveHighlightProps['onClick']= e => {
    if (disabled) return;

    const {pageX, pageY, currentTarget} = e;
    const {x, y, width, height} = currentTarget.getBoundingClientRect();
    const checkPoint = createPoint(pageX - x, pageY - y);
    const checkPointPer = createPoint(
      +(checkPoint.x / width).toFixed(1),
      +(checkPoint.y / height).toFixed(1),
    );

    setIsSvgVisible(true);
    setClickPoint(checkPoint);
    setClickPointPer(checkPointPer);
    onClick?.(e);
  };

  useEffect(() => {
    const onend = () => {
      setIsSvgVisible(false);
      setClickPoint(undefined);
    };

    const svgElement = svgAnimateElRef.current;

    if (svgElement) {
      svgElement.addEventListener('endEvent', onend);
    }

    return () => {
      if (svgElement) {
        svgElement.removeEventListener('endEvent', onend);
      }
    };
  }, [svgAnimateElRef, isSvgVisible, setIsSvgVisible, setClickPoint]);

  return (
    <div
      ref={mainElRef}
      className={classnames(className, styles.main)}
      onClick={onClickHighlight}>
      {
        mainElRef && isSvgVisible && clickPoint && clickPointPer && (
          <svg
            className={styles.containerSvgClickHighlight}
            width={mainElRef.current?.offsetWidth}
            height={mainElRef.current?.offsetHeight}
            xmlns="http://www.w3.org/2000/svg">
            <defs>
              <radialGradient
                id="clickHighlightRadialGradient"
                cx={clickPointPer.x}
                cy={clickPointPer.y}>
                <stop
                  offset="10%"
                  stopColor="#f3f4f6" />
                <stop
                  offset="100%"
                  stopColor="#e5e7eb" />
              </radialGradient>
            </defs>
            <circle
              className={styles.bodySvgClickHighlight}
              cx={clickPoint.x}
              cy={clickPoint.y}
              r={(mainElRef.current?.offsetWidth ?? 0) / 2}
              fill="url('#clickHighlightRadialGradient')">
              <animate
                ref={svgAnimateElRef}
                attributeName="r"
                dur="0.14s"
                from={(mainElRef.current?.offsetWidth ?? 0) / 2}
                to={(mainElRef.current?.offsetWidth ?? 0) * 1}
                fill="freeze"
                restart="always"
                calcMode="spline"
                keyTimes="0; 1"
                keySplines=".16, .34, .79, .94"
                repeatCount="1" />
            </circle>
          </svg>
        )
      }
      {children}
    </div>
  );
};
