import {LoadingIcon} from '@/components/base';
import type {AsyncVoidEventHandler, VoidEventHandler} from '@/types/components';
import type {VFC} from 'react';
import {useState, useEffect, useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import styles from './styles.module.scss';

type OnUndoTextClick = () => Promise<void>

export type UndoToastProps = {
  open: boolean;
  message: string;
  undoText: string;
  autoCloseDelay?: number;
  onUndo: AsyncVoidEventHandler;
  onClose: VoidEventHandler;
};

export const UndoToast: VFC<UndoToastProps> = props => {
  const {
    open,
    message,
    undoText,
    autoCloseDelay = 5000,
    onUndo,
    onClose,
  } = props;
  const [isProcessing, setIsProcessing] = useState<boolean>(false);
  const mainElRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const notificationCloseTimer = window.setTimeout(() => {
      if (!open || isProcessing) return;

      onClose();
    }, autoCloseDelay);

    return () => {
      window.clearTimeout(notificationCloseTimer);
    };
  });

  const onUndoTextClick: OnUndoTextClick = async () => {
    setIsProcessing(true);

    try {
      await onUndo();

      onClose();
    } finally {
      setIsProcessing(false);
    }
  };

  const AsideRight: VFC = () => (
    isProcessing
      ? (
        <div className={styles.containerLoadingIcon}>
          <LoadingIcon 
            width={32}
            height={32} />
        </div>
      )
      : (
        <div
          className={styles.textUndo}
          onClick={onUndoTextClick}>
          {undoText}
        </div>
      )
  );

  return (
    <div className={styles.main}>
      <CSSTransition
        nodeRef={mainElRef}
        in={open}
        timeout={300}
        classNames={{
          enter: styles.fadeTransitionEnter,
          enterActive: styles.fadeTransitionEnterActive,
          exit: styles.fadeTransitionExit,
          exitActive: styles.fadeTransitionExitActive,
        }}
        unmountOnExit
        onExit={onClose}>
        <div
          ref={mainElRef}
          className={styles.body}>
          <div className={styles.textMessage}>{message}</div>
          <div className={styles.containerAsideRight}>
            <AsideRight />
          </div>
        </div>
      </CSSTransition>
    </div>
  );
};
