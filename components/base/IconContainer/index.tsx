import type {VFC, HTMLAttributes} from 'react';
import classnames from 'classnames';
import styles from './styles.module.scss';
import {ClickActiveHighlight} from '../ClickActiveHighlight';

export type IconContainerProps = HTMLAttributes<HTMLDivElement> & {
  isStatic?: boolean;
  isActive?: IconContainerProps['isStatic'] extends true
    ? never
    : IconContainerProps['preventActive'] extends true
      ? never
      : boolean;
  preventActive?: IconContainerProps['isStatic'] extends true ? never : boolean;
  size?: 'small' | 'medium' | 'large' | 'xlarge' | 'x2large' | 'x3large';
};

export const IconContainer: VFC<IconContainerProps> = ({
  children,
  className,
  size = 'medium',
  isActive = false,
  isStatic = true,
  preventActive = false,
  ...otherProps
}) => (
  <ClickActiveHighlight className={styles.main}>
    <div
      {...otherProps}
      className={
        classnames(
          className,
          styles.containerIcon,
          styles[size],
          {
            [styles.static]: isStatic,
            [styles.active]: isActive,
            [styles.preventActive]: preventActive,
          },
        )
      }>

      {children}
    </div>
  </ClickActiveHighlight>
);


