import classnames from 'classnames';
import debounce from 'lodash.debounce';
import type {FC, HTMLAttributes, UIEventHandler} from 'react';
import styles from './styles.module.scss';

export type ScrollEndDetectorProps = HTMLAttributes<HTMLDivElement> & {
  scrollEndInterval?: number;
  scrollEndTolerance?: number;
  onAboutScrollEnd?: UIEventHandler<HTMLDivElement>;
};

export const ScrollEndDetector: FC<ScrollEndDetectorProps> = props => {
  const {
    className,
    children,
    onScroll,
    scrollEndInterval = 10,
    scrollEndTolerance = 600,
    onAboutScrollEnd,
    ...otherProps
  } = props;

  const onDetectorScroll: UIEventHandler<HTMLDivElement> = debounce(e => {
    onScroll?.(e);

    if (!(e.target instanceof HTMLElement)) {
      return;
    }

    const {scrollHeight, scrollTop, offsetHeight} = e.target;

    if ((scrollHeight - scrollTop - offsetHeight) > scrollEndTolerance) return;

    onAboutScrollEnd?.(e);
  }, scrollEndInterval);

  return (
    <div
      {...otherProps}
      className={classnames(className, styles.main)}
      onScroll={onDetectorScroll}>
      {children}
    </div>
  );
};
