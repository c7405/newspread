import {VFC} from 'react';
import {CircleInformation} from 'grommet-icons';
import styles from './styles.module.scss';

export type ValidationMessageProps = {
  message: string | null;
};

export const ValidationMessage: VFC<ValidationMessageProps> = ({message}) => {
  if (!message) return null;

  return (
    <div className={styles.main}>
      <CircleInformation
        color="plain"
        className={styles.iconInfo} />
      <span>{message}</span>
    </div>
  );
};
