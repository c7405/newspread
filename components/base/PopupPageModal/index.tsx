import type {VoidEventHandler} from '@/types/components';
import {FormPrevious} from 'grommet-icons';
import type {FC} from 'react';
import {useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import styles from './styles.module.scss';

export type PopupPageModalProps = {
  open: boolean;
  onOpenChange: (open: boolean) => void;
  title?: string;
  onCloseEnd?: VoidEventHandler;
};

export const PopupPageModal: FC<PopupPageModalProps> = ({
  children,
  open,
  title,
  onOpenChange,
  onCloseEnd,
}) => {
  const modalRef = useRef<HTMLDivElement | null>(null);

  return (
    <CSSTransition
      nodeRef={modalRef}
      in={open}
      timeout={400}
      classNames={{
        enter: styles.mainTransitionEnter,
        enterActive: styles.mainTransitionEnterActive,
        exit: styles.mainTransitionExit,
        exitActive: styles.mainTransitionExitActive,
      }}
      unmountOnExit
      onExited={onCloseEnd}>
      <div
        ref={modalRef}
        className={styles.main}>
        <header className={styles.header}>
          <aside className={styles.containerBackTextIcon}>
            <FormPrevious color="plain" />
            <span
              className={styles.textBack}
              onClick={() => onOpenChange(!open)}>
              Back
            </span>
          </aside>
          <div className={styles.containerTitle}>
            {title}
          </div>
        </header>
        <section className={styles.body}>{children}</section>
      </div>
    </CSSTransition>
  );
};
