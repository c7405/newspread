import {AlertModal, Portal} from '@/components/base';
import type {AlertModalProps} from '@/components/base/AlertModal';
import {useAppErrorModal} from '@/hooks/app';
import {appStatusCodeMessageMap} from '@cornerstone/app-status-code';
import {HTTPError} from 'ky';
import {useRouter} from 'next/router';
import type {FC, ReactNode} from 'react';
import {useEffect, useState} from 'react';

export const AppErrorContainer: FC = ({children}) => {
  const [isAlertModalOpen, setIsAlertModalOpen] = useState<boolean>(false);
  const [alertModalMessage, setAlertModalMessage] = useState<string | ReactNode>('');
  const {
    title,
    titleVariant,
    getAppErrorMessage,
    resetModal,
  } = useAppErrorModal();
  const router = useRouter();

  useEffect(() => {
    const onUnhandledRejection: (event: PromiseRejectionEvent) => Promise<void> = async e => {
      e.preventDefault();

      const error = e.reason;

      let errorMessage: string | ReactNode = error.message;

      if (error instanceof HTTPError) {
        const bodyContent = await error.response.clone().json();
        const code = bodyContent?.error?.code;

        if (code in appStatusCodeMessageMap) {
          errorMessage = getAppErrorMessage(code);
        }
      }

      setAlertModalMessage(errorMessage);
      setIsAlertModalOpen(true);
    };

    window.addEventListener('unhandledrejection', onUnhandledRejection);

    return () => {
      window.removeEventListener('unhandledrejection', onUnhandledRejection);
    };
  });

  useEffect(() => {
    setIsAlertModalOpen(false);
  }, [router.asPath]);

  const onAlertModalConfirm: AlertModalProps['onConfirm'] = ()=> {
    setIsAlertModalOpen(false);
  };

  const onAlertModalCloseEnd: AlertModalProps['onCloseEnd'] = ()=> {
    resetModal();
  };

  return (
    <>
      <Portal portalId="alert-modal">
        <AlertModal
          open={isAlertModalOpen}
          message={alertModalMessage}
          title={title}
          titleVariant={titleVariant}
          onConfirm={onAlertModalConfirm}
          onCloseEnd={onAlertModalCloseEnd} />
      </Portal>
      {children}
    </>
  );
};
