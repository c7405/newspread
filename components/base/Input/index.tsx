import React, {InputHTMLAttributes} from 'react';
import classnames from 'classnames';
import debounce from 'lodash.debounce';
import styles from './styles.module.scss';

export type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  fieldName: string;
  hasError?: boolean;
  lazy?: boolean;
  lazyInterval?: number;
};

export const Input = React.forwardRef<HTMLInputElement, InputProps>((
  {
    fieldName,
    hasError,
    lazy,
    lazyInterval,
    onChange,
    className,
    ...otherProps
  },
  ref,
) => {
  let onInputChange = onChange;

  if (onInputChange) {
    onInputChange = lazy ? debounce(onInputChange, lazyInterval ?? 500) : onInputChange;
  }

  return (
    <span className={styles.main}>
      <input
        {...otherProps}
        ref={ref}
        placeholder={fieldName}
        className={classnames(className, styles.input, {[styles.hasError]: hasError})}
        onChange={onInputChange} />
      <span className={classnames(styles.fieldName, {[styles.hasError]: hasError})}>
        {fieldName}
      </span>
    </span>
  );
});

Input.displayName = 'Input';

