import type {VFC, ButtonHTMLAttributes} from 'react';
import {Variant} from '@/types/components';
import classNames from 'classnames';
import {LoadingIcon} from '@/components/base/LoadingIcon';
import styles from './styles.module.scss';

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  /* Button variant */
  variant?: Variant;
  
  /* Button status for loading */
  loading?: boolean;
}

export const Button: VFC<ButtonProps> = ({
  variant = 'default',
  className,
  children,
  loading,
  disabled,
  ...buttonProps
}) => (
  <button
    {...buttonProps}
    disabled={loading || disabled}
    className={classNames(className, styles.button, styles[variant])}>
    {
      loading
        ? (
          <div className={styles.iconLoadingIcon}>
            <LoadingIcon />
          </div>
        )
        : children
    }
  </button>
);
