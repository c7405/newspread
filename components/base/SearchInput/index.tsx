import {useState, useEffect} from 'react';
import type {VFC} from 'react';
import {FormClose} from 'grommet-icons';
import {Input, IconContainer} from '@/components/base';
import type {InputProps} from '@/components/base/Input';
import {useFocus} from '@/hooks/useFocus';
import styles from './styles.module.scss';

export type SearchInputProps = InputProps;

type OnInputClear = () => void;

export const SearchInput: VFC<SearchInputProps> = ({value, ...otherProps}) => {
  const [inputElRef, setInputElFocus] = useFocus();
  const [shouldFocus, setShouldFocus] = useState<boolean>(false);

  useEffect(
    () => {
      if (!shouldFocus) return;

      setInputElFocus();
      setShouldFocus(false);
    },
    [setInputElFocus, shouldFocus, setShouldFocus],
  );

  const onInputClear: OnInputClear = () =>{
    const inputEl = inputElRef.current;

    if (!inputEl) return;

    Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value')
      ?.set
      ?.call(inputElRef.current, '');

    inputEl.dispatchEvent(
      new Event('change', {bubbles: true}),
    );

    setShouldFocus(true);
  };

  return (
    <span className={styles.main}>
      <Input
        {...otherProps}
        ref={inputElRef}
        className={styles.input}
        type="text" />
      <span
        className={styles.containerIconFormClose}>
        {
          value &&
          <IconContainer isStatic={false}>
            <FormClose
              color="plain"
              onClick={onInputClear} />
          </IconContainer>
        }
      </span>
    </span>
  );
};
