import {useImageFormat} from '@/hooks/page';
import type {ImageProps} from 'next/image';
import Image from 'next/image';
import type {VFC} from 'react';

export const AppImage: VFC<ImageProps> = props => {
  const {src, alt} = props;
  const {imageLoader} = useImageFormat({src});

  return (
    <Image
      {...props}
      alt={alt}
      loader={imageLoader} />
  );
};
