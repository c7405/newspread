import type {AsyncVoidEventHandler, VoidEventHandler} from '@/types/components';
import type {ReactChild, VFC} from 'react';
import {useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import styles from './styles.module.scss';

export type ModalContainerProps = {
  open: boolean;
  header: ReactChild,
  body: ReactChild,
  footer?: ReactChild,
  onCloseEnd: VoidEventHandler | AsyncVoidEventHandler;
};

export const ModalContainer: VFC<ModalContainerProps> = props => {
  const {
    open,
    header,
    body,
    footer,
    onCloseEnd,
  } = props;
  const mainElRef = useRef<HTMLDivElement | null>(null);

  return (
    <CSSTransition
      nodeRef={mainElRef}
      in={open}
      timeout={400}
      classNames={{
        enter: styles.mainTransitionEnter,
        enterActive: styles.mainTransitionEnterActive,
        exit: styles.mainTransitionExit,
        exitActive: styles.mainTransitionExitActive,
      }}
      unmountOnExit
      onExited={onCloseEnd}>
      <div
        ref={mainElRef}
        className={styles.main}>
        <div className={styles.containerModal}>
          <header className={styles.containerHeader}>{header}</header>
          <section className={styles.containerBody}>{body}</section>
          {
            footer && (
              <section className={styles.containerFooter}>{footer}</section>
            )
          }
        </div>
      </div>
    </CSSTransition>
  );
};
