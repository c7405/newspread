import {useEffect, useRef, useState, useCallback, useMemo} from 'react';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type LoadingIconProps = {
  radius?: number;
  width?: number;
  height?: number;
  stroke?: string;
  strokeWidth?: number;
};

export const LoadingIcon: VFC<LoadingIconProps> = ({
  width = 40,
  height = 40,
  radius = 8,
  stroke = 'black',
  strokeWidth = 2,
}) => {
  const defaultStartX = width / 2 - radius;
  const defaultStartY = height / 2;
  const defaultEndX = defaultStartX + radius;
  const defaultEndY = defaultStartY + radius;

  const requestAnimationFrameIdRef = useRef<ReturnType<typeof window.requestAnimationFrame> | null>(null);
  const [startX, setStartX] = useState<number>(defaultStartX);
  const [startY, setStartY] = useState<number>(defaultStartY);
  const [endX, setEndX] = useState<number>(defaultStartX);
  const [endY, setEndY] = useState<number>(defaultStartY);
  const [frameIdx, setFrameIdx] = useState<number>(-1);
  const [largeArc, setLargeArc] = useState<boolean>(false);

  const clockwisePositions = useMemo(() => {
    const clockwisePositionsValue: [number, number, number, number, boolean][] = [];
    const counterClockwisePositionsValue: [number, number, number, number, boolean][] = [];

    const {cos, sin, PI} = Math;
    const radiants = PI / 180;
    const offsetDegree = 6;

    let nextDegree = 180;

    while (nextDegree > -90) {
      const posX = defaultStartX + cos(nextDegree * radiants) * radius + radius;
      const posY = defaultStartY - sin(nextDegree * radiants) * radius;
      const isStartLargeArc = nextDegree < 0;
      const isEndLargeArc = nextDegree > 90;

      clockwisePositionsValue.push([
        defaultStartX,
        defaultStartY,
        posX,
        posY,
        isStartLargeArc,
      ]);
      counterClockwisePositionsValue.push([
        posX,
        posY,
        defaultEndX,
        defaultEndY,
        isEndLargeArc,
      ]);

      nextDegree -= offsetDegree;
    }

    return [
      ...clockwisePositionsValue,
      ...counterClockwisePositionsValue,
      ...Array(16).fill('').map<[number, number, number, number, boolean]>(() => [
        defaultStartX,
        defaultStartY,
        defaultStartX,
        defaultStartY,
        true,
      ]),
    ];
  }, [radius, defaultStartX, defaultStartY, defaultEndX, defaultEndY]);

  const animate = useCallback(
    () => {
      let nextFrameIdx = frameIdx + 1;

      nextFrameIdx = (nextFrameIdx >= clockwisePositions.length) ? 0 : nextFrameIdx;

      setFrameIdx(nextFrameIdx);

      const [
        nextStartX,
        nextStartY,
        nextEndX,
        nextEndY,
        isLargeArc,
      ] = clockwisePositions[nextFrameIdx];

      setStartX(nextStartX);
      setStartY(nextStartY);
      setEndX(nextEndX);
      setEndY(nextEndY);
      setLargeArc(isLargeArc);
    },
    [
      clockwisePositions,
      frameIdx,
      setFrameIdx,
      setStartX,
      setStartY,
      setEndX,
      setEndY,
      setLargeArc,
    ]);

  useEffect(() => {
    const requestAnimationFrameId = window.requestAnimationFrame(animate);

    requestAnimationFrameIdRef.current = requestAnimationFrameId;

    return () => cancelAnimationFrame(requestAnimationFrameId);
  }, [animate]);

  return (
    <div
      style={{
        width: `${width}px`,
        height: `${height}px`,
      }}
      className={styles.main}>
      <svg
        width={width}
        height={height}
        xmlns="http://www.w3.org/2000/svg">
        <path
          d={`M${startX} ${startY} A ${radius} ${radius}, 0, ${+largeArc}, 1, ${endX} ${endY}`}
          fillOpacity={0}
          stroke={stroke}
          strokeWidth={strokeWidth} />
      </svg>
    </div>
  );
};
