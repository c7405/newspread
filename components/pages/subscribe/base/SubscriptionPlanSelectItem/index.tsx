import {RadioButton} from '@/components/base';
import type {SubscriptionPlanMap, SubscriptionPlanType} from '@/types/resources';
import classnames from 'classnames';
import type {FC} from 'react';
import styles from './styles.module.scss';

export type SubscriptionPlanSelectItemProps = {
  plan: SubscriptionPlanType;
  subscriptionPlanMap: SubscriptionPlanMap;
  disabled: boolean;
  selected: boolean;
  onSelectedChange: (selected: boolean) => void | Promise<void>;
};

export const SubscriptionPlanSelectItem: FC<SubscriptionPlanSelectItemProps> = ({
  plan,
  subscriptionPlanMap,
  disabled,
  selected,
  onSelectedChange,
}) => {
  const {description, price, unit} = subscriptionPlanMap[plan];
  const radioId = `subscription-plan-${description}`;

  return (
    <label
      htmlFor={radioId}
      className={
        classnames(
          styles.main,
          {
            [styles.selected]: selected,
            [styles.disabled]: disabled,
          },
        )
      }>
      <span className={styles.containerRadioButton}>
        <RadioButton
          id={radioId}
          disabled={disabled}
          checked={selected}
          onChange={e => onSelectedChange(e.target.checked)} />
      </span>
      <span className={styles.textSubscriptionPlanText}>{description}</span>
      <span className={styles.textSubscriptionPlanPriceUnit}>
        ${`${price} / ${unit}`}
      </span>
    </label>
  );
};
