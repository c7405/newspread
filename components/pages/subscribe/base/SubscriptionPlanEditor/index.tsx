import {Button, IconTextHeader, Information} from '@/components/base';
import {SubscriptionPlanSelectItem} from '@/components/pages/subscribe/base';
import type {SubscriptionPlanSelectItemProps} from '@/components/pages/subscribe/base/SubscriptionPlanSelectItem';
import {useValidation} from '@/hooks/useValidation';
import type {AsyncVoidEventHandler} from '@/types/components';
import type {SubscriptionPlanType} from '@/types/resources';
import {updateSubscriptionPlanSchema} from '@/validators/subscription';
import {Info, Shop} from 'grommet-icons';
import type {VFC} from 'react';
import {useState} from 'react';
import styles from './styles.module.scss';

export type SubscriptionPlanEditorProps = {
  headerTitle: string;
  subscriptionPlan: SubscriptionPlanType | null;
  subscriptionPlanMap: SubscriptionPlanSelectItemProps['subscriptionPlanMap'];
  onCheckout: (data: {subscriptionPlan: SubscriptionPlanType}) => Promise<void>;
};

export const SubscriptionPlanEditor: VFC<SubscriptionPlanEditorProps> = ({
  headerTitle,
  subscriptionPlan,
  subscriptionPlanMap,
  onCheckout,
}) => {
  const [
    isDataChanged,
    validationData,
    setValidationDataChange,
    valildationError,
  ] = useValidation(
    {
      subscriptionPlan,
    },
    updateSubscriptionPlanSchema,
  );

  const [isCheckoutProcessing, setIsCheckoutProcessing] = useState<boolean>(false);

  const isEditorDisabled = !!subscriptionPlan;

  const subscriptionPlans: SubscriptionPlanType[] = ['MONTHLY', 'ANNUAL'];

  const onCheckoutBtnClick: AsyncVoidEventHandler = async () => {
    const validSubscriptionPlan = validationData?.subscriptionPlan;

    if (!validSubscriptionPlan) return;

    setIsCheckoutProcessing(true);

    try {
      await onCheckout({subscriptionPlan: validSubscriptionPlan});
    } finally {
      setIsCheckoutProcessing(false);
    }
  };

  return (
    <div className={styles.main}>
      {
        isEditorDisabled && (
          <Information
            variant="warning"
            icon={Info}
            message="Pleaese cancel the current subscription before customing your new plan." />
        )
      }
      <IconTextHeader
        icon={Shop}
        title={headerTitle}
        subTitle="Get full access to all the content, and you can cancel the subscription at any time." />
      <div className={styles.containerInputField}>
        {
          subscriptionPlans.map(plan => (
            <SubscriptionPlanSelectItem
              key={plan}
              plan={plan}
              subscriptionPlanMap={subscriptionPlanMap}
              disabled={isEditorDisabled}
              selected={validationData?.subscriptionPlan === plan}
              onSelectedChange={() => setValidationDataChange({subscriptionPlan: plan})} />
          ))
        }
      </div>
      <footer className={styles.footer}>
        <Button
          variant="primary"
          loading={isCheckoutProcessing}
          disabled={isCheckoutProcessing || isEditorDisabled || !isDataChanged || !!valildationError}
          onClick={onCheckoutBtnClick}>
          Checkout
        </Button>
      </footer>
    </div>
  );
};
