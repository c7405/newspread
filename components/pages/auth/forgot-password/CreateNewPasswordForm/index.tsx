import type {VFC} from 'react';
import {Button, PasswordInput, ValidationMessage} from '@/components/base';
import {useValidation} from '@/hooks/useValidation';
import {createUserNewPasswordFormSchema} from '@/validators/authentication';
import {useState} from 'react';
import styles from './styles.module.scss';

export type CreateNewPasswordData = {
  newPassword: string;
  confirmNewPassword: string;
};

export type CreateNewPasswordFormProps = {
  onSubmit: (data: CreateNewPasswordData) => Promise<void> | never;
};

export type OnCreateNewPasswordDataChange = (updateData: Partial<CreateNewPasswordData>) => void;

export type OnFormDataSubmit = () => void;

export const CreateNewPasswordForm: VFC<CreateNewPasswordFormProps> = ({onSubmit}) => {
  const [isFormSubmitting, setIsFormSubmitting] = useState<boolean>(false);

  const [
    isCreateNewPasswordDataChanged,
    createNewPasswordData,
    setCreateNewPasswordData,
    createNewPasswordDataValidationError,
    createNewPasswordDataErrorDetailMap,
  ] = useValidation(
    {
      newPassword: '',
      confirmNewPassword: '',
    },
    createUserNewPasswordFormSchema,
  );

  const onCreateNewPasswordDataChange: OnCreateNewPasswordDataChange = updateData => {
    setCreateNewPasswordData(updateData);
  };

  const onFormDataSubmit: OnFormDataSubmit = async () => {
    setIsFormSubmitting(true);

    try {
      const {newPassword, confirmNewPassword} = createNewPasswordData;

      await onSubmit({
        newPassword,
        confirmNewPassword,
      });
    } finally {
      setIsFormSubmitting(false);
    }
  };

  return (
    <div className={styles.main}>
      <section className={styles.containerInputField}>
        <div>
          <PasswordInput
            fieldName="New Password"
            hasError={!!createNewPasswordDataErrorDetailMap.newPassword}
            value={createNewPasswordData.newPassword}
            onChange={({target: {value: newPassword}}) => {onCreateNewPasswordDataChange({newPassword});}} />
          <ValidationMessage
            message={createNewPasswordDataErrorDetailMap.newPassword} />
        </div>
        <div>
          <PasswordInput
            fieldName="Confirm New Password"
            hasError={!!createNewPasswordDataErrorDetailMap.confirmNewPassword}
            value={createNewPasswordData.confirmNewPassword}
            onChange={({target: {value: confirmNewPassword}}) => {onCreateNewPasswordDataChange({confirmNewPassword});}} />
          <ValidationMessage
            message={createNewPasswordDataErrorDetailMap.confirmNewPassword} />
        </div>
      </section>
      <footer>
        <Button
          variant="primary"
          loading={isFormSubmitting}
          disabled={!isCreateNewPasswordDataChanged || !!createNewPasswordDataValidationError}
          onClick={onFormDataSubmit}>
          Create
        </Button>
      </footer>
    </div>
  );
};
