import {Button, Input, ValidationMessage} from '@/components/base';
import {useValidation} from '@/hooks/useValidation';
import {resetPasswordSchema} from '@/validators/authentication';
import type {VFC} from 'react';
import {useState} from 'react';
import {AsyncVoidEventHandler} from '@/types/components';
import styles from './styles.module.scss';

export type ResetPasswordData = {
  account: string;
};

export type OnResetPasswordDataChange = (updateData: ResetPasswordData) => void;

export type ResetPasswordFormProps = {
  onSubmit: (updateData: ResetPasswordData) => Promise<void>;
};

export const ResetPasswordForm: VFC<ResetPasswordFormProps> = ({onSubmit}) => {
  const [
    isResetPasswordDataChange,
    resetPasswordData,
    setResetPasswordDataChange,
    resetPasswordDataValildationError,
    resetPasswordDataErrorDetailMap,
  ] = useValidation(
    {
      account: '',
    },
    resetPasswordSchema,
  );

  const [isSubmiting, setIsSubmiting] = useState<boolean>(false);

  const onResetPasswordDataChange: OnResetPasswordDataChange = updateData => {
    setResetPasswordDataChange(updateData);
  };

  const onFormDataSubmit: AsyncVoidEventHandler = async () => {
    setIsSubmiting(true);

    try {
      await onSubmit(resetPasswordData);
    } finally {
      setIsSubmiting(false);
    }
  };

  return (
    <div className={styles.main}>
      <section>
        <div>
          <Input
            fieldName="Email"
            hasError={!!resetPasswordDataErrorDetailMap.account}
            value={resetPasswordData.account}
            onChange={({target: {value: account}}) => {onResetPasswordDataChange({account});}} />
          <ValidationMessage
            message={resetPasswordDataErrorDetailMap.account} />
        </div>
      </section>
      <footer>
        <Button
          variant="primary"
          loading={isSubmiting}
          disabled={!isResetPasswordDataChange || !!resetPasswordDataValildationError}
          onClick={onFormDataSubmit}>
          Send Instruction
        </Button>
      </footer>
    </div>
  );
};
