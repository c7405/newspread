import {useState} from 'react';
import type {VFC} from 'react';
import Link from 'next/link';
import {
  Anchor,
  Button,
  Checkbox,
  Input,
  PasswordInput,
  ValidationMessage,
} from '@/components/base';
import {useValidation} from '@/hooks/useValidation';
import {signUpSchema} from '@/validators/authentication';
import styles from './styles.module.scss';

export type SignUpData = {
  account: string;
  password: string;
  repassword: string;
  firstName: string;
  lastName: string;
};

export type OnSignUpDataChange = (updateData: Partial<SignUpData>) => void;

export type SignUpFormProps = {
  onSubmit: (data: SignUpData) => Promise<void>;
};

export const SignUpForm: VFC<SignUpFormProps> = ({onSubmit}) => {
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const [isTermsOfServiceRead, setIsTermsOfServiceRead] = useState<boolean>(false);

  const [
    isSignUpDataChanged,
    signUpData,
    setSignUpData,
    signUpDataValildationError,
    signUpDataErrorDetailMap,
  ] = useValidation(
    {
      account: '',
      password: '',
      repassword: '',
      firstName: '',
      lastName: '',
    },
    signUpSchema,
  );

  const onSignUpDataChange: OnSignUpDataChange = updateData => {
    setSignUpData(updateData);
  };

  const onSignUpFormSubmit = async () => {
    if (!isSignUpDataChanged || signUpDataValildationError || isSubmitting) return;

    setIsSubmitting(true);

    try {
      const {
        account,
        password,
        repassword,
        firstName,
        lastName,
      } = signUpData;

      await onSubmit({
        account,
        password,
        repassword,
        firstName,
        lastName,
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  return (
    <div className={styles.main}>
      <section className={styles.containerInputField}>
        <div>
          <Input
            fieldName="Email"
            hasError={!!signUpDataErrorDetailMap.account}
            value={signUpData.account}
            onChange={({target: {value: account}}) => {onSignUpDataChange({account});}} />
          <ValidationMessage
            message={signUpDataErrorDetailMap.account} />
        </div>
        <div>
          <PasswordInput
            fieldName="Password"
            value={signUpData.password}
            hasError={!!signUpDataErrorDetailMap.password}
            onChange={({target: {value: password}}) => {onSignUpDataChange({password});}} />
          <ValidationMessage
            message={signUpDataErrorDetailMap.password} />
        </div>
        <div>
          <PasswordInput
            fieldName="Re-enter Password"
            value={signUpData.repassword}
            hasError={!!signUpDataErrorDetailMap.repassword}
            onChange={({target: {value: repassword}}) => {onSignUpDataChange({repassword});}} />
          <ValidationMessage
            message={signUpDataErrorDetailMap.repassword} />
        </div>
        <div>
          <Input
            fieldName="First Name"
            value={signUpData.firstName}
            hasError={!!signUpDataErrorDetailMap.firstName}
            onChange={({target: {value: firstName}}) => {onSignUpDataChange({firstName});}} />
          <ValidationMessage
            message={signUpDataErrorDetailMap.firstName} />
        </div>
        <div>
          <Input
            fieldName="Last Name"
            value={signUpData.lastName}
            hasError={!!signUpDataErrorDetailMap.lastName}
            onChange={({target: {value: lastName}}) => {onSignUpDataChange({lastName});}} />
          <ValidationMessage
            message={signUpDataErrorDetailMap.lastName} />
        </div>
      </section>
      <footer className={styles.footer}>
        <span className={styles.containerTermsOfServiceNotice}>
          <Checkbox
            id="termsOfServiceCheck"
            checked={isTermsOfServiceRead}
            onChange={e => {setIsTermsOfServiceRead(e.target.checked);}} />
          <label htmlFor="termsOfServiceCheck">
            <span className={styles.termsOfServiceNotice}>
              I have read and accept the {' '}
            </span>
            <Link
              href="/terms-of-service"
              passHref>
              <Anchor target="_blank">
                Terms of Service
              </Anchor>
            </Link>
          </label>
        </span>
        <Button
          variant="primary"
          disabled={!isTermsOfServiceRead || !isSignUpDataChanged || !!signUpDataValildationError}
          loading={isSubmitting}
          onClick={onSignUpFormSubmit}>
          Sign Up
        </Button>
      </footer>
    </div>
  );
};
