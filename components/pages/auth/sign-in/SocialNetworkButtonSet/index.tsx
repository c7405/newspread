import {Button} from '@/components/base';
import type {AsyncVoidEventHandler} from '@/types/components';
import {Facebook, Google} from 'grommet-icons';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type SocialNetworkButtonSetProps = {
  onGoogleSignIn: AsyncVoidEventHandler;
  onFacebookSignIn: AsyncVoidEventHandler;
};

export const SocialNetworkButtonSet: VFC<SocialNetworkButtonSetProps> = ({
  onGoogleSignIn,
  onFacebookSignIn,
}) => (
  <div className={styles.main}>
    <Button
      className={styles.socialNetworkButton}
      onClick={() => onGoogleSignIn()}>
      <Google className={styles.socialNetworkIcon} />
    </Button>
    <Button
      className={styles.socialNetworkButton}
      onClick={() => onFacebookSignIn()}>
      <Facebook className={styles.socialNetworkIcon} />
    </Button>
  </div>
);
