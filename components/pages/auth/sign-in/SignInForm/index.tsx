import {useState} from 'react';
import type {VFC} from 'react';
import {
  Button, Input, PasswordInput,
  ValidationMessage,
} from '@/components/base';
import {useValidation} from '@/hooks/useValidation';
import styles from './styles.module.scss';
import {signInSchema} from '@/validators/authentication';

export type SignInCredential = {
  account: string;
  password: string;
};

export type OnSignInCredentialSubmit = (signInCredential: SignInCredential) => Promise<void>;

export type SignInFormProps = {
  /* Submit event handler */
  onSubmit: OnSignInCredentialSubmit;
};

type OnSignInCredentialChange = (updateData: Partial<SignInCredential>) => void;

export const SignInForm: VFC<SignInFormProps> = ({onSubmit}) => {
  const [
    isDataChange,
    signInCredential,
    setSignInCredential,
    signInCredentialValidationError,
    signInCredentialErrorMessage,
  ] = useValidation(
    {
      account: '',
      password: '',
    },
    signInSchema,
  );

  const [isFormSubmitting, setIsFormSubmitting] = useState<boolean>(false);

  const onSignInCredentialChange: OnSignInCredentialChange = updateData => {
    setSignInCredential(updateData);
  };

  const onFormSubmit = async () => {
    if (signInCredentialValidationError || !onSubmit) return;

    try {
      setIsFormSubmitting(true);
      await onSubmit(signInCredential);
    } finally {
      setIsFormSubmitting(false);
    }
  };

  return (
    <div className={styles.main}>
      <section className={styles.containerInputField}>
        <div>
          <Input
            fieldName="Email"
            hasError={!!signInCredentialErrorMessage.account}
            value={signInCredential.account}
            onChange={({target: {value: account}}) => {onSignInCredentialChange({account});}} />
          <ValidationMessage
            message={signInCredentialErrorMessage.account} />
        </div>
        <div>
          <PasswordInput
            fieldName="Password"
            value={signInCredential.password}
            hasError={!!signInCredentialErrorMessage.password}
            onChange={({target: {value: password}}) => {onSignInCredentialChange({password});}} />
          <ValidationMessage
            message={signInCredentialErrorMessage.password} />
        </div>
      </section>
      <footer>
        <Button
          variant="primary"
          loading={isFormSubmitting}
          disabled={!isDataChange || !!signInCredentialValidationError}
          onClick={onFormSubmit}>
          Sign In
        </Button>
      </footer>
    </div>
  );
};
