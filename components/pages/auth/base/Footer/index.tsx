import {VFC, HTMLAttributes} from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {Anchor} from '@/components/base';
import styles from './styles.module.scss';

export type FooterProps = HTMLAttributes<HTMLDivElement>;

export const Footer: VFC<FooterProps> = ({className}) => (
  <footer className={classnames(className, styles.main)}>
    <section>
      <Link
        href="/terms-of-service"
        passHref>
        <Anchor>
          Terms of Services
        </Anchor>
      </Link>
      <span className={styles.separator}> | </span>
      <Link
        href="/privacy-policy"
        passHref>
        <Anchor>
          Privacy Policy
        </Anchor>
      </Link>
    </section>
    <section>
      <span className={styles.locale}>English</span>
    </section>
  </footer>
);
