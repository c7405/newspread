import type {VFC, HTMLAttributes} from 'react';
import Image from 'next/image';
import Link from 'next/link';
import {Anchor} from '@/components/base';
import styles from './styles.module.scss';

export type HeaderProps = HTMLAttributes<HTMLDivElement>;

export const Header: VFC<HeaderProps> = () => (
  <header className={styles.main}>
    <Link
      href="/"
      passHref>
      <Anchor>
        <Image
          src="/images/logo.svg"
          alt="Newspread"
          layout="fixed"
          width="120px"
          height="25px" />
      </Anchor>
    </Link>
  </header>
);
