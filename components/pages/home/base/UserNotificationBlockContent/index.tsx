import {
  UserNotificationBlockContentLink,
  UserNotificationBlockContentText,
} from '@/components/pages/home/base';
import type {NotificationBlock} from '@/types/resources';
import type {VFC} from 'react';

export type UserNotificationBlockContentProps = {
  block: NotificationBlock;
};

export const UserNotificationBlockContent: VFC<UserNotificationBlockContentProps> = ({block}) => (
  <div>
    {
      block.map((content, idx) => {
        switch (content.type) {
          case 'TEXT':
            return (
              <UserNotificationBlockContentText
                {...content}
                key={idx} />
            );
          case 'LINK':
            return (
              <UserNotificationBlockContentLink
                {...content}
                key={idx} />
            );
          default:
            return null;
        }
      })
    }
  </div>
);
