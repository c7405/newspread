import {FooterIconContainer} from '@/components/pages/home/base';
import classnames from 'classnames';
import {Bookmark, Home, Search, UserSettings} from 'grommet-icons';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';
import Link from 'next/link';
import {ClickActiveHighlight} from '@/components/base';

export type ActiveType = 'home' | 'search' | 'bookmark' | 'user-profile';

export type FooterProps = HTMLAttributes<HTMLDivElement> & {
  activeType?: ActiveType;
};

export const Footer: VFC<FooterProps> = ({
  className,
  activeType,
  ...otherProps
}) => {
  return (
    <div
      {...otherProps}
      className={classnames(className, styles.main)}>
      <ClickActiveHighlight
        disabled={activeType === 'user-profile'}
        className={styles.iconClickHighlight}>
        <Link
          href="/home"
          passHref>
          <a>
            <FooterIconContainer isActive={activeType === 'home'}>
              <Home color="plain" />
            </FooterIconContainer>
          </a>
        </Link>
      </ClickActiveHighlight>
      <ClickActiveHighlight
        disabled={activeType === 'search'}
        className={styles.iconClickHighlight}>
        <Link
          href="/search"
          passHref>
          <a>
            <FooterIconContainer isActive={activeType === 'search'}>
              <Search color="plain" />
            </FooterIconContainer>
          </a>
        </Link>
      </ClickActiveHighlight>
      <ClickActiveHighlight
        disabled={activeType === 'bookmark'}
        className={styles.iconClickHighlight}>
        <Link
          href="/bookmark"
          passHref>
          <a>
            <FooterIconContainer isActive={activeType === 'bookmark'}>
              <Bookmark color="plain" />
            </FooterIconContainer>
          </a>
        </Link>
      </ClickActiveHighlight>
      <ClickActiveHighlight
        disabled={activeType === 'user-profile'}
        className={styles.iconClickHighlight}>
        <Link
          href="/user-profile"
          passHref>
          <a>
            <FooterIconContainer isActive={activeType === 'user-profile'}>
              <UserSettings color="plain" />
            </FooterIconContainer>
          </a>
        </Link>
      </ClickActiveHighlight>
    </div>
  );
};
