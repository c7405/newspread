import type {VFC} from 'react';
import dayjs from 'dayjs';
import {formatTimeRange} from '@/formatters/time';
import {humanReadDiff} from '@/libs/time';
import styles from './styles.module.scss';

export type PostMetaProps = {
  updateDate: string;
  readTime: number;
};

export const PostMeta: VFC<PostMetaProps> = ({updateDate, readTime}) => (
  <span className={styles.main}>
    <time className={styles.updateDate}>
      {
        formatTimeRange(
          humanReadDiff(updateDate, dayjs()),
          'en',
        )
      }
    </time>
    <span className={styles.separator}>|</span>
    <span className={styles.readTime}>
      {readTime}min read time
    </span>
  </span>
);
