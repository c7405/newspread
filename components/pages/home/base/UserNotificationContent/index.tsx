import {NotificationContent} from '@/types/resources';
import type {VFC} from 'react';
import styles from './styles.module.scss';
import {UserNotificationBlockContent} from '@/components/pages/home/base/UserNotificationBlockContent';

export type UserNotificationContentProps = {
  content: NotificationContent;
};

export const UserNotificationContent: VFC<UserNotificationContentProps> = ({content}) => (
  <div className={styles.main}>
    {
      content.map((block, idx) => (
        <UserNotificationBlockContent
          block={block}
          key={idx} />
      ))
    }
  </div>
);
