import type {NotificationTextContent} from '@/types/resources';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type UserNotificationBlockContentTextProps = Pick<NotificationTextContent, 'text' | 'props'>;

export const UserNotificationBlockContentText: VFC<UserNotificationBlockContentTextProps> = ({text, props}) => {
  const {style, newline} = props;
  const ComponentTag = newline ? 'div' : 'span';

  return (
    <ComponentTag
      className={styles.main}
      style={style}>
      {text}
    </ComponentTag>
  );
};
