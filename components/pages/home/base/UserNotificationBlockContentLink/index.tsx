import type {NotificationLinkContent} from '@/types/resources';
import type {VFC} from 'react';
import Link from 'next/link';
import {Anchor} from '@/components/base';

export type UserNotificationBlockContentLinkProps = Pick<NotificationLinkContent, 'text' | 'props'>;

export const UserNotificationBlockContentLink: VFC<UserNotificationBlockContentLinkProps> = ({text, props}) => {
  const {style, href} = props;

  return (
    <Link
      href={href}
      passHref>
      <Anchor style={style}>
        {text}
      </Anchor>
    </Link>
  );
};
