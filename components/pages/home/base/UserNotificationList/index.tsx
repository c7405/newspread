import {ClickActiveHighlight} from '@/components/base';
import {UserNotificationListItem} from '@/components/pages/home/base';
import type {Notification} from '@/types/resources';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type UserNotificationListProps = {
  notifications: Notification[];
  onNotificationItemClick: (config: {listIdx: number}) => void | Promise<void>;
};

export const UserNotificationList: VFC<UserNotificationListProps> = props => {
  const {notifications, onNotificationItemClick} = props;

  return (
    <ul className={styles.main}>
      {
        notifications.map((notification, listIdx) => (
          <li
            key={notification.id}
            onClick={() => {
              onNotificationItemClick({listIdx});
            }}>
            <ClickActiveHighlight>
              <UserNotificationListItem
                coverImagePath={notification.coverImagePath}
                title={notification.title}
                hasRead={notification.hasRead}
                createDate={notification.createDate} />
            </ClickActiveHighlight>
          </li>
        ))
      }
    </ul>
  );
};
