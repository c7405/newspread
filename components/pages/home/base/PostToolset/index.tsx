import type {VFC, MouseEventHandler} from 'react';
import classnames from 'classnames';
import {Bookmark, ShareOption} from 'grommet-icons';
import {IconContainer} from '@/components/base';
import styles from './styles.module.scss';

export type PostToolsetProps = {
  bookmarked?: boolean;
  allowBookmark?: boolean;
  onShareClick: MouseEventHandler<HTMLDivElement>;
  onBookmarkClick: MouseEventHandler<HTMLDivElement>;
};

export const PostToolset: VFC<PostToolsetProps> = ({
  bookmarked = false,
  allowBookmark = false,
  onShareClick,
  onBookmarkClick,
}) => (
  <div className={styles.main}>
    <IconContainer
      isStatic={false}
      size="small"
      className={styles.containerIcon}
      onClick={onShareClick}>
      <ShareOption color="plain" />
    </IconContainer>
    {
      allowBookmark && (
        <IconContainer
          isStatic={false}
          size="small"
          className={
            classnames(
              styles.containerIcon,
              styles.containerIconBookmark,
              {[styles.active]: bookmarked},
            )
          }
          preventActive
          onClick={onBookmarkClick}>
          <Bookmark color="plain" />
        </IconContainer>
      )
    }
  </div>
);
