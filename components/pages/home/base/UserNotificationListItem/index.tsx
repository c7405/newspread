import {AppImage} from '@/components/base';
import {formatTimeRange} from '@/formatters/time';
import {humanReadDiff} from '@/libs/time';
import {Notification} from '@/types/resources';
import classnames from 'classnames';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type UserNotificationListItemProps = {
  coverImagePath: Notification['coverImagePath'];
  title: Notification['title'];
  hasRead: Notification['hasRead'];
  createDate: Notification['createDate'];
};

export const UserNotificationListItem: VFC<UserNotificationListItemProps> = props => {
  const {
    coverImagePath,
    title,
    hasRead,
    createDate,
  } = props;

  return (
    <div className={classnames(styles.main, {[styles.hasRead]: hasRead})}>
      <aside className={styles.containerCoverImage}>
        <AppImage
          src={coverImagePath}
          alt="Notification Cover"
          sizes="5rem"
          layout="fill"
          objectFit="cover" />
      </aside>
      <section className={styles.containerTitle}>
        <div className={styles.textTitle}>{title}</div>
        <time className={styles.textNotificationCreateDate}>{formatTimeRange(humanReadDiff(createDate, new Date()), 'en')}</time>
      </section>
    </div>
  );
};
