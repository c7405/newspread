import {Anchor, AppImage} from '@/components/base';
import {PostFooter} from '@/components/pages/home/base';
import type {PostFooterProps} from '@/components/pages/home/base/PostFooter';
import {getImageSizes} from '@/libs/image';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type PostCardProps = PostFooterProps & {
  index: number;
  postId: number;
  coverImageSrc: string;
  title: string;
  exclusive: boolean;
};

export const PostCard: VFC<PostCardProps> = props => {
  const {
    index,
    coverImageSrc,
    title,
    exclusive,
    updateDate,
    readTime,
    bookmarked,
    allowBookmark,
    onShareClick,
    onBookmarkClick,
  } = props;

  return (
    <Anchor
      href={`/post/${title}`}
      className={styles.main}>
      {
        exclusive && (
          <span className={styles.labelExclusive}>Exclusive</span>
        )
      }
      <div className={styles.containerImage}>
        <AppImage
          src={coverImageSrc}
          alt={title}
          sizes={getImageSizes(1)}
          layout="fill"
          objectFit="cover"
          priority={index <= 5} />
      </div>
      <div className={styles.containerTitleFooter}>
        <h3 className={styles.title}>{title}</h3>
        <PostFooter
          updateDate={updateDate}
          readTime={readTime}
          bookmarked={bookmarked}
          allowBookmark={allowBookmark}
          onShareClick={e => {
            e.preventDefault();

            onShareClick(e);
          }}
          onBookmarkClick={e => {
            e.preventDefault();

            onBookmarkClick(e);
          }} />
      </div>
    </Anchor>
  );
};

