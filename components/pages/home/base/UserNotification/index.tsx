import {AppImage} from '@/components/base';
import {getImageSizes} from '@/libs/image';
import type {Notification} from '@/types/resources';
import type {VFC} from 'react';
import {UserNotificationContent} from '../UserNotificationContent';
import styles from './styles.module.scss';

export type UserNotificationProps = {
  coverImagePath: Notification['coverImagePath'];
  coverImageWidth: Notification['coverImageWidth'];
  coverImageHeight: Notification['coverImageHeight'];
  title: Notification['title'];
  content: Notification['content'];
  createDate: Notification['createDate'];
};

export const UserNotification: VFC<UserNotificationProps>= props => {
  const {
    coverImagePath,
    coverImageWidth,
    coverImageHeight,
    title,
    content,
    createDate,
  } = props;

  return (
    <article>
      <header>
        <AppImage
          layout="responsive"
          alt="Cover Image"
          width={coverImageWidth}
          height={coverImageHeight}
          sizes={getImageSizes(1)}
          src={coverImagePath} />
        <div className={styles.containerTitle}>
          <span className={styles.textTitle}>{title}</span>
          <time className={styles.textCreateDate}>{createDate}</time>
        </div>
      </header>
      <section className={styles.body}>
        <UserNotificationContent
          content={content} />
      </section>
    </article>
  );
};
