import {
  Anchor,
  IconContainer,
  LoadingIcon,
  RedDotContainer,
} from '@/components/base';
import type {User} from '@/types/resources';
import classnames from 'classnames';
import {Login, Notification} from 'grommet-icons';
import Image from 'next/image';
import Link from 'next/link';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';

export type HeaderProps = HTMLAttributes<HTMLElement> & {
  user?: User;
  userLoading: boolean;
  hasUnreadNotification: boolean;
  notificationActive: boolean;
  onNotificationActiveChange: (active: boolean) => void;
};

export const Header: VFC<HeaderProps> = props => {
  const {
    className,
    user,
    userLoading,
    hasUnreadNotification,
    notificationActive,
    onNotificationActiveChange,
  } = props;

  const RightTopIcon: VFC = () => {
    if (userLoading) {
      return (
        <aside className={styles.containerHeaderIcon}>
          <LoadingIcon radius={10} />
        </aside>
      );
    }

    return (
      <aside className={styles.containerHeaderIcon}>
        {
          !user
            ? (
              <Link
                href="/auth/sign-in"
                passHref>
                <a>
                  <IconContainer size="medium">
                    <Login color="plain" />
                  </IconContainer>
                </a>
              </Link>
            )
            : (
              <RedDotContainer isActive={hasUnreadNotification}>
                <IconContainer
                  size="medium"
                  isStatic={false}
                  isActive={notificationActive}
                  onClick={() => onNotificationActiveChange(!notificationActive)}>
                  <Notification color="plain" />
                </IconContainer>
              </RedDotContainer>
            )
        }
      </aside>
    );
  };

  return (
    <header className={classnames(className, styles.main)}>
      <div className={styles.containerImage}>
        <Link
          href="/"
          passHref>
          <Anchor>
            <Image
              src="/images/logo.svg"
              alt="Home"
              layout="fixed"
              width="120px"
              height="25px"
              priority />
          </Anchor>
        </Link>
      </div>
      <RightTopIcon />
    </header>
  );
};
