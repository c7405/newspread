import type {FC, HTMLAttributes} from 'react';
import classnames from 'classnames';
import styles from './styles.module.scss';

export type FooterIconContainerProps = HTMLAttributes<HTMLDivElement> & {
  isActive: boolean;
};

export const FooterIconContainer: FC<FooterIconContainerProps> = ({
  className,
  isActive,
  children,
  ...otherProps
}) => (
  <div
    {...otherProps}
    className={classnames(className, styles.main, {[styles.active]: isActive})}>
    {children}
  </div>
);
