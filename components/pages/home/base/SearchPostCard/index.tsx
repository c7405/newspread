import {Anchor, AppImage} from '@/components/base';
import {PostFooter} from '@/components/pages/home/base';
import type {PostFooterProps} from '@/components/pages/home/base/PostFooter';
import {getImageSizes} from '@/libs/image';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type SearchPostCardProps = PostFooterProps & {
  postId: number;
  coverImageSrc: string;
  title: string;
  exclusive: boolean;
};

export const SearchPostCard: VFC<SearchPostCardProps> = props => {
  const {
    coverImageSrc,
    title,
    exclusive,
    updateDate,
    readTime,
    allowBookmark,
    bookmarked,
    onShareClick,
    onBookmarkClick,
  } = props;

  return (
    <Anchor
      href={`/post/${title}`}
      className={styles.main}>
      {exclusive && <span className={styles.labelExclusive}>Exclusive</span>}
      <section className={styles.content}>
        <div className={styles.containerImage}>
          <AppImage
            src={coverImageSrc}
            alt={title}
            sizes={getImageSizes(0.25)}
            layout="fill"
            objectFit="cover" />
        </div>
        <header className={styles.containerTitle}>
          <h3 className={styles.title}>{title}</h3>
        </header>
      </section>
      <div className={styles.containerTitleFooter}>
        <PostFooter
          updateDate={updateDate}
          readTime={readTime}
          allowBookmark={allowBookmark}
          bookmarked={bookmarked}
          onShareClick={e => {
            e.preventDefault();

            onShareClick(e);
          }}
          onBookmarkClick={e => {
            e.preventDefault();

            onBookmarkClick(e);
          }} />
      </div>
    </Anchor>
  );
};

