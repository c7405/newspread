import {VFC} from 'react';
import {PostMeta, PostToolset} from '@/components/pages/home/base';
import type {PostMetaProps} from '@/components/pages/home/base/PostMeta';
import type {PostToolsetProps} from '@/components/pages/home/base/PostToolset';
import styles from './styles.module.scss';

export type PostFooterProps = PostMetaProps & PostToolsetProps;

export const PostFooter: VFC<PostFooterProps> = ({
  updateDate,
  readTime,
  bookmarked,
  allowBookmark,
  onShareClick,
  onBookmarkClick,
}) => (
  <footer className={styles.main}>
    <PostMeta
      updateDate={updateDate}
      readTime={readTime} />
    <PostToolset
      bookmarked={bookmarked}
      allowBookmark={allowBookmark}
      onShareClick={onShareClick}
      onBookmarkClick={onBookmarkClick} />
  </footer>
);
