import {
  AppImage,
  Button,
  Input,
  ValidationMessage,
} from '@/components/base';
import {useValidation} from '@/hooks/useValidation';
import type {User, UserDetail} from '@/types/resources';
import {updateUserProfileSchema} from '@/validators/authentication';
import type {MouseEventHandler, VFC} from 'react';
import {useState} from 'react';
import styles from './styles.module.scss';

export type UserProfileUpdateData = Pick<UserDetail, 'avatarImagePath' | 'firstName' | 'lastName'>;

export type UserProfileEditorProps = UserDetail & {
  account: User['account'];
  onUserProfileUpdate: (
    id: UserDetail['id'],
    newUserProfile: UserProfileUpdateData,
  ) => Promise<void>;
};

type OnUserProfileDataChange = (updateData: Partial<UserProfileUpdateData>) => void;

export const UserProfileEditor: VFC<UserProfileEditorProps> = ({
  id,
  account,
  avatarImagePath: rawAvatarImagePath,
  firstName: rawFirstName,
  lastName: rawLastName,
  onUserProfileUpdate,
}) => {
  const [
    isUserProfileDataChanged,
    userProfileData,
    setUserProfileData,
    valildationError,
    errorDetailMap,
  ] = useValidation(
    {
      avatarImagePath: rawAvatarImagePath,
      firstName: rawFirstName,
      lastName: rawLastName,
    },
    updateUserProfileSchema,
  );

  const [isUserProfileUpdating, setIsUserProfileUpdating] = useState<boolean>(false);

  const onUserProfileDataChange: OnUserProfileDataChange = updateData => {
    setUserProfileData(updateData);
  };

  const onUpdateBtnClick: MouseEventHandler<HTMLButtonElement> = async () => {
    setIsUserProfileUpdating(true);

    try {
      await onUserProfileUpdate(id, userProfileData);
    } finally {
      setIsUserProfileUpdating(false);
    }
  };

  return (
    <div className={styles.main}>
      <div className={styles.containerAvatar}>
        <div className={styles.containerAvatarImage}>
          <AppImage
            src={userProfileData.avatarImagePath}
            alt="Avatar"
            sizes="14rem"
            width="400px"
            height="400px" />
        </div>
      </div>
      <div className={styles.containerInputField}>
        <div>
          <header className={styles.textInputFieldTitle}>Account</header>
          <span className={styles.textAccount}>{account}</span>
        </div>
        <div>
          <Input
            fieldName="First Name"
            value={userProfileData.firstName}
            hasError={!!errorDetailMap.firstName}
            onChange={({target: {value: firstName}}) => onUserProfileDataChange({firstName})} />
          <ValidationMessage message={errorDetailMap.firstName} />
        </div>
        <div>
          <Input
            fieldName="Last Name"
            value={userProfileData.lastName}
            hasError={!!errorDetailMap.lastName}
            onChange={({target: {value: lastName}}) => onUserProfileDataChange({lastName})} />
          <ValidationMessage message={errorDetailMap.lastName} />
        </div>
      </div>
      <footer className={styles.containerSubmitButton}>
        <Button
          variant="primary"
          loading={isUserProfileUpdating}
          disabled={!isUserProfileDataChanged || !!valildationError}
          onClick={onUpdateBtnClick}>
          Update
        </Button>
      </footer>
    </div>
  );
};
