import {AppImage, IconContainer} from '@/components/base';
import {VoidEventHandler} from '@/types/components';
import {Edit} from 'grommet-icons';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type UserProfileBasicProps = {
  account: string;
  avatarImagePath: string;
  firstName: string;
  lastName: string;
  onProfileEdit: VoidEventHandler;
};

export const UserProfileBasic: VFC<UserProfileBasicProps> = ({
  account,
  avatarImagePath,
  firstName,
  lastName,
  onProfileEdit,
}) => (
  <div className={styles.main}>
    <div className={styles.containerAvatar}>
      <AppImage
        src={avatarImagePath}
        alt={account}
        sizes="3.5rem"
        layout="responsive"
        width="400px"
        height="400px" />
    </div>
    <div className={styles.containerAccountName}>
      <span className={styles.textName}>{`${firstName} ${lastName}`}</span>
      <span className={styles.textAccount}>{account}</span>
    </div>
    <IconContainer isStatic={false}>
      <Edit
        color="plain"
        onClick={onProfileEdit} />
    </IconContainer>
  </div>
);
