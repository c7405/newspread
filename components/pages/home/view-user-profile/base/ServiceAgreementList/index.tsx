import type {VFC} from 'react';
import {ForwardItemList, ForwardItem} from '@/components/base';
import {ForwardItemProps} from '@/components/base/ForwardItem';
import styles from './styles.module.scss';

export type ServiceAgreementListProps = {
  onPrivacyPolicyClick: ForwardItemProps['onClick'];
  onTermsOfServiceClick: ForwardItemProps['onClick'];
};

export const ServiceAgreementList: VFC<ServiceAgreementListProps> = ({
  onPrivacyPolicyClick,
  onTermsOfServiceClick,
}) => (
  <div className={styles.main}>
    <ForwardItemList>
      <ForwardItem onClick={onPrivacyPolicyClick}>
        Privacy Policy
      </ForwardItem>
      <ForwardItem onClick={onTermsOfServiceClick}>
        Terms of Service
      </ForwardItem>
    </ForwardItemList>
  </div>
);
