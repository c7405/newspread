import {EmptyContentIndicator, FullContentLoadingIndicator, YearSwitch} from '@/components/base';
import type {YearSwitchProps} from '@/components/base/YearSwitch';
import {BillingHistoryTable} from '@/components/pages/home/view-user-profile/base';
import type {BillingHistoryTableProps} from '@/components/pages/home/view-user-profile/base/BillingHistoryTable';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type BillingHistoryViewerProps =
  Pick<YearSwitchProps, 'minYear' | 'year'> &
  Partial<BillingHistoryTableProps> &
  {
    onYearSwitch: YearSwitchProps['onSwitch'];
  };

export const BillingHistoryViewer: VFC<BillingHistoryViewerProps> = ({
  minYear,
  year,
  billingHistory,
  subscriptionPlanMap,
  onYearSwitch,
}) => {
  const BodyContent: VFC = () => {
    if (!billingHistory || !subscriptionPlanMap) {
      return <FullContentLoadingIndicator />;
    }

    return (
      !billingHistory.length
        ? (
          <EmptyContentIndicator message="No Billing History" />
        )
        : (
          <BillingHistoryTable
            billingHistory={billingHistory}
            subscriptionPlanMap={subscriptionPlanMap}
            className={styles.tableBillingHistory} />
        )
    );
  };

  return (
    <div className={styles.main}>
      <header className={styles.header}>
        <YearSwitch
          minYear={minYear}
          year={year}
          onSwitch={onYearSwitch} />
      </header>
      <section className={styles.body}>
        <BodyContent />
      </section>
    </div>
  );
};
