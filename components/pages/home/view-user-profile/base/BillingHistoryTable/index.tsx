import {formatDate} from '@/formatters/date';
import type {BillingHistory, SubscriptionPlanMap} from '@/types/resources';
import classnames from 'classnames';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';
export type BillingHistoryTableProps = HTMLAttributes<HTMLTableElement> & {
  billingHistory: BillingHistory[];
  subscriptionPlanMap: SubscriptionPlanMap;
};

export const BillingHistoryTable: VFC<BillingHistoryTableProps> = ({
  className,
  billingHistory,
  subscriptionPlanMap,
}) => (
  <table className={
    classnames(
      styles.main,
      className,
    )
  }>
    <thead>
      <tr className={styles.headerRow}>
        <th>Plan</th>
        <th>Price</th>
        <th>Status</th>
        <th>Renewal Date</th>
        <th>Invoice Date</th>
      </tr>
    </thead>
    <tbody className={styles.body}>
      {
        billingHistory.map(({subscriptionPlan, paymentStatus, renewalDate, invoiceDate}, idx) => {
          const {description, price} = subscriptionPlanMap[subscriptionPlan];

          return (
            <tr
              key={idx}
              className={styles.bodyRow}>
              <td>{description}</td>
              <td>${price}</td>
              <td>{paymentStatus ? 'Success' : 'Failed'}</td>
              <td>{formatDate(renewalDate)}</td>
              <td>{formatDate(invoiceDate)}</td>
            </tr>
          );
        })
      }
    </tbody>
  </table>
);
