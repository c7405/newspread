import type {MouseEventHandler, VFC} from 'react';
import type {SubscriptionPlanType, SubscriptionPlanMap} from '@/types/resources';
import {ForwardItemList, ForwardItem} from '@/components/base';
import {formatDate} from '@/formatters/date';
import type {VoidEventHandler} from '@/types/components';
import styles from './styles.module.scss';

export type SubscriptionPlanInfoProps = {
  subscriptionPlan?: SubscriptionPlanType | null,
  subscriptionPlanMap: SubscriptionPlanMap,
  paymentStatus?: boolean | null,
  renewalDate?: string | null;
  invoiceDate?: string | null;
  onUnsubscribe: VoidEventHandler;
  onCustomizePlanClick: MouseEventHandler<HTMLElement>;
  onBillingHistoryClick: MouseEventHandler<HTMLElement>;
};

export const SubscriptionPlanInfo: VFC<SubscriptionPlanInfoProps> = ({
  subscriptionPlan,
  subscriptionPlanMap,
  paymentStatus,
  renewalDate,
  invoiceDate,
  onCustomizePlanClick,
  onBillingHistoryClick,
  onUnsubscribe,
}) => {
  const subscriptionTypeInfo = subscriptionPlan && subscriptionPlanMap[subscriptionPlan];

  return (
    <div className={styles.main}>
      <div className={styles.containerSubscription}>
        <span className={styles.textSubscriptionTitle}>Subscription</span>
        {
          subscriptionTypeInfo
            ? (
              <div className={styles.containerSubscriptionTextOperation}>
                <span className={styles.textSubscriptionPlan}>{subscriptionTypeInfo.description}</span>
                <span
                  className={styles.textUnsubscript}
                  onClick={() => onUnsubscribe()}>
                    Unsubscribe
                </span>
              </div>
            )
            : (
              <span className={styles.textSubscriptionPlan}>None</span>
            )
        }
      </div>
      {
        subscriptionTypeInfo &&
        <div className={styles.containerSubscriptionDetail}>
          <div className={styles.rowSubscriptionDetail}>
            <div className={styles.itemSubscriptionDetail}>
              <header className={styles.titleSubscriptionDetail}>Status</header>
              <span className={styles.textSubscriptionDetail}>{paymentStatus ? 'Success' : 'Failed'}</span>
            </div>
            <div className={styles.itemSubscriptionDetail}>
              <header className={styles.titleSubscriptionDetail}>Billing Price</header>
              <span className={styles.textSubscriptionDetail}>{`$${subscriptionTypeInfo.price}`}</span>
            </div>
          </div>
          <div className={styles.rowSubscriptionDetail}>
            <div className={styles.itemSubscriptionDetail}>
              <header className={styles.titleSubscriptionDetail}>Renewal Date</header>
              <span className={styles.textSubscriptionDetail}>{formatDate(renewalDate)}</span>
            </div>
            <div className={styles.itemSubscriptionDetail}>
              <header className={styles.titleSubscriptionDetail}>Invoice Date</header>
              <span className={styles.textSubscriptionDetail}>{formatDate(invoiceDate)}</span>
            </div>
          </div>
        </div>
      }
      {
        subscriptionTypeInfo
          ? (
            <ForwardItemList>
              <ForwardItem onClick={onCustomizePlanClick}>
                Customize Plan
              </ForwardItem>
              <ForwardItem onClick={onBillingHistoryClick}>
                Billing History
              </ForwardItem>
            </ForwardItemList>
          )
          : (
            <ForwardItemList>
              <ForwardItem onClick={onCustomizePlanClick}>
                Customize Plan
              </ForwardItem>
            </ForwardItemList>
          )
      }
    </div>
  );
};
