import {Button, IconTextHeader} from '@/components/base';
import {AsyncVoidEventHandler, VoidEventHandler} from '@/types/components';
import {Help} from 'grommet-icons';
import type {VFC} from 'react';
import {useState} from 'react';
import styles from './styles.module.scss';

export type UnsubscriptionPlanEditorProps = {
  onContinue: AsyncVoidEventHandler;
  onCancel: VoidEventHandler;
};

export const UnsubscriptionPlanEditor: VFC<UnsubscriptionPlanEditorProps> = props => {
  const {onContinue, onCancel} = props;
  const [isUnsubscribePlanProcessing, setIsUnsubscribePlanProcessing] = useState<boolean>(false);

  const onUnsubscribeUserSubscriptionBtnClick: UnsubscriptionPlanEditorProps['onContinue'] = async () => {
    setIsUnsubscribePlanProcessing(true);

    try {
      await onContinue();
    } finally {
      setIsUnsubscribePlanProcessing(false);
    }
  };

  return (
    <div className={styles.main}>
      <IconTextHeader
        icon={Help}
        title="Unsubscribe Plan"
        subTitle="You are going to unsubscribe our service now, are you sure to continue?" />
      <footer className={styles.footer}>
        <Button
          variant="danger"
          loading={isUnsubscribePlanProcessing}
          onClick={onUnsubscribeUserSubscriptionBtnClick}>
            Continue
        </Button>
        <Button
          variant="default"
          disabled={isUnsubscribePlanProcessing}
          onClick={onCancel}>
            No, let me think twice
        </Button>
      </footer>
    </div>
  );
};
