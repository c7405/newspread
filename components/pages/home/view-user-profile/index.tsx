import {deleteToken, updateUserDetail} from '@/apis/client/authentication';
import {createCheckoutCreditCardEntrypoint} from '@/apis/client/checkout';
import {deleteUserSubscription} from '@/apis/client/subscription/delete-user-subscription';
import {
  Button,
  FullContentLoadingIndicator,
  PopupPageModal,
  Portal,
  SignInNotification,
} from '@/components/base';
import {
  BillingHistoryViewer,
  ServiceAgreementList,
  SubscriptionPlanInfo,
  UnsubscriptionPlanEditor,
  UserProfileBasic,
  UserProfileEditor,
} from '@/components/pages/home/view-user-profile/base';
import type {BillingHistoryViewerProps} from '@/components/pages/home/view-user-profile/base/BillingHistoryViewer';
import type {SubscriptionPlanInfoProps} from '@/components/pages/home/view-user-profile/base/SubscriptionPlanInfo';
import type {UnsubscriptionPlanEditorProps} from '@/components/pages/home/view-user-profile/base/UnsubscriptionPlanEditor';
import {PrivacyPolicyArticle} from '@/components/pages/privacy-policy/base';
import {SubscriptionPlanEditor} from '@/components/pages/subscribe/base';
import type {SubscriptionPlanEditorProps} from '@/components/pages/subscribe/base/SubscriptionPlanEditor';
import {TermsOfServiceArticle} from '@/components/pages/terms-of-service/base';
import {useUser} from '@/hooks/app';
import {
  useCheckoutUser,
  useSubscriptionPlans,
  useUserBillingHistory,
} from '@/hooks/page';
import type {AsyncVoidEventHandler, VoidEventHandler} from '@/types/components';
import {submitCheckoutForm} from '@/utils/client/checkout-form';
import router from 'next/router';
import type {VFC} from 'react';
import {useState} from 'react';
import type {PopupPageModalProps} from '@/components/base/PopupPageModal';
import type {UserProfileEditorProps} from './base/UserProfileEditor';
import styles from './styles.module.scss';

export const ViewUserProfile: VFC = () => {
  const [isUserProfileEditorModalOpen, setIsUserProfileEditorModalOpen] = useState<boolean>(false);
  const [isSubscriptionPlanEditorModalOpen, setIsSubscriptionPlanEditorModalOpen] = useState<boolean>(false);
  const [isUnsubscriptionPlanEditorModalOpen, setIsUnsubscriptionPlanEditorModalOpen] = useState<boolean>(false);
  const [isBillingHistoryModalOpen, setIsBillingHistoryModalOpen] = useState<boolean>(false);
  const [isPrivacyPolicyModalOpen, setIsPrivacyPolicyModalOpen] = useState<boolean>(false);
  const [isTermsOfServiceModalOpen, setIsTermsOfServiceModalOpen] = useState<boolean>(false);

  const {
    user,
    isLoading: isUserLoading,
    onUserDetailUpdate,
    onReset: onUserReset,
  } = useUser();

  const {
    subscriptionPlanMap,
    isLoading: isSubscriptionPlansLoading,
  } = useSubscriptionPlans();

  const {
    userSubscription,
    ecpayTransaction,
    isLoading: isCheckoutUserLoading,
    onCheckoutUserRefresh,
  } = useCheckoutUser({user});

  const {
    billingHistory,
    billingHistoryYear,
    setBillingHistoryYear,
  } = useUserBillingHistory({
    year: user ? (new Date()).getFullYear() : undefined,
  });

  const subscriptionPlan = userSubscription?.subscriptionPlanType ?? null;

  const onUserProfileModalOpenChange: PopupPageModalProps['onOpenChange'] = open => {
    setIsUserProfileEditorModalOpen(open);
  };
  const onUserProfileUpdate: UserProfileEditorProps['onUserProfileUpdate'] = async (id, updateData) => {
    const {userDetail} = await updateUserDetail(id, updateData);

    onUserDetailUpdate(userDetail);
    onUserProfileModalOpenChange(false);
  };

  if (!user) return (<SignInNotification message="Please sign in to view your personal profile" />);

  const isDataLoaded = (
    !isUserLoading &&
    !isSubscriptionPlansLoading &&
    !isCheckoutUserLoading
  );

  const onCustomizePlanClick: VoidEventHandler = () => {
    setIsSubscriptionPlanEditorModalOpen(true);
  };

  const onBillingHistoryYearSwitch: BillingHistoryViewerProps['onYearSwitch'] = newBillingHistoryYear => {
    setBillingHistoryYear(newBillingHistoryYear);
  };

  const onBillingHistoryClick: SubscriptionPlanInfoProps['onBillingHistoryClick'] = () => {
    setIsBillingHistoryModalOpen(true);
  };

  const onPrivacyPolicyClick: VoidEventHandler = () => {
    setIsPrivacyPolicyModalOpen(true);
  };
  const onTermsOfServiceClick: VoidEventHandler = () => {
    setIsTermsOfServiceModalOpen(true);
  };

  const onSubscriptionPlanCheckout: SubscriptionPlanEditorProps['onCheckout'] = async ({subscriptionPlan: subscriptionType}) => {
    const {checkoutEntrypoint, checkoutCreditCardParams} = await createCheckoutCreditCardEntrypoint({
      subscriptionType,
    });

    submitCheckoutForm(checkoutEntrypoint, checkoutCreditCardParams);
  };

  const onUserSubscriptionDelete: UnsubscriptionPlanEditorProps['onContinue'] = async () => {
    const userSubscriptionId = userSubscription?.id;

    if (!userSubscriptionId) return;

    await deleteUserSubscription({
      id: userSubscriptionId,
    });

    await onCheckoutUserRefresh();

    setIsUnsubscriptionPlanEditorModalOpen(false);
  };
  const onUnsubscriptionPlanEditorCancel: UnsubscriptionPlanEditorProps['onCancel'] = () => {
    setIsUnsubscriptionPlanEditorModalOpen(false);
  };

  const onUserLogout: AsyncVoidEventHandler = async () => {
    try {
      await deleteToken();

      onUserReset();
    } catch (e) {
      console.error(e);
    } finally {
      router.replace('/auth/sign-in');
    }
  };

  return isDataLoaded
    ? (
      <>
        <Portal portalId="sub-page-user-profile">
          <PopupPageModal
            open={isUserProfileEditorModalOpen}
            onOpenChange={onUserProfileModalOpenChange}>
            <div className={styles.containerModalContentEditor}>
              <UserProfileEditor
                account={user.account}
                id={user.userDetail.id}
                avatarImagePath={user.userDetail.avatarImagePath}
                firstName={user.userDetail.firstName}
                lastName={user.userDetail.lastName}
                onUserProfileUpdate={onUserProfileUpdate} />
            </div>
          </PopupPageModal>
          <PopupPageModal
            open={isSubscriptionPlanEditorModalOpen}
            onOpenChange={open => setIsSubscriptionPlanEditorModalOpen(open)}>
            {
              subscriptionPlanMap &&
              <div className={styles.containerModalContentEditor}>
                <SubscriptionPlanEditor
                  headerTitle="Customize your plan"
                  subscriptionPlan={subscriptionPlan}
                  subscriptionPlanMap={subscriptionPlanMap}
                  onCheckout={onSubscriptionPlanCheckout} />
              </div>
            }
          </PopupPageModal>
          <PopupPageModal
            open={isUnsubscriptionPlanEditorModalOpen}
            onOpenChange={open => setIsUnsubscriptionPlanEditorModalOpen(open)}>
            <div className={styles.containerModalContentEditor}>
              <UnsubscriptionPlanEditor
                onContinue={onUserSubscriptionDelete}
                onCancel={onUnsubscriptionPlanEditorCancel} />
            </div>
          </PopupPageModal>
          <PopupPageModal
            open={isBillingHistoryModalOpen}
            onOpenChange={open => setIsBillingHistoryModalOpen(open)}>
            <div className={styles.containerModalBillingHistory}>
              {
                billingHistoryYear && (
                  <BillingHistoryViewer
                    minYear={2022}
                    year={billingHistoryYear}
                    billingHistory={billingHistory}
                    subscriptionPlanMap={subscriptionPlanMap}
                    onYearSwitch={onBillingHistoryYearSwitch} />
                )
              }
            </div>
          </PopupPageModal>
          <PopupPageModal
            open={isPrivacyPolicyModalOpen}
            onOpenChange={open => setIsPrivacyPolicyModalOpen(open)}>
            <div className={styles.containerModalAgreement}>
              <PrivacyPolicyArticle />
            </div>
          </PopupPageModal>
          <PopupPageModal
            open={isTermsOfServiceModalOpen}
            onOpenChange={open => setIsTermsOfServiceModalOpen(open)}>
            <div className={styles.containerModalAgreement}>
              <TermsOfServiceArticle />
            </div>
          </PopupPageModal>
        </Portal>
        <div className={styles.main}>
          <section className={styles.body}>
            <UserProfileBasic
              account={user.account}
              firstName={user.userDetail.firstName}
              lastName={user.userDetail.lastName}
              avatarImagePath={user.userDetail.avatarImagePath}
              onProfileEdit={() => setIsUserProfileEditorModalOpen(true)} />
            {
              subscriptionPlanMap && (
                <SubscriptionPlanInfo
                  subscriptionPlan={subscriptionPlan}
                  subscriptionPlanMap={subscriptionPlanMap}
                  paymentStatus={ecpayTransaction?.rtnCode === 1}
                  renewalDate={ecpayTransaction?.createTime}
                  invoiceDate={ecpayTransaction?.paymentDate}
                  onUnsubscribe={() => setIsUnsubscriptionPlanEditorModalOpen(true)}
                  onCustomizePlanClick={onCustomizePlanClick}
                  onBillingHistoryClick={onBillingHistoryClick} />
              )
            }
            <ServiceAgreementList
              onPrivacyPolicyClick={onPrivacyPolicyClick}
              onTermsOfServiceClick={onTermsOfServiceClick} />
          </section>
          <footer className={styles.footer}>
            <Button
              className={styles.buttonLogout}
              onClick={onUserLogout}>
                Logout
            </Button>
          </footer>
        </div>
      </>
    )
    : (
      <FullContentLoadingIndicator />
    );
};
