import {
  ClickActiveHighlight,
  LoadingIcon,
  Portal,
  ScrollEndDetector,
  UndoToast,
} from '@/components/base';
import type {ScrollEndDetectorProps} from '@/components/base/ScrollEndDetector';
import {PostCard} from '@/components/pages/home/base';
import {useUser} from '@/hooks/app';
import {
  usePostInfiniteScroll,
  usePostsBookmarkProcess,
  usePostShare,
} from '@/hooks/page';
import type {AsyncVoidEventHandler, VoidEventHandler} from '@/types/components';
import type {Bookmark, Post} from '@/types/resources';
import type {UIEvent, VFC} from 'react';
import styles from './styles.module.scss';

export type ViewPostProps = {
  bookmarks: Bookmark[];
  posts: Post[];
  cursor?: number;
};

export type OnBeforeScrollEnd = (e: UIEvent<HTMLElement>) => void;

export type OnBookmarkClick = (postId: Post['id'], postBookmarkId?: Bookmark['id']) => Promise<void>;

export const ViewPost: VFC<ViewPostProps> = ({posts: initPosts, cursor: initCursor, bookmarks: initBookmarks}) => {
  const {
    isLoading,
    isValidating,
    posts,
    postBookmarkIdMap,
    isPageEndReached,
    onReload: onViewPostsReload,
    onNextPageLoad: onViewPostsNextPageLoad,
  } = usePostInfiniteScroll({
    headFallbackData: {
      bookmarks: initBookmarks,
      posts: initPosts,
      cursor: initCursor,
    },
  });

  const {user} = useUser();

  const {
    isUndoToastOpen: isPostBookmarkUndoToastOpen,
    undoMessage: postBookmarkUndoToastUndoMessage,
    undoText: postBookmarkUndoToastUndoText,
    lastPostId,
    setIsUndoToastOpen,
    onProcess: onPostBookmarkProcess,
  } = usePostsBookmarkProcess();

  const {onPostShare} = usePostShare();

  const onAbountScrollEnd: ScrollEndDetectorProps['onAboutScrollEnd'] = () => {
    if (isLoading || isValidating || isPageEndReached()) return;

    onViewPostsNextPageLoad();
  };

  const onBookmarkClick: OnBookmarkClick = async (postId, postBookmarkId) => {
    if (!user) return;

    await onPostBookmarkProcess(postId, postBookmarkId, onViewPostsReload);
  };

  const onPostBookmarkUndoToastUndo: AsyncVoidEventHandler = async () => {
    if (!user || !lastPostId) return;

    await onPostBookmarkProcess(lastPostId, undefined, onViewPostsReload);
  };

  const onPostBookmarkUndoToastClose: VoidEventHandler = () => {
    setIsUndoToastOpen(false);
  };

  return (
    <>
      <Portal portalId="view-post-post-bookmark-undo-toast">
        <UndoToast
          open={isPostBookmarkUndoToastOpen}
          message={postBookmarkUndoToastUndoMessage}
          undoText={postBookmarkUndoToastUndoText}
          onUndo={onPostBookmarkUndoToastUndo}
          onClose={onPostBookmarkUndoToastClose} />
      </Portal>
      <ScrollEndDetector onAboutScrollEnd={onAbountScrollEnd}>
        <div className={styles.main}>
          <div className={styles.containerViewPostList}>
            {
              posts && posts.map((post, index) => {
                const {
                  id: postId,
                  coverImageSrc,
                  title,
                  exclusive,
                  updateDate,
                  readTime,
                } = post;
                const postBookmarkId = postBookmarkIdMap[postId];

                return (
                  <ClickActiveHighlight key={postId}>
                    <PostCard
                      index={index}
                      postId={postId}
                      coverImageSrc={coverImageSrc}
                      title={title}
                      exclusive={exclusive}
                      updateDate={updateDate}
                      readTime={readTime}
                      bookmarked={!!postBookmarkId}
                      allowBookmark={!!user}
                      onShareClick={() => onPostShare(title)}
                      onBookmarkClick={() => onBookmarkClick(postId, postBookmarkId)} />
                  </ClickActiveHighlight>
                );
              })
            }
          </div>
          {
            (isLoading || isValidating) && (
              <div className={styles.containerLoadingIcon}>
                <LoadingIcon />
              </div>
            )
          }
        </div>
      </ScrollEndDetector>
    </>
  );
};
