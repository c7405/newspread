import {
  ClickActiveHighlight,
  EmptyContentIndicator,
  LoadingIcon,
  Portal,
  ScrollEndDetector,
  SearchInput,
  SignInNotification,
  UndoToast,
} from '@/components/base';
import type {ScrollEndDetectorProps} from '@/components/base/ScrollEndDetector';
import {SearchInputProps} from '@/components/base/SearchInput';
import {UndoToastProps} from '@/components/base/UndoToast';
import {SearchPostCard} from '@/components/pages/home/base';
import {useUser} from '@/hooks/app';
import {
  usePostInfiniteScroll,
  usePostsBookmarkProcess,
  usePostShare,
} from '@/hooks/page';
import type {Bookmark, Post} from '@/types/resources';
import {Book} from 'grommet-icons';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';

export type SearchBookmarkProps = HTMLAttributes<HTMLElement>;

export type onPostBookmarkClick = (postId: Post['id'], postBookmarkId?: Bookmark['id']) => Promise<void>;

export const SearchBookmark: VFC<SearchBookmarkProps> = () => {
  const {
    isLoading: isSearchBookmarksLoading,
    isValidating: isSearchBookmarksValidating,
    posts,
    keyword: searchBookmarksKeyword,
    postBookmarkIdMap: searchBookmarksPostBookmarkIdMap,
    isPageEndReached: isSearchPostsPageEndReached,
    setKeyword: setSearchPostsKeyword,
    onReload: onSearchBookmarksPageReload,
    onNextPageLoad: onSearchBookmarksNextPageLoad,
  } = usePostInfiniteScroll({
    mode: 'USER_BOOKMARKED',
  });

  const {user} = useUser();

  const {onPostShare} = usePostShare();

  const {
    isUndoToastOpen: isSearchBookmarksUndoToastOpen,
    undoMessage: searchBookmarkUndoMessage,
    undoText: searchBookmarkUndoText,
    lastPostId,
    setIsUndoToastOpen: setIsSearchBookmarksUndoToastOpen,
    onProcess: onSearchBookmarksProcess,
  } = usePostsBookmarkProcess();

  if (!user) {
    return <SignInNotification message="Please Sign In to Bookmark Posts" />;
  }

  const onSearchBookmarksPostBookmarkClick: onPostBookmarkClick = async (postId, postBookmarkId) => {
    if (!user) return;

    await onSearchBookmarksProcess(postId, postBookmarkId, onSearchBookmarksPageReload);
  };

  const onSearchBookmarksPostBookmarkProcessUndo: UndoToastProps['onUndo'] = async () => {
    if (!user || !lastPostId) return;

    await onSearchBookmarksProcess(lastPostId, undefined, onSearchBookmarksPageReload);
  };

  const onAbountScrollEnd: ScrollEndDetectorProps['onAboutScrollEnd'] = () => {
    if (
      isSearchBookmarksLoading ||
      isSearchBookmarksValidating ||
      isSearchPostsPageEndReached()
    ) return;

    onSearchBookmarksNextPageLoad();
  };

  const onSearchBookmarkKeywordChange: SearchInputProps['onChange'] = ({target: {value}}) => {
    setSearchPostsKeyword(value);
  };

  const onSearchBookmarksUndoToastClose: UndoToastProps['onClose'] = () => {
    setIsSearchBookmarksUndoToastOpen(false);
  };

  const BodyContent: VFC = () => {
    if (!posts?.length) {
      if (!searchBookmarksKeyword) {
        return (
          <div className={styles.containerBgIconText}>
            <div className={styles.bgIconText}>
              <Book color="plain" />
              <span>Bookmark anything interests you here!</span>
            </div>
          </div>
        );
      }

      return (
        <EmptyContentIndicator message="Sorry, nothing found" />
      );
    }

    return (
      <div className={styles.containerSearchPostList}>
        {
          posts.map(post => {
            const {
              id: postId,
              coverImageSrc,
              title,
              exclusive,
              updateDate,
              readTime,
            } = post;
            const postBookmarkId = searchBookmarksPostBookmarkIdMap[postId];

            return (
              <ClickActiveHighlight key={postId}>
                <SearchPostCard
                  postId={postId}
                  coverImageSrc={coverImageSrc}
                  title={title}
                  exclusive={exclusive}
                  updateDate={updateDate}
                  readTime={readTime}
                  allowBookmark={!!user}
                  bookmarked={!!postBookmarkId}
                  onShareClick={() => onPostShare(title)}
                  onBookmarkClick={() => onSearchBookmarksPostBookmarkClick(postId, postBookmarkId)} />
              </ClickActiveHighlight>
            );
          })
        }
        {
          (isSearchBookmarksLoading || isSearchBookmarksValidating) && (
            <div className={styles.containerLoadingIcon}>
              <LoadingIcon />
            </div>
          )
        }
      </div>
    );
  };

  return (
    <>
      <Portal portalId="search-bookmarks-undo-toast">
        <UndoToast
          open={isSearchBookmarksUndoToastOpen}
          message={searchBookmarkUndoMessage}
          undoText={searchBookmarkUndoText}
          onUndo={onSearchBookmarksPostBookmarkProcessUndo}
          onClose={onSearchBookmarksUndoToastClose} />
      </Portal>
      <div className={styles.main}>
        <header className={styles.containerHeader}>
          <SearchInput
            lazy
            fieldName="Search"
            value={searchBookmarksKeyword}
            onChange={onSearchBookmarkKeywordChange} />
        </header>
        <ScrollEndDetector onAboutScrollEnd={onAbountScrollEnd}>
          <BodyContent />
        </ScrollEndDetector>
      </div>
    </>
  );
};
