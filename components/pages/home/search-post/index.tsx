import {
  ClickActiveHighlight,
  EmptyContentIndicator,
  LoadingIcon,
  Portal,
  ScrollEndDetector,
  SearchInput,
  UndoToast,
} from '@/components/base';
import type {ScrollEndDetectorProps} from '@/components/base/ScrollEndDetector';
import {SearchInputProps} from '@/components/base/SearchInput';
import {UndoToastProps} from '@/components/base/UndoToast';
import {SearchPostCard} from '@/components/pages/home/base';
import {useUser} from '@/hooks/app';
import {
  usePostsBookmarkProcess,
  usePostInfiniteScroll,
  usePostShare,
} from '@/hooks/page';
import type {Bookmark, Post} from '@/types/resources';
import {Overview} from 'grommet-icons';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';

export type SearchPostProps = HTMLAttributes<HTMLElement>;

export type OnPostBookmarkProcess = (postId: Post['id'], postBookmarkId?: Bookmark['id']) => Promise<void>;

export const SearchPost: VFC<SearchPostProps> = () => {
  const {
    isLoading,
    isValidating,
    keyword: searchPostsKeyword,
    posts,
    postBookmarkIdMap: searchPostsBookmarkIdMap,
    isPageEndReached: isSearchPostsPageEndReached,
    setKeyword: setSearchPostsKeyword,
    onReload: onSearchPostsReload,
    onNextPageLoad: onNextSearchPostPageLoad,
  } = usePostInfiniteScroll({
    isKeywordRequired: true,
  });

  const {user} = useUser();

  const {onPostShare} = usePostShare();

  const {
    isUndoToastOpen: isSearchPostBookmarkUndoToastOpen,
    undoMessage: searchPostBookmarkUndoMessage,
    undoText: searchPostBookmarkUndoText,
    lastPostId,
    setIsUndoToastOpen: setIsSearchPostBookmarkUndoToastOpen,
    onProcess: onSearchPostBookmarkProcess,
  } = usePostsBookmarkProcess();

  const onAbountScrollEnd: ScrollEndDetectorProps['onAboutScrollEnd'] = () => {
    if (isLoading || isValidating || isSearchPostsPageEndReached()) return;

    onNextSearchPostPageLoad();
  };

  const onSearchPostsBookmarkProcess: OnPostBookmarkProcess = async (postId, postBookmarkId) => {
    if (!user) return;

    await onSearchPostBookmarkProcess(postId, postBookmarkId, onSearchPostsReload);
  };

  const onSearchPostBookmarkUndo: UndoToastProps['onUndo'] = async () => {
    if (!user || !lastPostId) return;

    await onSearchPostBookmarkProcess(lastPostId, undefined, onSearchPostsReload);
  };

  const onSearchPostsUndoToastClose: UndoToastProps['onClose'] = () => {
    setIsSearchPostBookmarkUndoToastOpen(false);
  };

  const onSearchPostsKeywordChange: SearchInputProps['onChange'] = ({target: {value}}) => {
    setSearchPostsKeyword(value);
  };

  const BodyContent: VFC = () => {
    if (isLoading) {
      if (!searchPostsKeyword) {
        return (
          <div className={styles.containerBgIconText}>
            <div className={styles.bgIconText}>
              <Overview color="plain" />
              <span>Find something new</span>
            </div>
          </div>
        );
      }
    }

    if (!posts?.length) {
      return (
        <EmptyContentIndicator message="Sorry, nothing found" />
      );
    }

    return (
      <div className={styles.containerSearchPostList}>
        {
          posts.map(post => {
            const {
              id: postId,
              coverImageSrc,
              title,
              exclusive,
              updateDate,
              readTime,
            } = post;
            const postBookmarkId = searchPostsBookmarkIdMap[postId];

            return (
              <ClickActiveHighlight key={postId}>
                <SearchPostCard
                  postId={postId}
                  coverImageSrc={coverImageSrc}
                  title={title}
                  exclusive={exclusive}
                  updateDate={updateDate}
                  readTime={readTime}
                  allowBookmark={!!user}
                  bookmarked={!!postBookmarkId}
                  onShareClick={() => onPostShare(title)}
                  onBookmarkClick={() => onSearchPostsBookmarkProcess(postId, postBookmarkId)} />
              </ClickActiveHighlight>
            );
          })
        }
        {
          (isLoading || isValidating) && (
            <div className={styles.containerLoadingIcon}>
              <LoadingIcon />
            </div>
          )
        }
      </div>
    );
  };

  return (
    <>
      <Portal portalId="search-post-bookmark-undo-toast">
        <UndoToast
          open={isSearchPostBookmarkUndoToastOpen}
          message={searchPostBookmarkUndoMessage}
          undoText={searchPostBookmarkUndoText}
          onUndo={onSearchPostBookmarkUndo}
          onClose={onSearchPostsUndoToastClose} />
      </Portal>
      <div className={styles.main}>
        <header className={styles.containerHeader}>
          <SearchInput
            lazy
            fieldName="Search"
            value={searchPostsKeyword}
            onChange={onSearchPostsKeywordChange} />
        </header>
        <ScrollEndDetector onAboutScrollEnd={onAbountScrollEnd}>
          <BodyContent />
        </ScrollEndDetector>
      </div>
    </>
  );
};
