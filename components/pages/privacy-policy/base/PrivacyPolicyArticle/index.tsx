import type {VFC} from 'react';
import styles from './styles.module.scss';

export const PrivacyPolicyArticle: VFC = () => (
  <article>
    <header className={styles.header}>
      <h2 className={styles.textHeader}>Privacy Policy</h2>
    </header>
    <section className={styles.body}>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut tellus tristique, scelerisque
        erat in, congue elit. Nam dignissim velit metus, eget commodo sem tempus et. Vestibulum
        ac suscipit mi, a ornare urna. Mauris gravida rutrum neque ut dapibus. Nulla non quam viverra,
        mollis orci in, mattis risus. Etiam sed cursus nisi. Mauris at mattis tortor. Donec a nisl
        eget turpis tincidunt imperdiet non et ante.
      </p>
      <p>
        Proin efficitur, velit a lobortis consequat, sapien urna interdum neque, eget vehicula
        libero sem eget mauris. Curabitur ultricies sit amet ante ac viverra. Quisque ac consequat
        velit. Aliquam auctor ante non sagittis pretium. Pellentesque justo felis, maximus at rutrum
        eu, accumsan sit amet neque. Praesent finibus elit ex, faucibus volutpat lacus vestibulum
        ut. Mauris condimentum leo nec mattis semper. Pellentesque condimentum, arcu sit amet
        luctus fermentum, lacus sem ultrices lectus, eu ullamcorper massa magna ac quam. Nunc
        ultrices vitae sapien vitae pulvinar. 
      </p>
      <p>
        Aliquam nec arcu a felis feugiat varius. Phasellus tristique bibendum leo.
        Pellentesque varius felis vel nisl blandit accumsan. Sed non aliquet magna, eget efficitur
        lectus. Aenean sagittis vitae urna id consequat. Suspendisse vel turpis elit. Etiam
        rhoncus dignissim lorem, a cursus tortor aliquet eu.
      </p>
      <p>
        Nulla ac erat et ante luctus iaculis. Donec consectetur, ante eget eleifend fermentum,
        dolor nisl convallis ipsum, sed aliquet mi libero at nibh. Donec eu odio sed ipsum laoreet
        ullamcorper sed nec metus. Phasellus sapien erat, sollicitudin quis pharetra eget, dictum
        vitae erat. Vivamus tempor iaculis suscipit. Quisque et nisl quam. Donec viverra metus ac
        erat sagittis, et pellentesque odio iaculis.
      </p>
      <p>
        Quisque blandit ante felis, eu vestibulum nibh maximus ut. In hac habitasse platea
        dictumst. Duis sollicitudin libero eget diam tincidunt, vel aliquam orci vestibulum.
        Curabitur vel sollicitudin urna. Nunc tempus, felis lacinia lobortis elementum, turpis
        nibh bibendum metus, non laoreet sem odio id nisl. Vivamus pellentesque diam non odio
        vulputate, sed lobortis risus convallis. Aenean euismod cursus congue. Vestibulum
        hendrerit cursus risus vitae maximus. Orci varius natoque penatibus et magnis dis
        parturient montes, nascetur ridiculus mus. Pellentesque et blandit tellus. Mauris id
        lectus pharetra odio rhoncus cursus sed aliquam sapien. Mauris facilisis lorem quis orci
        interdum tempor. Aenean nec consequat arcu, et facilisis felis. Vestibulum ante ipsum
        primis in faucibus orci luctus et ultrices posuere cubilia curae; 
      </p>
    </section>
  </article>
);
