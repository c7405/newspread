import type {VFC, HTMLAttributes} from 'react';
import classnames from 'classnames';
import {PostContentTextItem} from '@/types/resources';
import styles from './styles.module.scss';

export type PostViewerTextItemProps =
  HTMLAttributes<HTMLSpanElement> & Pick<PostContentTextItem, 'value' | 'props'>;

export const PostViewerTextItem: VFC<PostViewerTextItemProps> = ({value, props, className}) => (
  <span
    style={props?.style}
    className={
      classnames(
        styles.main,
        className,
      )
    }>
    {value}
  </span>
);
