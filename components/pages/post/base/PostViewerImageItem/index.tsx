import {AppImage} from '@/components/base';
import {getImageSizes} from '@/libs/image';
import {PostContentImageItem} from '@/types/resources';
import classnames from 'classnames';
import type {HTMLAttributes, VFC} from 'react';
import styles from './styles.module.scss';

export type PostViewerImageItemProps =
  HTMLAttributes<HTMLDivElement> & Pick<PostContentImageItem, 'value' | 'props'>;

export const PostViewerImageItem: VFC<PostViewerImageItemProps> = ({
  className,
  value,
  props: {
    width,
    height,
    caption,
  },
}) => (
  <figure className={
    classnames(
      styles.main,
      className,
    )
  }>
    <div>
      <AppImage
        src={value}
        alt={caption}
        sizes={getImageSizes(1)}
        layout="responsive"
        objectFit="contain"
        width={width}
        height={height} />
    </div>
    <figcaption className={styles.textCaption}>
      {caption}
    </figcaption>
  </figure>
);
