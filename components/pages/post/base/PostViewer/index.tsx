import {AppImage} from '@/components/base';
import {PostToolset} from '@/components/pages/home/base';
import type {PostToolsetProps} from '@/components/pages/home/base/PostToolset';
import {PostViewerImageItem, PostViewerTextItem} from '@/components/pages/post/base';
import {formatDate} from '@/formatters/date';
import {formatAuthorsName} from '@/formatters/page/post';
import {getImageSizes} from '@/libs/image';
import type {PostWithContentAuthor} from '@/types/resources';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type PostViewerProps = Omit<PostWithContentAuthor, 'id'> & PostToolsetProps;

export const PostViewer: VFC<PostViewerProps> = ({
  coverImageWidth,
  coverImageHeight,
  coverImageSrc,
  title,
  exclusive,
  authors,
  readTime,
  updateDate,
  content,
  allowBookmark,
  bookmarked,
  onShareClick,
  onBookmarkClick,
}) => (
  <article className={styles.main}>
    <header className={styles.header}>
      <div className={styles.containerCover}>
        <AppImage
          src={coverImageSrc}
          alt={title}
          sizes={getImageSizes(1)}
          layout="responsive"
          objectFit="cover"
          width={coverImageWidth}
          height={coverImageHeight}
          priority />
      </div>
      <div className={styles.containerPostMeta}>
        {
          exclusive && (
            <div className={styles.labelExclusive}>Exclusive</div>
          )
        }
        <h2 className={styles.textTitle}>{title}</h2>
        <div className={styles.containerPostMetaToolset}>
          <div className={styles.containerPostMetaContent}>
            <div className={styles.textAuthorName}>{`By ${formatAuthorsName(authors)}`}</div>
            <div className={styles.containerPostPublishMeta}>
              <span>{`Pubslished ${formatDate(updateDate)}`}</span>
              <span>{' | '}</span>
              <span>{`${readTime} min read time`}</span>
            </div>
          </div>
          <div className={styles.containerPostToolset}>
            <PostToolset
              allowBookmark={allowBookmark}
              bookmarked={bookmarked}
              onShareClick={onShareClick}
              onBookmarkClick={onBookmarkClick} />
          </div>
        </div>
      </div>
    </header>
    <section className={styles.body}>
      {
        content.map(({type, value, props, position}) => {
          switch (type) {
            case 'TEXT':
              return (
                <PostViewerTextItem
                  key={position}
                  value={value}
                  props={props} />
              );
            case 'IMAGE':
              return (
                <PostViewerImageItem
                  key={position}
                  value={value}
                  props={props} />
              );
            default:
              break;
          }
        })
      }
    </section>
  </article>
);
