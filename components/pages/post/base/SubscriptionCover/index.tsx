import {Button, IconContainer} from '@/components/base';
import type {VoidEventHandler} from '@/types/components';
import {Catalog} from 'grommet-icons';
import type {VFC} from 'react';
import styles from './styles.module.scss';

export type SubscriptionCoverProps = {
  onSubscribe: VoidEventHandler;
};

export const SubscriptionCover: VFC<SubscriptionCoverProps> = ({onSubscribe}) => (
  <div className={styles.main}>
    <header className={styles.header}>
      <IconContainer
        size="x2large">
        <Catalog color="plain" />
      </IconContainer>
      <div className={styles.containerHeaderTitle}>
        <span className={styles.textTitle}>Subscriber exclusive post</span>
        <span className={styles.textSubTitle}>Subscribe now to continue reading</span>
      </div>
    </header>
    <section>
      <div className={styles.containerSubscribeBtn}>
        <Button
          variant="primary"
          onClick={onSubscribe}>
          Subscribe
        </Button>
      </div>
    </section>
  </div>
);
