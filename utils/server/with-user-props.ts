import {User} from '@/types/resources';
import {fetchUser} from '@/utils/server/refresh-token';
import type {GetServerSideProps, GetServerSidePropsContext, GetServerSidePropsResult, PreviewData} from 'next';
import type {ParsedUrlQuery} from 'querystring';

export type UserProps = {
  user: User | null,
};

export type WithUserProps = <P, Q extends ParsedUrlQuery, D extends PreviewData>(fn: (user: User | null) => GetServerSideProps<P, Q, D>) => (context: GetServerSidePropsContext<Q, D>) => Promise<GetServerSidePropsResult<P & UserProps>>;

export const withUserProps: WithUserProps = fn => async context => {
  const user = await fetchUser(
    context.req.cookies['refresh-token'],
  );
  const originalResult = await fn(user)(context);

  if (!('props' in originalResult))  {
    return originalResult;
  }

  const originalProps = await originalResult.props;

  return {
    props: {
      ...originalProps,
      user,
    },
  };
};

export type CreateGetServerSideProps = () => GetServerSideProps<UserProps>;

export const createGetServerSideProps: CreateGetServerSideProps = () => async ({req}) => {
  const user = await fetchUser(req.cookies['refresh-token']);

  return {
    props: {
      user,
    },
  };
};
