import {decode} from 'jsonwebtoken';
import {AppError} from '@cornerstone/app-error';
import * as selfModule from './refresh-token';
import type {User} from '@/types/resources';
import {findUserWithDetail} from '@/apis/server/authentication';
import {decryptValue} from '@/libs/cookie';

export type ParseUserId = (token: string) => number | never;

export const parseUserId: ParseUserId = token => {
  const userId = decode(token, {json: true})?.sub;

  if (!userId) throw new AppError(undefined, 'INVALID_USER_REFRESH_TOKEN');

  return +userId;
};

export type FetchUser = (refreshToken: string) => Promise<User | null>;

export const fetchUser: FetchUser = async refreshToken => {
  let user: User | null = null;

  if (refreshToken) {
    user = await findUserWithDetail({
      id: selfModule.parseUserId(
        decryptValue(refreshToken),
      ),
    });
  }

  return user;
};
