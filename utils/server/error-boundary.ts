import {NextApiHandler} from 'next';
import {RequestError} from 'got';
import {StatusCodes} from 'http-status-codes';
import {AppError} from '@cornerstone/app-error';
import {errorResponse} from '@cornerstone/response-formatter';

export type ErrorBoundary = (fn: NextApiHandler) => NextApiHandler;

export const errorBoundary: ErrorBoundary = fn => async (req, res) => {
  try {
    const result = fn(req, res);

    if (!(result instanceof Promise)) return;

    await result;
  } catch (e) {
    if (!(e instanceof Error)) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json(
          errorResponse('UNKNOWN_ERROR', ''),
        );

      return;
    }

    if (e instanceof RequestError && e.response) {
      const {statusCode, body} = e.response;

      if (typeof body === 'string') {
        try {
          const bodyContent = JSON.parse(body);

          res.status(statusCode).json(bodyContent);
        } catch (error) {
          console.error(error);

          res.status(statusCode).send('Server Error');
        }
      }

      return;
    }

    if (e instanceof AppError && e.httpStatusCode && e.errorStatusCode) {
      res.status(e.httpStatusCode)
        .json(
          errorResponse(e.errorStatusCode),
        );

      return;
    }

    res.status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json(
        errorResponse('UNKNOWN_ERROR', e.message),
      );
  }
};
