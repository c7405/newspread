import {encryptValue} from '@/libs/cookie';
import {AppError} from '@cornerstone/app-error';
import Cookies from 'cookies';
import {decode} from 'jsonwebtoken';
import type {NextApiRequest, NextApiResponse} from 'next';

export type SetRefreshTokenCookie = (req: NextApiRequest, res: NextApiResponse) => (refreshToken: string) => void;

export const setRefreshTokenCookie: SetRefreshTokenCookie = (req, res) => refreshToken => {
  const jwtPayload = decode(refreshToken, {json: true});

  if (!jwtPayload?.exp) throw new AppError('INVALID_USER_REFRESH_TOKEN');

  new Cookies(req, res).set(
    'refresh-token',
    encryptValue(refreshToken),
    {
      expires: new Date(jwtPayload.exp * 1000),
      sameSite: 'strict',
      secure: false,
    },
  );
};
