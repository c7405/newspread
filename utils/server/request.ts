import {AllowedHttpMethod} from '@/types/request';

export function isAllowedHttpMethod(method: string): method is AllowedHttpMethod {
  return ['HEAD', 'GET', 'POST', 'PATCH', 'PUT', 'DELETE'].includes(method);
}
