import {StatusCodes} from 'http-status-codes';
import {HTTPError} from 'ky';

export type SurppressNotFoundError = <T, S>(requestPromise: () => Promise<T>, fallback: S) => Promise<T | S> | never;

export const surppressNotFoundError: SurppressNotFoundError = async (requestPromise, fallback) => {
  try {
    return await requestPromise();
  } catch (e) {
    if (e instanceof HTTPError && e.response.status === StatusCodes.NOT_FOUND) {
      return fallback;
    }

    throw e;
  }
};
