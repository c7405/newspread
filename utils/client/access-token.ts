import {decode} from 'jsonwebtoken';
import {AppError} from '@cornerstone/app-error';
import {createSingleToken} from '@/libs/single-token';

export const {
  setToken: setAccessToken,
  getToken: getAccessToken,
} = createSingleToken();

export type ParseUserId = (token: string) => number | never;

export const parseUserId: ParseUserId = token => {
  const userId = decode(token, {json: true})?.sub;

  if (!userId) throw new AppError('INVALID_USER_ACCESS_TOKEN');

  return +userId;
};



