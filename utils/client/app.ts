import {ActiveType as FooterActiveType} from '@/components/pages/home/base/Footer';

export function isAppFooterActiveType (testType: string): testType is FooterActiveType {
  return [
    'home',
    'search',
    'bookmark',
    'user-profile',
  ]
    .includes(testType);
}
