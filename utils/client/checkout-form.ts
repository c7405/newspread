export type SubmitCheckoutForm = (entrypoint: string, formFields: Record<string, string | number>) => void;

export const submitCheckoutForm: SubmitCheckoutForm = (entrypoint, formFields) => {
  const form = document.createElement('form');

  form.setAttribute('method', 'POST');
  form.setAttribute('action', entrypoint);

  Object.entries(formFields)
    .forEach(([key, value]) => {
      const inputEl = document.createElement('input');

      inputEl.setAttribute('type', 'text');
      inputEl.setAttribute('name', key);
      inputEl.setAttribute('value', `${value}`);

      form.appendChild(inputEl);
    });

  const submitBtnEl = document.createElement('button');

  submitBtnEl.setAttribute('type', 'submit');

  form.appendChild(submitBtnEl);

  form.style.position = 'absolute';
  form.style.top = '-9999px';
  form.style.zIndex = '-1';

  document.body.appendChild(form);

  form.submit();
};
