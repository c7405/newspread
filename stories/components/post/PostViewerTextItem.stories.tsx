import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostViewerTextItem} from '@/components/pages/post/base';

export default {
  title: 'components/pages/post/base/PostViewerTextItem',
  component: PostViewerTextItem,
  argTypes: {
    value: {
      control: 'text',
    },
  },
  args: {
    value: 'Nam eget odio eu nisl tincidunt pulvinar. Aliquam fermentum vitae augue ut porttitor. Sed justo lacus, tincidunt at ipsum ac, sodales tincidunt erat.',
    props: null,
  },
} as ComponentMeta<typeof PostViewerTextItem>;

const Template: ComponentStory<typeof PostViewerTextItem> = args => (
  <PostViewerTextItem {...args} />
);

export const Default = Template.bind({});

export const CustomStyle = Template.bind({});

CustomStyle.args = {
  props: {
    style: {
      color: 'blue',
      fontWeight: 700,
    },
  },
};

