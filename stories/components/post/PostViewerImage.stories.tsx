import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostViewerImageItem} from '@/components/pages/post/base';

export default {
  title: 'components/pages/post/base/PostViewerImageItem',
  component: PostViewerImageItem,
  argTypes: {
    value: {
      control: 'text',
    },
  },
  args: {
    value: 'https://picsum.photos/seed/picsum/1280/800',
    props: {
      width: 1280,
      height: 800,
      caption: 'Nam eget odio eu nisl tincidunt pulvinar. Aliquam fermentum vitae augue ut porttitor. Sed justo lacus, tincidunt at ipsum ac, sodales tincidunt erat.',
    },
  },
} as ComponentMeta<typeof PostViewerImageItem>;

const Template: ComponentStory<typeof PostViewerImageItem> = args => (
  <PostViewerImageItem {...args} />
);

export const Default = Template.bind({});
