import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostViewer} from '@/components/pages/post/base';

export default {
  title: 'components/pages/post/base/PostViewer',
  component: PostViewer,
  args: {
    coverImageWidth: 1280,
    coverImageHeight: 800,
    coverImageSrc: 'https://picsum.photos/seed/picsum/1280/800',
    title: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit',
    exclusive: true,
    authors: [
      {
        id: 1,
        firstName: 'First',
        lastName: 'Last',
      },
    ],
    readTime: 3,
    updateDate: new Date().toISOString(),
    content: [
      {
        postId: 1,
        type: 'TEXT',
        value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus massa blandit magna rhoncus aliquet. Duis ut ipsum neque. Curabitur porta blandit augue sed auctor. Pellentesque ipsum magna, luctus ac diam vel, imperdiet facilisis nunc. Nulla id suscipit odio. Pellentesque ultrices congue dapibus.',
        position: 0,
        props: null,
      },
      {
        postId: 2,
        type: 'TEXT',
        value: 'Donec pulvinar risus sed mi vulputate, et venenatis nulla porttitor. Nullam ullamcorper lorem vel scelerisque laoreet. Morbi vel nunc ultrices, dapibus lacus auctor, malesuada mi. Aliquam ut metus nisi. Vivamus lacus mauris, mollis eget laoreet aliquet, rhoncus sagittis dui. Vestibulum et nisl vel dolor sollicitudin facilisis eget a lectus. Phasellus justo tortor, sollicitudin sed justo vel, scelerisque sodales risus. Integer volutpat leo quis eleifend commodo. Praesent id ipsum id dolor feugiat iaculis ut sed tortor.',
        position: 1,
        props: null,
      },
      {
        postId: 3,
        type: 'IMAGE',
        value: 'https://picsum.photos/seed/picsum/1280/800',
        position: 2,
        props: {
          width: 1280,
          height: 800,
          caption: 'There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain.',
        },
      },
      {
        postId: 4,
        type: 'TEXT',
        value: 'Duis imperdiet congue leo vitae mattis. Nulla at semper nulla. Fusce purus tortor, efficitur eu ipsum vitae, semper convallis diam. Vestibulum maximus ullamcorper augue, ut ultricies enim elementum vitae.',
        position: 3,
        props: null,
      },
    ],
    allowBookmark: true,
    bookmarked: true,
  },
} as ComponentMeta<typeof PostViewer>;

const Template: ComponentStory<typeof PostViewer> = args => (<PostViewer {...args} />);

export const Default = Template.bind({});
