import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SubscriptionCover} from '@/components/pages/post/base';

export default {
  title: 'components/pages/post/base/SubscriptionCover',
  component: SubscriptionCover,
} as ComponentMeta<typeof SubscriptionCover>;

const Template: ComponentStory<typeof SubscriptionCover> = args => (
  <SubscriptionCover {...args} />
);

export const Default = Template.bind({});


