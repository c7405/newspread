import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SignUpForm} from '@/components/pages/auth/sign-up';

export default {
  title: 'components/pages/auth/sign-up/SignUpForm',
  component: SignUpForm,
} as ComponentMeta<typeof SignUpForm>;

const Template: ComponentStory<typeof SignUpForm> = args => (<SignUpForm {...args} />);

export const Default = Template.bind({});
