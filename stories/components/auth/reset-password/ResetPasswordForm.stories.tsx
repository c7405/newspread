import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ResetPasswordForm} from '@/components/pages/auth/reset-password';

export default {
  title: 'components/pages/auth/reset-password/ResetPasswordForm',
  component: ResetPasswordForm,
} as ComponentMeta<typeof ResetPasswordForm>;

const Template: ComponentStory<typeof ResetPasswordForm> = args => (<ResetPasswordForm {...args} />);

export const Default = Template.bind({});
