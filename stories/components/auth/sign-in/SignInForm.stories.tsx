import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SignInForm} from '@/components/pages/auth/sign-in';

export default {
  title: 'components/pages/auth/sign-in/SignInForm',
  component: SignInForm,
  argTypes: {
    onSubmit: {
      action: 'onSubmit',
    },
  },
} as ComponentMeta<typeof SignInForm>;

const Template: ComponentStory<typeof SignInForm> = args => (
  <SignInForm {...args} />
);

export const Default = Template.bind({});
