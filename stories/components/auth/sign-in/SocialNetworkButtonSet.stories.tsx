import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SocialNetworkButtonSet} from '@/components/pages/auth/sign-in';

export default {
  title: 'components/pages/auth/sign-in/SocialNetworkButtonSet',
  component: SocialNetworkButtonSet,
} as ComponentMeta<typeof SocialNetworkButtonSet>;

const Template: ComponentStory<typeof SocialNetworkButtonSet> = args => (
  <SocialNetworkButtonSet {...args} />
);

export const Default = Template.bind({});
