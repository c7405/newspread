import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Header} from '@/components/pages/auth/base';

export default {
  title: 'components/pages/auth/base/Header',
  component: Header,
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = args => (<Header {...args} />);

export const Default = Template.bind({});
