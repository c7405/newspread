import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Footer} from '@/components/pages/auth/base';

export default {
  title: 'components/pages/auth/base/Footer',
  component: Footer,
} as ComponentMeta<typeof Footer>;

const Template: ComponentStory<typeof Footer> = args => (<Footer {...args} />);

export const Default = Template.bind({});
