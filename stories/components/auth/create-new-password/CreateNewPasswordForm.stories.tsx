import {ComponentMeta, ComponentStory} from '@storybook/react';
import {CreateNewPasswordForm} from '@/components/pages/auth/forgot-password';

export default {
  title: 'components/pages/auth/reset-password',
  component: CreateNewPasswordForm,
  argTypes: {
    onSubmit: {
      action: 'submit',
    },
  },
} as ComponentMeta<typeof CreateNewPasswordForm>;

const Template: ComponentStory<typeof CreateNewPasswordForm> = args => (<CreateNewPasswordForm {...args} />);

export const Default = Template.bind({});
