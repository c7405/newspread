import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostFooter} from '@/components/pages/home/base';

export default {
  title: 'components/base/PostFooter',
  component: PostFooter,
  argTypes: {
    updateDate: {
      control: 'text',
    },
    readTime: {
      control: 'number',
    },
    bookmarked: {
      control: 'boolean',
    },
    allowBookmark: {
      control: 'boolean',
    },
  },
  args: {
    updateDate: (new Date()).toISOString(),
    readTime: 80,
    bookmarked: undefined,
    allowBookmark: undefined,
  },
} as ComponentMeta<typeof PostFooter>;

const Template: ComponentStory<typeof PostFooter> = args => (<PostFooter {...args} />);

export const Default = Template.bind({});

Default.args = {
  updateDate: (new Date()).toISOString(),
  readTime: 80,
};

export const Bookmarked = Template.bind({});

Bookmarked.args = {
  updateDate: (new Date()).toISOString(),
  readTime: 80,
  bookmarked: true,
  allowBookmark: true,
};
