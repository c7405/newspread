import {PostToolset} from '@/components/pages/home/base';
import {ComponentMeta, ComponentStory} from '@storybook/react';

export default {
  title: 'components/base/PostToolset',
  component: PostToolset,
  argTypes: {
    bookmarked: {
      control: 'boolean',
    },
    allowBookmark: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof PostToolset>;

const Template: ComponentStory<typeof PostToolset> = args => (<PostToolset {...args} />);

export const Default = Template.bind({});

export const Bookmarked = Template.bind({});

Bookmarked.args = {
  bookmarked: true,
  allowBookmark: true,
};
