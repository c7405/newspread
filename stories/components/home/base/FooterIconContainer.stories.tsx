import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Home} from 'grommet-icons';
import {FooterIconContainer} from '@/components/pages/home/base';

export default {
  title: 'components/pages/home/base/FooterIconContainer',
  component: FooterIconContainer,
  argTypes: {
    isActive: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof FooterIconContainer>;

const Template: ComponentStory<typeof FooterIconContainer> = args => (
  <FooterIconContainer {...args}>
    <Home color="plain" />
  </FooterIconContainer>
);

export const Default = Template.bind({});

Default.args = {
  isActive: false,
};

export const Active = Template.bind({});

Active.args = {
  isActive: true,
};
