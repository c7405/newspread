import {Header} from '@/components/pages/home/base';
import type {ComponentMeta, ComponentStory} from '@storybook/react';

export default {
  title: 'components/pages/home/base/Header',
  component: Header,
  argTypes: {
    userLoading: {
      control: 'boolean',
    },
    hasUnreadNotification: {
      control: 'boolean',
    },
    notificationActive: {
      control: 'boolean',
    },
  },
  args: {
    user: {
      id: 1,
      account: 'mock@newspread.tw',
      userDetail: {
        id: 1,
        firstName: 'First',
        lastName: 'Last',
        avatarImagePath: 'https://mock.com',
      },
    },
    userLoading: false,
    hasUnreadNotification: false,
    notificationActive: false,
  },
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = args => (<Header {...args} />);

export const Default = Template.bind({});

export const HasSignIn = Template.bind({});

HasSignIn.args = {
  user: undefined,
};

export const HasUnreadNotification = Template.bind({});
