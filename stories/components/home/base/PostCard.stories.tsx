import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostCard} from '@/components/pages/home/base';

export default {
  title: 'components/base/PostCard',
  component: PostCard,
  argTypes: {
    postId: {
      control: 'number',
    },
    coverImageSrc: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    exclusive: {
      control: 'boolean',
    },
    updateDate: {
      control: 'text',
    },
    readTime: {
      control: 'number',
    },
    bookmarked: {
      control: 'boolean',
    },
    allowBookmark: {
      control: 'boolean',
    },
  },
  args: {
    postId: 1,
    coverImageSrc: 'https://picsum.photos/id/1/367/267',
    title: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',
    exclusive: false,
    updateDate: (new Date()).toISOString(),
    readTime: 10,
    bookmarked: undefined,
    allowBookmark: undefined,
  },
} as ComponentMeta<typeof PostCard>;

const Template: ComponentStory<typeof PostCard> = args => (<PostCard {...args} />);

export const Default = Template.bind({});

export const Bookmarked = Template.bind({});

Bookmarked.args = {
  ...Default.args,
  bookmarked: true,
  allowBookmark: true,
};

export const Exclusive = Template.bind({});

Exclusive.args = {
  ...Default.args,
  exclusive: true,
};
