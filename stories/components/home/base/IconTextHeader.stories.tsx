import {ComponentMeta, ComponentStory} from '@storybook/react';
import * as iconMap from 'grommet-icons';
import {IconTextHeader} from '@/components/base';

export default {
  title: 'components/base/IconTextHeader',
  component: IconTextHeader,
  argTypes: {
    icon: {
      control: 'select',
      options: Object.keys(iconMap),
      mapping: iconMap,
    },
    title: {
      control: 'text',
    },
    subTitle: {
      control: 'text',
    },
  },
  args: {
    icon: iconMap.Help,
  },
} as ComponentMeta<typeof IconTextHeader>;

const Template: ComponentStory<typeof IconTextHeader> = args => (
  <IconTextHeader {...args} />
);

export const Default = Template.bind({});

Default.args = {
  title: 'Default',
  subTitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
};
