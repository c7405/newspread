import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Footer} from '@/components/pages/home/base';

export default {
  title: 'components/pages/home/base/Footer',
  component: Footer,
  argTypes: {
    activeType: {
      control: 'select',
      options: ['home', 'search', 'bookmark', 'userProfile'],
    },
    onActiveTypeChange: {
      action: 'activeTypeChange',
    },
  },
} as ComponentMeta<typeof Footer>;

const Template: ComponentStory<typeof Footer> = args => (
  <Footer {...args} />
);

export const Home = Template.bind({});

Home.args = {
  activeType: 'home',
};

export const Search = Template.bind({});

Search.args = {
  activeType: 'search',
};

export const Bookmark = Template.bind({});

Bookmark.args = {
  activeType: 'bookmark',
};

export const UserProfile = Template.bind({});

UserProfile.args = {
  activeType: 'user-profile',
};
