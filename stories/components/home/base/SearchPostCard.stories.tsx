import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SearchPostCard} from '@/components/pages/home/base';

export default {
  title: 'components/base/SearchPostCard',
  component: SearchPostCard,
  argTypes: {
    postId: {
      control: 'number',
    },
    coverImageSrc: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    exclusive: {
      control: 'boolean',
    },
    updateDate: {
      control: 'text',
    },
    readTime: {
      control: 'number',
    },
    bookmarked: {
      control: 'boolean',
    },
    onShareClick: {
      action: 'shareClick',
    },
    onBookmarkClick: {
      action: 'bookmarkClick',
    },
  },
} as ComponentMeta<typeof SearchPostCard>;

const Template: ComponentStory<typeof SearchPostCard> = args => (<SearchPostCard {...args} />);

export const Default = Template.bind({});

Default.args = {
  postId: 1,
  coverImageSrc: 'https://picsum.photos/id/1/367/267',
  title: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',
  exclusive: false,
  updateDate: '2020-04-01 00:00:00',
  readTime: 10,
  bookmarked: false,
};

export const Bookmarked = Template.bind({});

Bookmarked.args = {
  ...Default.args,
  bookmarked: true,
};

export const Exclusive = Template.bind({});

Exclusive.args = {
  ...Default.args,
  exclusive: true,
};
