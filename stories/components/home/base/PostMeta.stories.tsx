import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PostMeta} from '@/components/pages/home/base';

export default {
  title: 'components/base/PostMeta',
  component: PostMeta,
  argTypes: {
    updateDate: {
      control: 'text',
    },
    readTime: {
      control: 'number',
    },
  },
} as ComponentMeta<typeof PostMeta>;

const Template: ComponentStory<typeof PostMeta> = args => (<PostMeta {...args} />);

export const Default = Template.bind({});

Default.args = {
  updateDate: (new Date()).toISOString(),
  readTime: 1,
};
