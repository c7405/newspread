import {ComponentMeta, ComponentStory} from '@storybook/react';
import * as iconMapping from 'grommet-icons';
import {Information} from '@/components/base';

export default {
  title: 'components/base/Information',
  component: Information,
  argTypes: {
    icon: {
      control: 'select',
      options: Object.keys(iconMapping),
      mappings: iconMapping,
    },
    message: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: [
        'default',
        'primary',
        'warning',
        'danger',
      ],
    },
  },
} as ComponentMeta<typeof Information>;

const Template: ComponentStory<typeof Information> = args => (
  <Information {...args} />
);

export const Default = Template.bind({});

Default.args = {
  icon: iconMapping.Info,
  message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
  variant: 'default',
};
