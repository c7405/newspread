import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Input} from '@/components/base';

export default {
  title: 'components/base/Input',
  component: Input,
  argTypes: {
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = args => (<Input {...args} />);

export const Default = Template.bind({});

Default.args = {
  fieldName: 'Field',
};

export const HasError = Template.bind({});

HasError.args = {
  fieldName: 'Field',
  hasError: true,
};
