import {ComponentStory, ComponentMeta} from '@storybook/react';
import {Button} from '@/components/base';

export default {
  title: 'components/base/Button',
  component: Button,
  argTypes: {
    variant: {
      control: 'select',
      options: [
        'default',
        'primary',
        'warning',
        'danger',
      ],
    },
    loading: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
  },
  args: {
    variant: 'default',
    loading: false,
    disabled: false,
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = args => (
  <Button {...args}>
    Button
  </Button>
);

export const Default = Template.bind({});

export const Loading = Template.bind({});

Loading.args = {
  loading: true,
};

export const Disabled = Template.bind({});

Disabled.args = {
  disabled: true,
};
