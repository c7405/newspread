import {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {ModalContainer, Button} from '@/components/base';

export default {
  title: 'components/base/ModalContainer',
  component: ModalContainer,
  argTypes: {
    onCloseEnd: {
      action: 'onCloseEnd',
    },
  },
} as ComponentMeta<typeof ModalContainer>;

const AllOnTemplate: ComponentStory<typeof ModalContainer> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <Button
        onClick={() => setIsOpen(!isOpen)}>
        Open Modal
      </Button>
      <ModalContainer
        open={isOpen}
        header={<div>Test</div>}
        body={<div>Body</div>}
        footer={<div>Footer</div>}
        onCloseEnd={args.onCloseEnd} />
    </>
  );
};

export const Default = AllOnTemplate.bind({});

const WithoutFooterTemplate: ComponentStory<typeof ModalContainer> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <Button
        onClick={() => setIsOpen(!isOpen)}>
        Open Modal
      </Button>
      <ModalContainer
        open={isOpen}
        header={<div>Test</div>}
        body={<div>Body</div>}
        onCloseEnd={args.onCloseEnd} />
    </>
  );
};

export const WithoutFooter = WithoutFooterTemplate.bind({});
