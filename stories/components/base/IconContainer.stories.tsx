import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Notification} from 'grommet-icons';
import {IconContainer} from '@/components/base';

export default {
  title: 'components/base/IconContainer',
  component: Notification,
  argTypes: {
    isActive: {
      control: 'boolean',
    },
    isStatic: {
      control: 'boolean',
    },
    preventActive: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof IconContainer>;

const Template: ComponentStory<typeof IconContainer> = args => (
  <IconContainer {...args}>
    <Notification color="plain" />
  </IconContainer>
);

export const StaticInactive = Template.bind({});

StaticInactive.args = {
  isActive: false,
  isStatic: true,
};

export const DynamicInactive = Template.bind({});

DynamicInactive.args = {
  isActive: false,
  isStatic: false,
};

export const DynamicActive = Template.bind({});

DynamicActive.args = {
  isActive: true,
  isStatic: false,
};

export const PreventActive = Template.bind({});

PreventActive.args = {
  isActive: false,
  isStatic: false,
  preventActive: true,
};
