import {ComponentMeta, ComponentStory} from '@storybook/react';
import {PasswordInput} from '@/components/base';

export default {
  title: 'components/base/PasswordInput',
  component: PasswordInput,
  argTypes: {
    fieldName: {
      control: 'text',
    },
  },
} as ComponentMeta<typeof PasswordInput>;

const Template: ComponentStory<typeof PasswordInput> = args => (
  <PasswordInput {...args} />
);

export const Default = Template.bind({});

Default.args = {
  fieldName: 'Password',
};
