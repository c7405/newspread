import {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {SearchInput} from '@/components/base';

export default {
  title: 'components/base/SearchInput',
  component: SearchInput,
  argTypes: {
    fieldName: {
      control: 'text',
    },
  },
} as ComponentMeta<typeof SearchInput>;

const Template: ComponentStory<typeof SearchInput> = args => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <SearchInput
      {...args}
      value={searchValue}
      onChange={e => {setSearchValue(e.target.value);}} />
  );
};

export const Default = Template.bind({});

Default.args = {
  fieldName: 'Search',
};
