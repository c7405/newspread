import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ScrollEndDetector} from '@/components/base';

export default {
  title: 'components/base/ScrollEndDetector',
  component: ScrollEndDetector,
  argsTypes: {
    scrollEndTolerance: {
      control: 'number',
    },
  },
  args: {
    scrollEndTolerance: undefined,
  },
} as ComponentMeta<typeof ScrollEndDetector>;

const Template: ComponentStory<typeof ScrollEndDetector> = args => (
  <div style={{height: '300px'}}>
    <ScrollEndDetector {...args}>
      <div style={{height: '3000px'}}>Content</div>
    </ScrollEndDetector>
  </div>
);

export const Default = Template.bind({});
