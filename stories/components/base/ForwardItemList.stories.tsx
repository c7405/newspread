import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ForwardItem, ForwardItemList} from '@/components/base';

export default {
  title: 'components/base/ForwardItemList',
  component: ForwardItemList,
} as ComponentMeta<typeof ForwardItemList>;

const SingleItemTemplate: ComponentStory<typeof ForwardItemList> = () => (
  <ForwardItemList>
    <ForwardItem>
      Text 1
    </ForwardItem>
  </ForwardItemList>
);

export const SingleItem = SingleItemTemplate.bind({});

const MultipleItemTemplate: ComponentStory<typeof ForwardItemList> = () => (
  <ForwardItemList>
    <ForwardItem>
      Text 1
    </ForwardItem>
    <ForwardItem>
      Text 2
    </ForwardItem>
    <ForwardItem>
      Text 3
    </ForwardItem>
  </ForwardItemList>
);

export const MultipleItem = MultipleItemTemplate.bind({});
