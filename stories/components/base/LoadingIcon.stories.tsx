import {ComponentMeta, ComponentStory} from '@storybook/react';
import {LoadingIcon} from '@/components/base';

export default {
  title: 'components/base/LoadingIcon',
  component: LoadingIcon,
  argTypes: {
    width: {
      control: 'number',
    },
    height: {
      control: 'number',
    },
    radius: {
      control: 'number',
    },
    stroke: {
      control: 'text',
    },
  },
  args: {
    width: 40,
    height: 40,
    radius: 8,
    stroke: 'black',
  },
} as ComponentMeta<typeof LoadingIcon>;

const Template: ComponentStory<typeof LoadingIcon> = args => (
  <LoadingIcon {...args} />
);

export const Default = Template.bind({});
