import {ComponentMeta, ComponentStory} from '@storybook/react';
import {RedDotContainer} from '@/components/base';

export default {
  title: 'components/base/RedDotContainer',
  component: RedDotContainer,
  argTypes: {
    isActive: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof RedDotContainer>;

const Template: ComponentStory<typeof RedDotContainer> = args => (
  <RedDotContainer {...args}>
    Children
  </RedDotContainer>
);

export const Inactive = Template.bind({});

Inactive.args = {
  isActive: false,
};

export const Active = Template.bind({});

Active.args = {
  isActive: true,
};
