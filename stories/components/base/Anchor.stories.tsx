import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Anchor} from '@/components/base';

export default {
  title: 'components/base/Anchor',
  component: Anchor,
  argTypes: {
    href: {
      control: 'text',
    },
  },
} as ComponentMeta<typeof Anchor>;

const Template: ComponentStory<typeof Anchor> = args => (
  <Anchor {...args}>
    Link
  </Anchor>
);

export const Default = Template.bind({});

Default.args = {
  href: 'https://example.com',
};
