import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Portal} from '@/components/base';

export default {
  title: 'components/base/Portal',
  component: Portal,
  argTypes: {
    portalId: {
      control: 'text',
    },
    portalClassName: {
      control: 'text',
    },
  },
} as ComponentMeta<typeof Portal>;

const Template: ComponentStory<typeof Portal> = args => (<Portal {...args}>Children</Portal>);

export const Default = Template.bind({});

Default.args = {
  portalId: 'portal-id',
  portalClassName: 'portal-classname',
};
