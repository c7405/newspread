import {ComponentMeta, ComponentStory} from '@storybook/react';
import {SignInNotification} from '@/components/base';

export default {
  title: 'components/base/SignInNotification',
  component: SignInNotification,
  argsTypes: {
    title: {
      control: 'text',
    },
    message: {
      control: 'text',
    },
  },
  args: {
    title: undefined,
    message: 'Message',
  },
} as ComponentMeta<typeof SignInNotification>;

const Template: ComponentStory<typeof SignInNotification> = args => (
  <SignInNotification {...args} />
);

export const Default = Template.bind({});
