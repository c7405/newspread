import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ValidationMessage} from '@/components/base';

export default {
  title: 'components/base/ValidationMessage',
  component: ValidationMessage,
  argTypes: {
    message: {
      control: 'text',
    },
  },
} as ComponentMeta<typeof ValidationMessage>;

const Template: ComponentStory<typeof ValidationMessage> = args => (<ValidationMessage {...args} />);

export const Default = Template.bind({});

Default.args = {
  message: 'Field should be of string',
};
