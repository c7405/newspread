import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {ClickActiveHighlight} from '@/components/base/ClickActiveHighlight';

export default {
  title: 'components/base/ClickActiveHighlight',
  components: ClickActiveHighlight,
  argTypes: {
    disabled: {
      type: 'boolean',
    },
  },
} as ComponentMeta<typeof ClickActiveHighlight>;

const Template: ComponentStory<typeof ClickActiveHighlight> = args => (
  <ClickActiveHighlight {...args}>
    <div style={{width: '100%', height: '400px', background: 'white'}}>
      Content
    </div>
  </ClickActiveHighlight>
);

export const Default = Template.bind({});

Default.args = {
  disabled: false,
};

export const Disabled = Template.bind({});

Disabled.args = {
  disabled: true,
};

