import {ComponentMeta, ComponentStory} from '@storybook/react';
import {FullContentLoadingIndicator} from '@/components/base';

export default {
  title: 'components/base/FullContentLoadingIndicator',
  component: FullContentLoadingIndicator,
} as ComponentMeta<typeof FullContentLoadingIndicator>;

const Template: ComponentStory<typeof FullContentLoadingIndicator> = () => (
  <FullContentLoadingIndicator />
);

export const Default = Template.bind({});
