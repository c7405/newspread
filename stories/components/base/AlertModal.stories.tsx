import {ComponentMeta, ComponentStory} from '@storybook/react';
import {AlertModal} from '@/components/base';

export default {
  title: 'components/base/AlertModal',
  component: AlertModal,
  argTypes: {
    open: {
      control: 'boolean',
    },
    title: {
      control: 'text',
    },
    titleVariant: {
      control: 'select',
      options: ['default', 'danger'],
    },
    message: {
      control: 'text',
    },
    buttonText: {
      control: 'text',
    },
    buttonVariant: {
      control: 'select',
      options: ['default', 'primary', 'warning', 'danger'],
    },
  },
  args: {
    title: 'Title',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
} as ComponentMeta<typeof AlertModal>;

const Template: ComponentStory<typeof AlertModal> = args => (
  <AlertModal {...args} />
);

export const Default = Template.bind({});

Default.args = {
  open: true,
};
