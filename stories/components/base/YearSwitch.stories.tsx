import {ComponentMeta, ComponentStory} from '@storybook/react';
import {YearSwitch} from '@/components/base';

export default {
  title: 'components/base/YearSwitch',
  component: YearSwitch,
  argTypes: {
    minYear: {
      control: 'number',
    },
    year: {
      control: 'number',
    },
  },
} as ComponentMeta<typeof YearSwitch>;

const Template: ComponentStory<typeof YearSwitch> = args => (<YearSwitch {...args} />);

export const Default = Template.bind({});

Default.args = {
  minYear: 2022,
  year: 2023,
};

export const MinYear = Template.bind({});

MinYear.args = {
  minYear: 2022,
  year: 2022,
};
