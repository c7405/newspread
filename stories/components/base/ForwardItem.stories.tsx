import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ForwardItem} from '@/components/base';

export default {
  title: 'components/base/ForwardItem',
  component: ForwardItem,
  argTypes: {
    onClick: {
      action: 'click',
    },
  },
} as ComponentMeta<typeof ForwardItem>;

const Template: ComponentStory<typeof ForwardItem> = args => (
  <ForwardItem {...args}>
    Text
  </ForwardItem>
);

export const Default = Template.bind({});
