import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Checkbox} from '@/components/base';

export default {
  title: 'components/base/Checkbox',
  component: Checkbox,
  argTypes: {
    checked: {
      type: 'boolean',
    },
    disabled: {
      type: 'boolean',
    },
    onChange: {
      action: 'change',
    },
  },
  args: {
    checked: false,
    disabled: false,
  },
} as ComponentMeta<typeof Checkbox>;

const Template: ComponentStory<typeof Checkbox> = args => (<Checkbox {...args} />);

export const Unchecked = Template.bind({});

Unchecked.args = {
  checked: false,
};

export const Checked = Template.bind({});

Checked.args = {
  checked: true,
};

export const UncheckedDisabled = Template.bind({});

UncheckedDisabled.args = {
  checked: false,
  disabled: true,
};

export const CheckedDisabled = Template.bind({});

CheckedDisabled.args = {
  checked: true,
  disabled: true,
};
