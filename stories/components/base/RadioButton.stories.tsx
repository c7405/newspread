import {ComponentMeta, ComponentStory} from '@storybook/react';
import {RadioButton} from '@/components/base';

export default {
  title: 'components/base/RadioButton',
  component: RadioButton,
  argTypes: {
    checked: {
      type: 'boolean',
    },
    disabled: {
      type: 'boolean',
    },
    onChange: {
      action: 'change',
    },
  },
  args: {
    checked: false,
    disabled: false,
  },
} as ComponentMeta<typeof RadioButton>;

const Template: ComponentStory<typeof RadioButton> = args => (<RadioButton {...args} />);

export const Unchecked = Template.bind({});

Unchecked.args = {
  checked: false,
};

export const Checked = Template.bind({});

Checked.args = {
  checked: true,
};

export const UncheckedDisabled = Template.bind({});

UncheckedDisabled.args = {
  checked: false,
  disabled: true,
};

export const CheckedDisabled = Template.bind({});

CheckedDisabled.args = {
  checked: true,
  disabled: true,
};
