import {ComponentMeta, ComponentStory} from '@storybook/react';
import {UndoToast} from '@/components/base';
import {useState} from 'react';

export default {
  title: 'components/base/UndoToast',
  component: UndoToast,
  argTypes: {
    message: {
      control: 'text',
    },
    undoText: {
      control: 'text',
    },
    onUndo: {
      action: 'undo',
    },
  },
} as ComponentMeta<typeof UndoToast>;

const Template: ComponentStory<typeof UndoToast> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <UndoToast
        {...args}
        open={isOpen}
        onClose={() => setIsOpen(false)}
        onUndo={() => new Promise(resolve => {
          const id = window.setTimeout(() => {
            args.onUndo();
  
            resolve();
  
            window.clearTimeout(id);
          }, 1000);
        })} />
      {
        !isOpen && (
          <button onClick={() => setIsOpen(!isOpen)}>
            Open
          </button>
        )
      }
    </>
  );
};

export const Default = Template.bind({});

Default.args = {
  message: 'Are you sure to undo?',
  undoText: 'Undo',
};
